﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(DragnetAlphaWebAPI.Startup))]

namespace DragnetAlphaWebAPI
{
    public partial class Startup
    {

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");

            //BackgroundJob.Enqueue(() => Console.WriteLine("Getting Started with HangFire!"));

            //BackgroundJob.Schedule(() => Console.WriteLine("Delayed!"),TimeSpan.FromSeconds(15));

            //RecurringJob.AddOrUpdate(  () => Console.WriteLine("Recurring!"),    Cron.Minutely);

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            //var options = new DashboardOptions
            //{
            //   Authorization = new[] { new AuthorizationFilter { Users = "admin", Roles = "advance" } }
                                       
        
            //};
            //app.UseHangfireDashboard("/hangfire", options);
        }
    }
}
