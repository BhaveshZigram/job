﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class ProjectInfo
    {
        public int ProjectID { get; set; }

        public string ProjectName { get; set; }

        public byte[] ProjectImage { get; set; }
        public string ImageType { get; set; }

    }
}