﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class DownloadFileUrlResponseModel
    {
        public string CSVDownloadUrl { get; set; }
        public string XMLDownloadUrl { get; set; }

    }
}