﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DragnetAlphaWebAPI.Models.Enums;
namespace DragnetAlphaWebAPI.Models
{
    public class AnalyticsModel
    {
        public String FlagName { get; set; }      
            public int Id { get; set; }
        public int Count { get; set; }
        public int TotalLinks { get; set; }
        public String StatusName { get; set; }
    }
}