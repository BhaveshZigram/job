﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class SelectedEntityModel
    {
        public List<string> EntitySelected { get; set; }
        public List<string> Language { get; set; }
        public List<string> SourceSelected { get; set; }
        public bool ManualSupervision { get; set; }
        public bool OneTimeMonitoring { get; set; }
        public string TimeFrameSelected { get; set; }
        public int ProjectId { get; set; }

    }
}