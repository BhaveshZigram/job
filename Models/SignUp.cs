﻿using System.Collections.Generic;

namespace DragnetAlphaWebAPI.Models
{
    public class SignUp
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
        public string Project { get; set; }
        public byte[] UserImage { get; set; }
        public string ImageType { get; set; }
    }
}