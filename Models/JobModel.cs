﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class JobModel
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public System.DateTime CreatedAt { get; set; }

        public string EntityName { get; set; }

        public List<string> Alias { get; set; }

        public string Filename { get; set; }

        public List<string> Subsidaries { get; set; }

        public List<string> Language { get; set; }

        public List<string> SearchChannel { get; set; }

        public int TimeFrame { get; set; }

        public int OngoingMonitoringFrequency { get; set; }

        public bool Status { get; set; }

        public string Country { get; set; }

        public string State { get; set; }
    }
}