﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class EventUpdate
    {
        public string Project { get; set; }
        public string EventSelected { get; set; }
        public int LinkId { get; set; }
    }
    public class ThemeUpdate
    {
        public string Project { get; set; }
        public string ThemeSelected { get; set; }
        public int LinkId { get; set; }
    }
}