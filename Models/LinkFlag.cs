﻿namespace DragnetAlphaWebAPI.Models
{
    public class LinkFlag
    {
        public int LinkFlags { get; set; }
        public int projectId { get; set; }
        public int LinkId { get; set; }
    }
}