﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models.Enums
{
    public enum LinkFlagEnums
    {
        
        NotMarked,      
        Relevent, 
        Favourite, 
        Irrelevent
    }
}