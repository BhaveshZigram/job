﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models.Enums
{
    public enum ReviewStatusEnum
    {
        [Display(Name = "Not Reviewed")]
        NotReviewed,
        [Display(Name = "In Progress")]
        InProgress,
        Reviewed
    }
}