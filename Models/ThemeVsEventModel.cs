﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class ThemeVsEventModel
    {
        public List<ThemeModel> ThemeVsEvent { get; set; }
    }

    public class ThemeModel
    {

        public string ThemeName { get; set; }
        public bool ThemeAvailable { get; set; }
        public int ThemeId { get; set; }
        public string ColorName { get; set; }
        public string ColorCode { get; set; }
        public List<EventModel> EventList { get; set; }
        public ThemeModel()
        {
            EventList = new List<EventModel>();
            this.ThemeAvailable = false;
        }
    }

    public class EventModel
    {
        public bool EventAvailable { get; set; }
        public string EventName { get; set; }
        public int EventId { get; set; }
        public string ColorCode { get; set; }
        public EventModel()
        {
            this.EventAvailable = false;
        }
        public string Criticality { get; set; }

        public List<KeywordModel> KeyWordList { get; set; }

    }
    public class KeywordModel
    {
        public string Keyword { get; set; }
        public int KeywordId { get; set; }
        public string Criticality { get; set; }

    }

}