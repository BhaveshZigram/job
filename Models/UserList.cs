﻿namespace DragnetAlphaWebAPI.Models
{
    public class UserList
    {
        public string UserName { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        public int? UserId { get; set; }
        public string FullName { get; set; }
       
    }
}