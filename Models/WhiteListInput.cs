﻿namespace DragnetAlphaWebAPI.Models
{
    public class WhiteListInput
    {
        public string WhiteList { get; set; }
    }
}