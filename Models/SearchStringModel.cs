﻿
namespace DragnetAlphaWebAPI.Models
{
    public class SearchStringModel
    {
        public int? TargetEntityId { get; set; }
        public string SearchString {get; set;}
        public int? SearchTerm_id { get; set; }
        public string TargetEntity { get; set; }
        public int? EventId { get; set; }
        public int project_id { get; set; }
        public int ThemeId { get; set; }
        public string EventTagged { get; set; }
        public string Theme { get; set; }
        public string ThemeVsEventModelJson { get; set; }
        public string CountryCode { get; set; }
        public string ScriptURL { get; set; }
        public string KeyMapping { get; set; }
    }
}