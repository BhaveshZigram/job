﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class GetLinkInputModel
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public string Role { get; set; }
        public List<string> ThemeSelected { get; set; }
    }
}