﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class ClientExternalSearchModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int DaysCount { get; set; }
        public int TimeFrequency { get; set; }
        public string EntityName { get; set; }
        public string Language { get; set; }
        public bool Review { get; set; }
        public bool DsModel { get; set; }
        public Guid guid { get; set; }
    }
}