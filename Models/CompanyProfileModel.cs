﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class CompanyProfileModel
    {
        public string EntityName { get; set; }
        public string Sector { get; set; }
        public string Industry { get; set; }
        public string CompanyType_Global_Local { get; set; }
        public string Headquarters { get; set; }
        public string CEO { get; set; }
        public string CFO { get; set; }
        public string Website { get; set; }
        public string FinancialType_PublicorPrivate { get; set; }
        public string MarketIndex { get; set; }
        public string ParentOrganisation { get; set; }
        public int Id { get; set; }
        public List<string> BoardofDirectors { get; set; }
        public List<string> Subsidiaries { get; set; }
        public List<string> Founders { get; set; }
        public List<string> OtherNamesforSearch { get; set; }
        public string TradeName { get; set; }
        public string Hits { get; set; }
        public string CompanyLogoUrl { get; set; }
        public int? Stories { get; set; }
        public int? EventsCount { get; set; }
        public int? ThemesCount { get; set; }
        public string AssociationMapLink { get; set; }
    }
}