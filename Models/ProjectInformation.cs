﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using MultipartDataMediaFormatter.Infrastructure;

namespace DragnetAlphaWebAPI.Models
{
    public class ProjectInformation
    {
        public string Name { get; set; }

        public byte[] ImageInfo { get; set; }
       // public Image ImageProj { get; set; }
    }
}
