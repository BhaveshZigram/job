﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class LinkType
    {
        public string Project { get; set; }
        public string HashCode { get; set; }
        public int UserId { get; set; }
        public string LinkCategory { get; set; }
    }
}