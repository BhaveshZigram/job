﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class UserDetailsModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string SecondaryEmail { get; set; }
        public List<string> ProjectaAssigned { get; set; }
        public string Role { get; set; }
        public string Country { get; set; }
        public string Company { get; set; }
        public string Department { get; set; }
        public string Industry { get; set; }
        public string Designation { get; set; }
        public string LinkedinProfileURL { get; set; }
        public string OfficialEmail { get; set; }
        public byte[] ProjectImage { get; set; }
        public string ImageType { get; set; }
    }
}