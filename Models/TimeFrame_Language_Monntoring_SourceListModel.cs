﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class TimeFrame_Language_Monntoring_SourceListModel
    {
        public List<Search_TimeFrame> timeframe { get; set; }
        public List<Search_Monitoring> monitoring { get; set; }
        public List<Search_Language> language { get; set; }
        public List<Search_NewsSourceModel> news { get; set; }
    }
}