﻿using System;
using System.Collections.Generic;

namespace DragnetAlphaWebAPI.Models
{
    public class LinksModel
    {
        public string Link { get; set; }
        public string ImageUrl { get; set; }
        public string Heading { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public string DatePublished { get; set; }
        public string NewsProvider { get; set; }
        public Nullable<int> SearchTerm_id { get; set; }
        public string LinkHashCode { get; set; }
        public string MakerName { get; set; }

        public Nullable<int> MakerId { get; set; }
        public Nullable<int> AdminId { get; set; }
        public Nullable<int> ProjectId { get; set; }
        public Nullable<int> LinkFlag_Id { get; set; }
        public string ReviewStatu { get; set; }
        public string LinkCategorization { get; set; }
        public string LinkCategory { get; set; }
        public string Comment { get; set; }
        public int LinkId { get; set; }
        public string Language { get; set; }
        public string type { get; set; }
        public Nullable<double> SentimentScore { get; set; }
        public Nullable<double> RelevanceScore { get; set; }
        public string Tags { get; set; }
        public string SentimentSentence { get; set; }
        public string Sentiment { get; set; }

        public string SubCategory { get; set; }

        public string DuplicateFlag { get; set; }
        public Nullable<int> LinkPriority { get; set; }
        public string SourceReputation { get; set; }
        public string content_extraction { get; set; }
        public Nullable<int> Content_Extraction_Script_Status { get; set; }
        public Nullable<int> DeDupe_Script_Status { get; set; }
        public Nullable<int> Sentiment_Script_Status { get; set; }
        public Nullable<int> Relevance_Score_Status { get; set; }
        public Nullable<int> Tags_Score_Status { get; set; }
        public string Summary { get; set; }
        public Nullable<int> Summary_Script_Status { get; set; }
        public Nullable<int> GroupingId { get; set; }
        public List<LinksModel> Grouping { get; set; }
        public string KeywordsFound { get; set; }
        public string Theme { get; set; }
        public string TargetedEntity { get; set; }
        public string EventsTagged { get; set; }
        public string ConcatenatedData { get; set; }

        public string ConcatenatedData2 { get; set; }
        public string ThemeVsEventJson { get; set; }

        public string SimilarLinks { get; set; }
        public int? TargetedEntityId { get; set; }

    }
    public class LinkEntity
    {
        public string TargetedEntity { get; set; }
        public string DatePublish { get; set; }
        public string Heading { get; set; }
        public string Description { get; set; }
        public string NewsLink { get; set; }
        public string NewsProvider { get; set; }
        public string Theme { get; set; }
        public string Event { get; set; }
        public string Keywords { get; set; }
        public double? Relevance { get; set; }
        public string Sentiment { get; set; }
        public double? SentimentScore { get; set; }
        public string SentimentSentence { get; set; }
        public string Summary { get; set; }
        public string SourceReputation { get; set; }
        public string SimilarLinks { get; set; }
    }

    public class LinkEntities
    {
        public List<LinkEntity> LinkEntity { get; set; }
    }
}