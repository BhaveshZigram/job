﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class GlobalSearchModel
    {
        public int linkid { get; set; }
        public int Foundcount { get; set; }
        public LinksModel LinksModel { get; set; }
    }
}