﻿using System;
using System.Collections.Generic;

namespace DragnetAlphaWebAPI.Models
{
    public class AuthResponseModel
    {
        public string Message { get; set; }
        public bool Status { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> Role { get; set; }
        public Guid IdGuid { get; set; }
        public List<ProjectModel> ProjectAssigned { get; set; }
        public string UserImage { get; set; }
        public string ImageType { get; set; }
    }
    public class ProjectModel
    {
        public string ProjectName { get; set; }
        public string ProjectImage { get; set; }
        public int? ProjectID { get; set; }
        public string ImageType { get; set; }
        }
}