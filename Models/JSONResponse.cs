﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class JSONResponse
    {
        public bool Success { get; set; }
        public HttpStatusCode ResponseCode { get; set; }
        public string Message { get; set; }
        public dynamic Body { get; set; }
    }
}