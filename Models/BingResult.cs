﻿namespace DragnetAlphaWebAPI.Models
{
    public class BingResult
    {
        public string Heading { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string DatePublished { get; set; }
        public string Category { get; set; }
        public string NewProvider { get; set; }
        public string ImageUrl { get; set; }
        public string HashCode { get; set; }
        public string SubCategory { get; set; }
        public string LinkDomain { get; set; }
        public string SearchSring { get; set; }
        public string Comment { get; set; }
        public string ReviewStatus { get; set; }
        public int LinkFlag { get; set; }
        public string MakerName { get; set; }
        //public Guid SourceId { get; set; }
    }
}