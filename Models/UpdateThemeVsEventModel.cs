﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Methods
{
    public class UpdateThemeVsEventModel
    {
        public int linkId { get; set; }
        public dynamic ThemeVsEventJson { get; set; }
    }
}