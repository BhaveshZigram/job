﻿namespace DragnetAlphaWebAPI.Models
{
    public class ReviewStatus
    {
        public int ReviewStatusCode { get; set; }
        public string Comment { get; set; }
        public int LinkId { get; set; }
    }
}