﻿using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace DragnetAlphaWebAPI.Models
{
    public class MD5Hash
    {
        public static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            foreach (byte t in data)
            {
                sBuilder.Append(t.ToString("x2", CultureInfo.InvariantCulture));
            }
            return sBuilder.ToString();
        }
    }
}