﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Models
{
    public class Search_NewsSourceModel
    {
        public string SourceName { get; set; }

        public Nullable<bool> Active_Status { get; set; }

        public int Id { get; set; }

        public string ScriptType { get; set; }

        public string ScriptURL { get; set; }

        public string KeyMapping { get; set; }
    }
}