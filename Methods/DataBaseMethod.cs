﻿using System.Linq;

namespace DragnetAlphaWebAPI.Methods
{
    public class DataBaseMethod
    {
        public static void UpdateGroupingId(int projectId)
        {
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                var links = dbContext.NewsLinks.Where(x => x.project_id == projectId).Where(x=>x.GroupingId ==null).Where(x=>x.DuplicateFlag=="No").ToList();
                links.ForEach(a => a.GroupingId = a.id);
                dbContext.SaveChanges();
            }
        }
        public static void ReadLinksTable_Admin()
        {

        }

    }
}