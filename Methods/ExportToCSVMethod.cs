﻿using CsvHelper;
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace DragnetAlphaWebAPI.Methods
{
    public class ExportToCSVMethod
    {
        public void ExportAsCSV(List<LinkEntity> relevantLinks, string filePath)
        {
            var cultureInfo = new System.Globalization.CultureInfo("en-US");

            using (var csv = new CsvWriter(new StreamWriter(filePath, false, Encoding.UTF8, 1), cultureInfo))
            {
                csv.WriteRecords(relevantLinks);
            }
        }

    }
}