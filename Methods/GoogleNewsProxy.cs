﻿using DragnetAlphaWebAPI.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web.Script.Serialization;

namespace DragnetAlphaWebAPI.Methods
{
    public class GoogleNewsProxy
    {
        public static List<NewsLink> GetGoogleNewsProxyLinks(int? daycount, SearchStringModel searchString, ThemeVsEventModel themeVsEventModel)
        {
            List<NewsLink> extractedGoogleDataEntities = new List<NewsLink>();

            try
            {
                List<string> proxyServersOfCountry = new List<string> { "bigg-us-drwk.tp-ns.com", "bigg-uk-fsvi.tp-ns.com" };
                string baseUrl = "https://news.google.com/search?hl=en-US&gl=US&ceid=US%3Aen&q=";
                string fullUrl = baseUrl + Uri.EscapeDataString(searchString.SearchString)+ $"%20when%3A1d";
                string searchPageContent = null;
                int i = 0;
                foreach (string eachProxyServer in proxyServersOfCountry)
                {
                    searchPageContent = GetContentUsingTrustedOld(fullUrl, eachProxyServer);
                    //   }

                    i++;
                    if (searchPageContent == null || searchPageContent.Contains("Our systems have detected unusual traffic from your computer network.  This page checks to "))
                    {
                        searchPageContent = "No content using google";
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                if (searchPageContent != null)
                {
                    extractedGoogleDataEntities = GetMetaDataWithResultsGoogleUpdated(searchPageContent, searchString.project_id, searchString.SearchTerm_id, themeVsEventModel, searchString);
                   return extractedGoogleDataEntities;
                }
                else
                  return null;
            }
            catch (Exception exception)
            {
                // Logger.Log().Error(exception);
                return null;
            }

        }
        public static List<NewsLink> GetMetaDataWithResultsGoogleUpdated(string searchPageContent, int projectId, int? SearchStringId, ThemeVsEventModel themeVsEventModel, SearchStringModel searchString)
        {
            List<NewsLink> ExtractedGoogleNewsMethods = new List<NewsLink>();
            
            //List<ExtractedGoogleDataModel> rejectedLinks = new List<ExtractedGoogleDataModel>();
            if (searchPageContent != null)
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(searchPageContent);
                HtmlNodeCollection shortUrlNodes = htmlDocument.DocumentNode.SelectNodes("//div[@class=\"xrnccd\"]");
                HtmlNodeCollection newsCardNodes = htmlDocument.DocumentNode.SelectNodes("//div[@class=\"xrnccd\"]");

                if (newsCardNodes != null)
                {
                    foreach (var newsCardNode in newsCardNodes)
                    {
                        NewsLink link = new NewsLink();
                        try
                        {
                            var nodes2 = newsCardNode.OuterHtml;
                            HtmlDocument htmlDocument1 = new HtmlDocument();
                            htmlDocument1.LoadHtml(newsCardNode.OuterHtml);
                            var nodes3 = htmlDocument1.DocumentNode.SelectNodes("//h3[@class=\"ipQwMb ekueJc RD0gLb\"]");
                            var heading = nodes3[0].InnerText;
                            var nodes4 = htmlDocument1.DocumentNode.SelectNodes("//h3[@class=\"ipQwMb ekueJc RD0gLb\"]/a[1]");
                            var url = nodes4[0].Attributes["href"].Value;
                            var NewsUrl = "https://news.google.com" + url;
                            var nodes5 = htmlDocument1.DocumentNode.SelectNodes("//div[@class=\"QmrVtf RD0gLb kybdz\"]//div[@class=\"SVJrMe\"]/a[1]");
                            var newsProvider = "";

                            if (nodes5 != null)
                            {
                                try
                                {
                                     newsProvider = nodes5[0].InnerText;
                                }
                                catch
                                {

                                }
                                                                           
                            var time = DateTime.Now;
                            var description = "";
                            try
                            {
                                if (nodes5[0].NextSibling != null)
                                {
                                    time = DateTime.Parse(nodes5[0].NextSibling.Attributes[1].Value);
                                    var timetag = nodes5[0].NextSibling.InnerText;
                                    var nodes6 = htmlDocument1.DocumentNode.SelectNodes("//div[@class=\"Da10Tb Rai5ob\"]//span[@class=\"xBbh9\"]");
                                    if (nodes6 != null)
                                        description = nodes6[0].InnerText;
                                    link = new NewsLink
                                    {
                                        Heading = heading,
                                        Link = NewsUrl,
                                        Description = description,
                                        DatePublished = time,
                                        NewsProvider = newsProvider,
                                        ImageUrl = "null",
                                        LinkCategory = "",
                                        SearchTerm_id = SearchStringId,
                                        project_id = projectId,
                                        ReviewStatus_Id = 0,
                                        Comment = "",
                                        LinkFlag_Id = 0,
                                        CreatedAt = DateTime.Now,
                                        ContentExtraction = "Yes",
                                        DuplicateFlag ="No",
                                        Sentiment_Script_Status=0,
                                        DeDupe_Script_Status=0,
                                        Summary_Script_Status=0,
                                        Content_Extraction_Script_Status=0,
                                        Relevance_Score_Status=0,
                                        Tags_Score_Status=0,
                                       TargetedEntity = searchString.TargetEntity,
                                       EventsTagged=searchString.EventTagged,
                                       Theme=searchString.Theme,
                                       ThemeVsEventJson = searchString.ThemeVsEventModelJson                                    
                                    };
                                    string hashcode = "null";
                                    using (MD5 md5Hash = MD5.Create())
                                    {
                                        hashcode = GetMd5Hash(md5Hash, link.Link);
                                    }
                                    link.LinkHashCode = hashcode;
                                    ExtractedGoogleNewsMethods.Add(link);
                                }
                                else
                                {
                                    //var nodes6 = htmlDocument1.DocumentNode.SelectNodes("//div[@class=\"Da10Tb Rai5ob\"]//span[@class=\"xBbh9\"]");
                                    //if (nodes6 != null)
                                    //    description = nodes6[0].InnerText;
                                    //link = new Link
                                    //{
                                    //    Heading = heading,
                                    //    Link = NewsUrl,
                                    //    Description = description,
                                    //    NewsProvider = newsProvider,
                                    //    ImageUrl = "null",
                                    //    LinkCategory = "",
                                    //    SearchTerm_id = SearchStringId,
                                    //    ProjectId = projectId,
                                    //    ReviewStatus_Id = 0,
                                    //    Comment = "",
                                    //    LinkFlag_Id = 0,
                                    //    CreatedAt = DateTime.Now
                                    //    //SubCategory = SubCategory,
                                    //    ////SubCategorySymbol = SubCategorySymbol
                                    //};
                                    //string hashcode = "null";
                                    //using (MD5 md5Hash = MD5.Create())
                                    //{
                                    //    hashcode = GetMd5Hash(md5Hash, link.Link);
                                    //}
                                    //link.LinkHashCode = hashcode;
                                    //ExtractedGoogleNewsMethods.Add(link);
                                }
                            }
                            catch
                            {

                            }
                            }
                            else
                            {
                                
                            }
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                }

            }
            //databaseMethodsObj.AddRejectedLinksToTable(xMLMethodsObj.ToXML(rejectedLinks));
            return ExtractedGoogleNewsMethods;
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            foreach (byte t in data)
            {
                sBuilder.Append(t.ToString("x2", CultureInfo.InvariantCulture));
            }
            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
        public static string GetContentUsingTrustedOld(string fullUrl, string server)
        {
            string useragent = GetRandomUserAgent();
            string searchPageContent = null;
            // //console.WriteLine(fullUrl);

            ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };
            WebProxy proxy = new WebProxy(server, 80)
            {
                BypassProxyOnLocal = false,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("altx", "0mATfxlU2g")
            };
            HttpClientHandler httpClientHandler = new HttpClientHandler()
            {
                Proxy = proxy,
                PreAuthenticate = true,
            };

            HttpClient client = new HttpClient(httpClientHandler, false);//
                                                                         // HttpClient client = new HttpClient();
            int retries = 2;
            bool success = false;
            Exception exc = new Exception();
            do
            {
                try
                {
                    //client.DefaultRequestHeaders.UserAgent.ParseAdd("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36");
                    client.DefaultRequestHeaders.UserAgent.ParseAdd(useragent);
                    HttpResponseMessage response = client.GetAsync(fullUrl).Result;
                    searchPageContent = response.Content.ReadAsStringAsync().Result;
                    success = true;
                }
                catch (Exception exe)
                {
                    //logger.Error("Trusted method: ", exe);
                    success = false;
                    retries--;
                    exc = exe;
                }
            } while (!success && retries > 0);
            if (!success)
            {
                return null;
            }
            return searchPageContent;
        }

        private static readonly string[] userAgents =
{
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.107",
                "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0",
                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.122 Safari/537.36 Vivaldi/2.3.1440.61",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36 OPR/62.0.3331.72"
        };
        private static string GetRandomUserAgent()
        {
            return userAgents[new Random().Next(userAgents.Length - 1)];
        }

        public static List<NewsLink> GetGoogleLinks(string searchString, int projectId, int SearchStringId, int? daycount, SearchTermMapping allSearchString, ThemeVsEventModel themeVsEventModel, List<EventList> EventLists, string ThemeVsEventjson)
        {
            int days = Convert.ToInt32(35);
            List<NewsLink> extractedGoogleDataEntities = new List<NewsLink>();
            DateTime today = DateTime.Today;
            string todayDate = today.ToString("yyyy-MM-dd");
            DateTime weekAgo = DateTime.Today.AddDays(-days);
            string weekAgoDate = weekAgo.ToString("yyyy-MM-dd");
            searchString = $"{searchString} after:{weekAgoDate} before:{todayDate}";
            try
            {
                List<string> proxyServersOfCountry = new List<string> { "bigg-us-drwk.tp-ns.com", "bigg-uk-fsvi.tp-ns.com", "bigg-fr-tapm.tp-ns.com", "bigg-de-bfuh.tp-ns.com" };
                string baseUrl = "https://www.google.com/search?q=";
                string fullUrl = baseUrl + Uri.EscapeDataString(searchString);
                string searchPageContent = null;
                int i = 0;
                foreach (string eachProxyServer in proxyServersOfCountry)
                {
                    searchPageContent = GetContentUsingTrustedOld(fullUrl, eachProxyServer);

                    i++;
                    if (searchPageContent == null || searchPageContent.Contains("Our systems have detected unusual traffic from your computer network.  This page checks to "))
                    {
                        searchPageContent = "No content using google";
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                if (searchPageContent != null)
                    extractedGoogleDataEntities = GetMetaDataWithResultsOpenGoogleUpdated(searchPageContent, searchString, projectId, SearchStringId, allSearchString, themeVsEventModel, EventLists, ThemeVsEventjson);
                else
                {

                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            return extractedGoogleDataEntities;
        }
        public static List<NewsLink> GetMetaDataWithResultsOpenGoogleUpdated(string searchPageContent, string searchString, int projectId, int SearchStringId, SearchTermMapping allSearchString, ThemeVsEventModel themeVsEventModel, List<EventList> EventLists, string ThemeVsEventjson)
        {
            List<NewsLink> ExtractedGoogleDataModels = new List<NewsLink>();
            //List<ExtractedGoogleDataModel> rejectedLinks = new List<ExtractedGoogleDataModel>();
            if (searchPageContent != null)
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(searchPageContent);
                HtmlNodeCollection shortUrlNodes = htmlDocument.DocumentNode.SelectNodes("//div[@class=\"g\"]");

                if (shortUrlNodes != null && shortUrlNodes.Count > 0)
                {
                   
                    foreach (HtmlNode subhtml in shortUrlNodes)
                    {
                        
                             
                        NewsLink extractedGoogleDataMethod = new NewsLink()
                        {
                            SearchTerm_id = SearchStringId,
                            ImageUrl = "null",
                            LinkCategory = "",
                            project_id = projectId,
                            ReviewStatus_Id = 0,
                            Comment = "",
                            LinkFlag_Id = 0,
                            CreatedAt = DateTime.Now,
                            NewsProvider = "Open Google Search",
                            //TargetedEntity = allSearchString.SubCategorySymbol,
                            //EventsTagged = allSearchString.SubCategory,
                            //Theme = allSearchString.Category,
                            //ThemeVsEventJson= ThemeVsEventjson
                        };
                        HtmlDocument htmlDocument1 = new HtmlDocument();
                        htmlDocument1.LoadHtml(subhtml.OuterHtml);

                        //HEADING
                        HtmlNodeCollection shortUrlNodes1 = htmlDocument1.DocumentNode.SelectNodes("//div[@class='rc']//h3");
                        if (shortUrlNodes1 != null && shortUrlNodes1.Count > 0)
                        {
                            extractedGoogleDataMethod.Heading = shortUrlNodes1[0].InnerText;
                        }
                        else
                        {
                            extractedGoogleDataMethod.Heading = "-";
                        }

                        //DESCRIPTION
                        HtmlNodeCollection shortUrlNodes2 = htmlDocument1.DocumentNode.SelectNodes("//div[@class='rc']//span[@class='aCOpRe']");
                        if (shortUrlNodes2 != null && shortUrlNodes2.Count > 0)
                        {
                            //foreach (HtmlNode subhtml2 in shortUrlNodes2)
                            //{
                            extractedGoogleDataMethod.Description = shortUrlNodes2[0].InnerText;
                            //}
                        }
                        else
                        {
                            extractedGoogleDataMethod.Description = "-";
                        }

                        //LINK
                        HtmlNodeCollection shortUrlNodes3 = htmlDocument1.DocumentNode.SelectNodes("//div[@class=\"rc\"]/div[@class=\"yuRUbf\"]/a[1]");
                        if (shortUrlNodes3 != null && shortUrlNodes3.Count > 0)
                        {
                            //foreach (HtmlNode subhtml3 in shortUrlNodes3)
                            //{
                            extractedGoogleDataMethod.Link = shortUrlNodes3[0].Attributes["href"].Value;
                            //}
                        }
                        else
                        {
                            extractedGoogleDataMethod.Link = "-";
                        }


                        ////SHORT-LINK
                        //HtmlNodeCollection shortUrlNodes4 = htmlDocument1.DocumentNode.SelectNodes("//div[@class=\"rc\"]//div[@class=\"yuRUbf\"]//cite");
                        //if (shortUrlNodes4 != null && shortUrlNodes4.Count > 0)
                        //{
                        //    //foreach (HtmlNode subhtml4 in shortUrlNodes4)
                        //    //{
                        //    extractedGoogleDataMethod.DisplayLink = shortUrlNodes4[0].InnerText;
                        //    //}
                        //}
                        //else
                        //{
                        //    extractedGoogleDataMethod.DisplayLink = "-";
                        //}

                        //Date Published
                        HtmlNodeCollection shortUrlNodes5 = htmlDocument1.DocumentNode.SelectNodes("//div[@class='rc']//span[@class='aCOpRe']//span[@class='f']");
                        if (shortUrlNodes5 != null && shortUrlNodes5.Count > 0)
                        {
                            try
                            {
                                string DateTimeFormat = shortUrlNodes5[0].InnerText.Replace(" — ", "");
                                DateTime dateTime = Convert.ToDateTime(DateTimeFormat, CultureInfo.InvariantCulture);

                                //DateTime dateTime = DateTime.Parse(DateTimeFormat);
                                extractedGoogleDataMethod.DatePublished = dateTime;
                            }
                            catch(Exception ex)
                            {

                            }


                        }
                        else
                        {
                            
                        }
                        using (MD5 md5Hash = MD5.Create())
                        {
                            extractedGoogleDataMethod.LinkHashCode = GetMd5Hash(md5Hash, extractedGoogleDataMethod.Link);
                        }

                        ExtractedGoogleDataModels.Add(extractedGoogleDataMethod);
                    }
                }
            }
            //databaseMethodsObj.AddRejectedLinksToTable(xMLMethodsObj.ToXML(rejectedLinks));
            return ExtractedGoogleDataModels;
        }    
    }
}