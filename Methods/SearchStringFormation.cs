﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;

namespace DragnetAlphaWebAPI.Methods
{
    public class SearchStringFormation
    {
        public List<SearchStringModel> GenerateSearchString(int projectId, List<string> NewsSource, List<ProjectNewsSourceMapping> NewsSource1)
        {
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                var targetEntity = dbContext.TargetEntities.Where(x => x.project_id == projectId && x.ActiveStatus == true).ToList();

                var alreadysearchStringMade = dbContext.ReadySearchStrings.Where(x => x.project_id == projectId).ToList();
                if (alreadysearchStringMade != null && alreadysearchStringMade.Count > 0)
                {
                    var searchstringformatLists = dbContext.ReadySearchStrings.Include("SearchTerm").Where(x => x.project_id == projectId).ToList();
                    try
                    {
                        List<SearchStringModel> STGOutputList = new List<SearchStringModel>();
                        foreach (var entity in targetEntity)
                        {
                            foreach (var searchstringformatList in searchstringformatLists)
                            {
                                STGOutputList.Add(new SearchStringModel
                                {
                                    SearchString = searchstringformatList.SearchString,
                                    SearchTerm_id = searchstringformatList.SearchTerm_id,
                                    TargetEntityId = entity.id,
                                    TargetEntity = entity.TargetedEntity,
                                    EventId = searchstringformatList.SearchTerm.Event_id,
                                    ThemeId = searchstringformatList.SearchTerm.EventList.ThemeList.Id,
                                    project_id = projectId,
                                    EventTagged = searchstringformatList.SearchTerm.EventList.EventName,
                                    Theme = searchstringformatList.SearchTerm.EventList.ThemeList.ThemeName,
                                    CountryCode = entity.CountryCode
                                });

                                RSSFeedScriptInput rSSFeedScriptInput = new RSSFeedScriptInput
                                {
                                    SearchString = searchstringformatList.SearchString,
                                    SearchTerm_id = searchstringformatList.SearchTerm_id,
                                    TargetEntityId = entity.id,
                                    TargetedEntity = entity.TargetedEntity,
                                    EventId = searchstringformatList.SearchTerm.Event_id,
                                    ThemeId = searchstringformatList.SearchTerm.EventList.ThemeList.Id,
                                    project_id = projectId,
                                    EventTagged = searchstringformatList.SearchTerm.EventList.EventName,
                                    Theme = searchstringformatList.SearchTerm.EventList.ThemeList.ThemeName,
                                    CountryCode = entity.CountryCode
                                };
                                if (NewsSource.Contains("RSS Feeds") && rSSFeedScriptInput != null)
                                {
                                    dbContext.RSSFeedScriptInputs.Add(rSSFeedScriptInput);
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                        return STGOutputList;
                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                {
                    List<SearchStringModel> STGOutputList1 = new List<SearchStringModel>();

                    foreach (var newssource in NewsSource1)
                    {
                        try
                        {

                            List<InvestingComScriptInput> investingComScriptInputs = new List<InvestingComScriptInput>();
                            List<DataBreachScriptInput> DataBreachScriptInputs = new List<DataBreachScriptInput>();

                            if (newssource.NewsSource.ScriptType == "rss_feed")
                            {
                                foreach (var entity in targetEntity)
                                {
                                    var id = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.Id).ToList().FirstOrDefault();


                                    var AlllanguageCode = entity.CountryCode;
                                    var Languagecode = AlllanguageCode.Split(',').ToList();
                                    foreach (var LanCode in Languagecode)
                                    {
                                        List<SearchStringModel> STGOutputList = new List<SearchStringModel>();

                                        var searchTerms = dbContext.SearchTerms.Where(x => x.Project_id == projectId && x.status == true && x.CountryCode == LanCode).ToList();
                                        var OtherNames = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.OtherNamesforSearch).ToList().FirstOrDefault();
                                        var TradeName = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.TradeName).ToList().FirstOrDefault();
                                        var ParentOrganisationName = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.ParentOrganisation).ToList().FirstOrDefault();
                                        var SubsidiariesName = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.Subsidiaries).ToList().FirstOrDefault();

                                        var OtherNamesClean = OtherNames.Replace("[", "").Replace("]", "").Replace(", ", "#");
                                        var OtherNamesClean_1 = OtherNamesClean.Split('#');

                                        var SubsidiariesNameClean = SubsidiariesName.Replace("[", "").Replace("]", "").Replace(", ", "#");
                                        var SubsidiariesNameClean_1 = SubsidiariesNameClean.Split('#');

                                        HashSet<string> entityNames = new HashSet<string>();
                                        entityNames.Add(entity.TargetedEntity);
                                        if (TradeName != "-")
                                        {
                                            entityNames.Add(TradeName);
                                        }
                                        if (ParentOrganisationName != "-")
                                        {
                                            entityNames.Add(ParentOrganisationName);
                                        }

                                        foreach (var otherNamesclean in OtherNamesClean_1)
                                        {
                                            if (otherNamesclean != "-")
                                                entityNames.Add(otherNamesclean);
                                        }
                                        foreach (var subsidiariesNameClean in SubsidiariesNameClean_1)
                                        {
                                            if (subsidiariesNameClean != "-")
                                                entityNames.Add(subsidiariesNameClean);
                                        }
                                        var list = entityNames.Distinct().ToList();
                                        foreach (var names in entityNames)
                                        {
                                            if (newssource.SearchTermRequired == true)
                                            {
                                                foreach (var searchTerm in searchTerms)
                                                {

                                                    STGOutputList.Add(new SearchStringModel
                                                    {
                                                        SearchString = String.Format(newssource.SearchStringFormatMapping, names, searchTerm.SearchTerm1),
                                                        SearchTerm_id = searchTerm.id,
                                                        TargetEntityId = id,
                                                        TargetEntity = entity.TargetedEntity,
                                                        EventId = searchTerm.EventList.Id,
                                                        ThemeId = searchTerm.EventList.ThemeList.Id,
                                                        project_id = projectId,
                                                        EventTagged = searchTerm.EventList.EventName,
                                                        Theme = searchTerm.EventList.ThemeList.ThemeName,
                                                        CountryCode = LanCode,
                                                        ScriptURL = newssource.NewsSource.ScriptURL,
                                                        KeyMapping = newssource.NewsSource.KeyMapping
                                                    });
                                                }
                                                STGOutputList.AddRange(AdditionalString(entity, newssource.SearchStringFormatMapping, projectId, LanCode, newssource.NewsSource.ScriptURL, id, names));
                                            }
                                            else if (newssource.SearchTermRequired == false)
                                            {

                                                STGOutputList.Add(new SearchStringModel
                                                {
                                                    SearchString = String.Format(newssource.SearchStringFormatMapping, names),
                                                    SearchTerm_id = 229,
                                                    TargetEntityId = id,
                                                    TargetEntity = entity.TargetedEntity,
                                                    project_id = projectId,
                                                    EventTagged = "-",
                                                    Theme = "-",
                                                    CountryCode = LanCode,
                                                    ScriptURL = newssource.NewsSource.ScriptURL

                                                });

                                            }

                                        }
                                        var stringXML = ToXML(STGOutputList);
                                        dbContext.insOnSTGCompletion_xml(stringXML);
                                        STGOutputList1.AddRange(STGOutputList);
                                    }

                                }

                            }
                            else if (newssource.NewsSource.ScriptType == "Selenium")
                            {
                                foreach (var entity in targetEntity)
                                {
                                    var id = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.Id).ToList().FirstOrDefault();

                                    var searchTerms = dbContext.SearchTerms.Where(x => x.Project_id == projectId && x.status == true).ToList();
                                    var OtherNames = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.OtherNamesforSearch).ToList().FirstOrDefault();
                                    //var TradeName = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.TradeName).ToList().FirstOrDefault();
                                    var ParentOrganisationName = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.ParentOrganisation).ToList().FirstOrDefault();

                                    var OtherNamesClean = OtherNames.Replace("[", "").Replace("]", "").Replace(", ", "#");
                                    var OtherNamesClean_1 = OtherNamesClean.Split('#');

                                    HashSet<string> entityNames = new HashSet<string>();
                                    entityNames.Add(entity.TargetedEntity);
                                    //if (TradeName != "-")
                                    //{
                                    //    entityNames.Add(TradeName);
                                    //}
                                    if (ParentOrganisationName != "-")
                                    {
                                        entityNames.Add(ParentOrganisationName);
                                    }

                                    foreach (var alias in OtherNamesClean_1)
                                    {
                                        if (alias != "-")
                                            entityNames.Add(alias);
                                    }
                                    var list = entityNames.Distinct().ToList();
                                    foreach (var name in list)
                                    {

                                        investingComScriptInputs.Add(new InvestingComScriptInput
                                        {
                                            SearchString = name,
                                            SearchTerm_id = 229,
                                            TargetEntityId = id,
                                            TargetedEntity = entity.TargetedEntity,
                                            project_id = projectId,
                                            EventTagged = "-",
                                            Theme = "-",
                                            CountryCode = "us"

                                        });
                                    }
                                    try
                                    {
                                        var stringXML_Investing = ToXML_Investing(investingComScriptInputs);
                                        dbContext.insOnSTGCompletionInvesting_xml(stringXML_Investing);
                                    }
                                    catch (Exception ex)
                                    {

                                    }

                                }
                            }
                            else if (newssource.NewsSource.ScriptType == "html")
                            {
                                foreach (var entity in targetEntity)
                                {
                                    DataBreachScriptInputs.Add(new DataBreachScriptInput
                                    {
                                        SearchString = entity.TargetedEntity,
                                        SearchTerm_id = 229,
                                        TargetEntityId = entity.id,
                                        TargetedEntity = entity.TargetedEntity,
                                        project_id = projectId,
                                        EventTagged = "-",
                                        Theme = "-",
                                        CountryCode = "us"
                                    });
                                }
                                try
                                {
                                    var stringXML_Investing = ToXML_DataBreacch(DataBreachScriptInputs);
                                    dbContext.insOnSTGCompletionDataBreacheCom_xml(stringXML_Investing);
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //return null;
                        }
                    }
                    return STGOutputList1;
                }
            }
        }
        public List<SearchStringModel> GenerateSearchStringEntitySpecific(int projectId, List<string> NewsSource, List<ProjectNewsSourceMapping> NewsSource1, string targetedEntity)
        {
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                //List<>
                var targetEntity = dbContext.TargetEntities.Where(x => x.project_id == projectId && x.ActiveStatus == true && x.TargetedEntity== targetedEntity).ToList();
                //var targetEntity = dbContext.TargetEntities.Where(x => x.project_id == projectId && x.ActiveStatus == true).ToList();
                var alreadysearchStringMade = dbContext.ReadySearchStrings.Where(x => x.project_id == projectId).ToList();
                if (alreadysearchStringMade != null && alreadysearchStringMade.Count > 0)
                {
                    var searchTerms = dbContext.SearchTerms.Where(x => x.Project_id == projectId).ToList();
                    var searchstringformatLists = dbContext.ReadySearchStrings.Include("SearchTerm").Where(x => x.project_id == projectId).ToList();
                    try
                    {
                        List<SearchStringModel> STGOutputList = new List<SearchStringModel>();
                        foreach (var entity in targetEntity)
                        {
                            foreach (var searchstringformatList in searchstringformatLists)
                            {
                                STGOutputList.Add(new SearchStringModel
                                {
                                    SearchString = searchstringformatList.SearchString,
                                    SearchTerm_id = searchstringformatList.SearchTerm_id,
                                    TargetEntityId = entity.id,
                                    TargetEntity = entity.TargetedEntity,
                                    EventId = searchstringformatList.SearchTerm.Event_id,
                                    ThemeId = searchstringformatList.SearchTerm.EventList.ThemeList.Id,
                                    project_id = projectId,
                                    EventTagged = searchstringformatList.SearchTerm.EventList.EventName,
                                    Theme = searchstringformatList.SearchTerm.EventList.ThemeList.ThemeName,
                                    CountryCode = entity.CountryCode
                                });
                            }
                        }
                        return STGOutputList;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
                else
                {
                    var searchTerms = dbContext.SearchTerms.Where(x => x.Project_id == projectId).ToList();
                    var searchstringformatList = dbContext.SearchStringFormatMappings.Where(x => x.project_id == projectId).Select(y => y.SearchStringFormat).ToList();
                    try
                    {
                        string searchStringFormat = searchstringformatList[0].SearchstringFormat1;
                        List<SearchStringModel> STGOutputList = new List<SearchStringModel>();
                        foreach (var entity in targetEntity)
                        {
                            foreach (var searchTerm in searchTerms)
                            {
                                STGOutputList.Add(new SearchStringModel
                                {
                                    SearchString = String.Format(searchStringFormat, entity.TargetedEntity, searchTerm.SearchTerm1),
                                    SearchTerm_id = searchTerm.id,
                                    TargetEntityId = entity.id,
                                    TargetEntity = entity.TargetedEntity,
                                    EventId = searchTerm.EventList.Id,
                                    ThemeId = searchTerm.EventList.ThemeList.Id,
                                    project_id = projectId,
                                    EventTagged = searchTerm.EventList.EventName,
                                    Theme = searchTerm.EventList.ThemeList.ThemeName,
                                    CountryCode = entity.CountryCode
                                });
                            }
                        }
                        return STGOutputList;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }


        }
        public static List<SearchStringModel> AdditionalString(TargetEntity entity, string searchStringFormat, int projectId, string LanCode, string ScriptUrl, int CompanyProfileId, string name)
        {
            List<SearchStringModel> AdditionalStringList = new List<SearchStringModel>();
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                var CEOName = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.CEO).ToList().FirstOrDefault();
                var CFOName = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.CFO).ToList().FirstOrDefault();
                var BODName = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.BoardofDirectors).ToList().FirstOrDefault();
                var FoundersName = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.Founders).ToList().FirstOrDefault();
                var headQuaters = dbContext.CompanyProfiles.Where(x => x.EntityName == entity.TargetedEntity && x.Project_id == projectId).Select(x => x.Headquarters).ToList().FirstOrDefault();
                List<string> AdditionalStrings = new List<string>();
                if (CEOName != "-" && CEOName != null)
                {
                    AdditionalStrings.Add(CEOName);
                }
                if (CFOName != "-" && CFOName != null)
                {
                    AdditionalStrings.Add(CFOName);
                }
                try
                {
                    if (BODName != "-" && BODName != null)
                    {
                        BODName = BODName.Replace("[", "");
                        BODName = BODName.Replace("]", "");
                        BODName = BODName.Replace(", ", "#");
                        var BODNameList = BODName.Split('#');
                        foreach (var bODName in BODNameList)
                        {
                            AdditionalStrings.Add(bODName);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                try
                {
                    if (FoundersName != "-" && FoundersName != null)
                    {
                        FoundersName = FoundersName.Replace("[", "");
                        FoundersName = FoundersName.Replace("]", "");
                        FoundersName = FoundersName.Replace(", ", "#");
                        var FoundersNameList = FoundersName.Split('#');
                        foreach (var foundersName in FoundersNameList)
                        {
                            AdditionalStrings.Add(foundersName);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                if (headQuaters != "-" && headQuaters != null)
                {
                    AdditionalStrings.Add(headQuaters);
                }
                if (AdditionalStrings != null && AdditionalStrings.Count() != 0)
                {
                    foreach (string term in AdditionalStrings)
                    {
                        if (term != "-" && term != "")
                        {
                            AdditionalStringList.Add(new SearchStringModel
                            {
                                SearchString = String.Format(searchStringFormat, name, term),
                                SearchTerm_id = 229,
                                TargetEntityId = CompanyProfileId,
                                TargetEntity = entity.TargetedEntity,
                                project_id = projectId,
                                EventTagged = "",
                                Theme = "",
                                CountryCode = LanCode,
                                ScriptURL = ScriptUrl
                            });

                        }

                    }
                }
                AdditionalStringList.Add(new SearchStringModel
                {
                    SearchString = entity.TargetedEntity,
                    SearchTerm_id = 229,//Need for DB
                    TargetEntityId = CompanyProfileId,
                    TargetEntity = entity.TargetedEntity,
                    project_id = projectId,
                    EventTagged = "",
                    Theme = "",
                    CountryCode = LanCode,
                    ScriptURL = ScriptUrl
                });
            }
            return AdditionalStringList;
        }
        public static string ToXML(List<SearchStringModel> obj)
        {
            try
            {
                using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<SearchStringModel>));
                    xmlSerializer.Serialize(stringWriter, obj);
                    return stringWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static string ToXML_Investing(List<InvestingComScriptInput> obj)
        {
            try
            {
                using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<InvestingComScriptInput>));
                    xmlSerializer.Serialize(stringWriter, obj);
                    return stringWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static string ToXML_DataBreacch(List<DataBreachScriptInput> obj)
        {
            try
            {
                using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<DataBreachScriptInput>));
                    xmlSerializer.Serialize(stringWriter, obj);
                    return stringWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}