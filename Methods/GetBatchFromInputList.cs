﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Methods
{
    public class GetBatchFromInputList
    {

        public static List<SearchStringModel> GetBatchFromInputListForProcessing(List<SearchStringModel> AllSearchStringInputs)
        {
            List<SearchStringModel> stringsToBePorcessed;
            if (AllSearchStringInputs.Count >= 30)
            {
                stringsToBePorcessed = AllSearchStringInputs.GetRange(0, 30).ToList();
            }
            else
            {
                stringsToBePorcessed = AllSearchStringInputs.ToList();
            }
            AllSearchStringInputs.RemoveAll(x => stringsToBePorcessed.Contains(x));
            return stringsToBePorcessed;
        }
    }
}