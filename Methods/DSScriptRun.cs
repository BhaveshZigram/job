﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace DragnetAlphaWebAPI.Methods
{
    public class DSScriptRun
    {
        public static async void DedupeLinksAsync(int projectId)
        {
            WebClient wc = new WebClient();
            string DedupeAPIUrl = $"https://testpythondragnetalpha.azurewebsites.net/deduplication?ProjectId=" + projectId;
            try
            {
                string cRBNews = wc.DownloadString(DedupeAPIUrl);
            }
            catch
            {

            }
        }
        public static async void SummarizeAsync(int projectId)
        {
            try
            {
                string result = Path.GetTempPath();

                string filename = result + "DA_summarizer.py";
                string filename1 = result + "DA_Relevant_Key_words_updated_07_09_2020_v1.pkl";

                BlobMethods.ReadBlobFile("DA_summarizer.py", "dragnet-alpha/DataScience/Summarizer", filename);
                BlobMethods.ReadBlobFile("DA_Relevant_Key_words_updated_07_09_2020_v1.pkl", "dragnet-alpha/DataScience/Summarizer", filename1);
              
                string error = "";
                Console.WriteLine($"Summarization Started at {DateTime.Now}");
                Process p = new Process();
                //ProcessStartInfo start = new ProcessStartInfo(@"D://home//python364x64//pythE:\DragnetAlpha\da_en_apisversion2\Methods\BlobMethods.cson.exe")
                //ProcessStartInfo start = new ProcessStartInfo(@"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64\python.exe")
                //{
                //    Arguments = string.Format("\"{0}\" \"{1}\"", filename, projectId),
                //    UseShellExecute = false,

                //};

                ////   Console.WriteLine("Script Started");
                //using (System.Diagnostics.Process process = Process.Start(start))
                //{
                //    process.WaitForExit();
                //}
                p.StartInfo = new ProcessStartInfo(@"D://home//python364x64//python.exe")
                //p.StartInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64\python.exe")
                {
                    Arguments = string.Format("\"{0}\" \"{1}\"", filename, projectId),
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardError = true
                };
                p.Start();
                error = p.StandardError.ReadToEnd();
                //Console.WriteLine(error);
                string output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                Console.WriteLine($"Summarization Finished at {DateTime.Now}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}