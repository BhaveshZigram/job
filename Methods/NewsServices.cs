﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DragnetAlphaWebAPI.Methods
{
    public class NewsServices
    {
        public static void BingNewsExtract(List<string> NewsSource, List<SearchStringModel> StgOutput, int linkCount, int? daycount, ThemeVsEventModel themeVsEventModel, int projectId)
        {
            SearchNewsBing searchNewsBing = new SearchNewsBing();
            if (NewsSource.Contains("Bing News"))
            {
                while (StgOutput != null && StgOutput.Count > 0)
                {
                    List<SearchStringModel> allinputListBatchForProcessing = new List<SearchStringModel>();
                    allinputListBatchForProcessing.AddRange(GetBatchFromInputList.GetBatchFromInputListForProcessing(StgOutput));
                    List<NewsLink> allLink = new List<NewsLink>();

                    Parallel.ForEach(allinputListBatchForProcessing, new ParallelOptions { MaxDegreeOfParallelism = 20 }, searchString =>
                    {
                        List<NewsLink> bingResult = new List<NewsLink>();
                        List<NewsLink> googleResult = new List<NewsLink>();
                        List<NewsLink> SocialMedia = new List<NewsLink>();
                        List<NewsLink> googleNewsProxyResult = new List<NewsLink>();
                        List<NewsLink> OpengoogleResult = new List<NewsLink>();
                        try
                        {
                            bingResult = SearchNewsBing.BingNewsSearch(linkCount, searchString);
                        }
                        catch (Exception ex)
                        {

                        }
                        //try
                        //{
                        //    googleResult = SearchNewsBing.GoogleNewsLinks(searchStrings1.SearchString, searchStrings1.SubCategory, projectId, searchStrings1.SearchStringId);
                        //}
                        //catch (Exception ex)
                        //{

                        //}
                        //try
                        //{
                        //    SocialMedia = SearchNewsBing.SocialMediaNews(searchStrings1.SearchString, searchStrings1.SubCategory, projectId, searchStrings1.SearchStringId);
                        //}
                        //catch (Exception ex)
                        //{

                        //}

                        //try
                        //{
                        //    googleNewsProxyResult = GoogleNewsProxy.GetGoogleNewsProxyLinks(daycount, searchString, themeVsEventModel);
                        //}
                        //catch (Exception ex)
                        //{

                        //}
                        //if (projectId == 59)
                        //{
                        //    try
                        //    {
                        //        var events = themeVsEventModel.ThemeVsEvent;
                        //        if (events.Select(o => o.ThemeId == searchString.ThemeId).Count() > 0)
                        //        {
                        //            foreach (var x in events)
                        //            {
                        //                if (x.ThemeName == searchString.Category)
                        //                {
                        //                    x.ThemeAvailable = true;
                        //                    foreach (var y in x.EventList)
                        //                    {
                        //                        if (y.EventName == searchString.SubCategory)
                        //                        {
                        //                            y.EventAvailable = true;
                        //                        }
                        //                        else
                        //                        {
                        //                            y.EventAvailable = false;
                        //                        }
                        //                    }
                        //                }
                        //                else
                        //                {
                        //                    x.ThemeAvailable = false;
                        //                }
                        //            }
                        //        }
                        //        //events = events.Select(x =>
                        //        //{
                        //        //    if (x.ThemeName == searchStrings1.Category && x.EventList.Where(d => d.EventName == searchStrings1.SubCategory).Count() == 0)
                        //        //    {
                        //        //        try
                        //        //        {
                        //        //            var z = x.EventList.Select(y => y.EventName == searchStrings1.SubCategory).FirstOrDefault();

                        //        //        }
                        //        //        catch (Exception ex)
                        //        //        {

                        //        //        }
                        //        //    }
                        //        //    x.EventList = x.EventList.DistinctBy(p => new { p.EventId, p.EventName }).ToList();
                        //        //    return x;
                        //        //}).ToList();

                        //        //var ThemeVsEventjson = new JavaScriptSerializer().Serialize(events);
                        //        //OpengoogleResult = GoogleNewsProxy.GetGoogleLinks(searchStrings1.SearchString, projectId, searchStrings1.SearchStringId, daycount, searchStrings1, themeVsEventModel, EventLists, ThemeVsEventjson);
                        //    }
                        //    catch (Exception ex)
                        //    {

                        //    }
                        //}
                        if (bingResult == null || bingResult.Count == 0)
                        {
                            Console.WriteLine();
                        }
                        else
                        {
                            allLink.AddRange(bingResult);
                        }
                        ////if (googleResult == null || googleResult.Count == 0)
                        ////{
                        ////    Console.WriteLine();

                        ////}
                        ////else
                        ////{
                        ////    allLink.AddRange(googleResult);
                        ////}
                        ////if (SocialMedia == null || SocialMedia.Count == 0)
                        ////{
                        ////    Console.WriteLine();
                        ////}
                        ////else
                        ////{
                        ////    allLink.AddRange(SocialMedia);
                        ////}
                        //if (googleNewsProxyResult == null || googleNewsProxyResult.Count == 0)
                        //{
                        //    Console.WriteLine();
                        //}
                        //else
                        //{
                        //    allLink.AddRange(googleNewsProxyResult);

                        //}
                        //if (OpengoogleResult == null || OpengoogleResult.Count == 0)
                        //{
                        //    Console.WriteLine();
                        //}
                        //else
                        //{
                        //    try
                        //    {
                        //        allLink.AddRange(OpengoogleResult);
                        //    }
                        //    catch (Exception ex)
                        //    {

                        //    }
                        //}
                    });
                    searchNewsBing.InsertToDBAsync(allLink, projectId, "Bing News");
                }
                //   List<NewsLink> finalResult = SearchNewsBing.MergerDuplicateLink(allLink);
                List<Task> tasks = new List<Task>();
                Task taskA = Task.Run(() =>
                {
                    //searchNewsBing.InsertToDBAsync(allLink);
                    //DSScriptRun.DedupeLinksAsync(projectId);
                    DataBaseMethod.UpdateGroupingId(projectId);
                    //System.Threading.Thread.Sleep(30000); //30Seconds
                    //ContentExtractionMethod.ExtractContent(projectId);
                    //DSScriptRun.SummarizeAsync(projectId);
                });
                searchNewsBing.UpdateNewsExtractionStatus(projectId);
            }
        }
        public static void GoogleNewsExtract(List<string> NewsSource, List<SearchStringModel> StgOutput, int linkCount, int? daycount, ThemeVsEventModel themeVsEventModel, int projectId)
        {
            SearchNewsBing searchNewsBing = new SearchNewsBing();
            if (NewsSource.Contains("GoogleNews"))
            {
                while (StgOutput != null && StgOutput.Count > 0)
                {
                    List<SearchStringModel> allinputListBatchForProcessing = new List<SearchStringModel>();
                    allinputListBatchForProcessing.AddRange(GetBatchFromInputList.GetBatchFromInputListForProcessing(StgOutput));
                    List<NewsLink> allLink = new List<NewsLink>();

                    Parallel.ForEach(allinputListBatchForProcessing, new ParallelOptions { MaxDegreeOfParallelism = 20 }, searchString =>
                    {
                        List<NewsLink> googleResult = new List<NewsLink>();

                        List<NewsLink> googleNewsProxyResult = new List<NewsLink>();
                        List<NewsLink> OpengoogleResult = new List<NewsLink>();

                        try
                        {
                            googleResult = SearchNewsBing.GoogleNewsLinks(linkCount, searchString);
                        }
                        catch (Exception ex)
                        {

                        }


                        try
                        {
                            googleNewsProxyResult = GoogleNewsProxy.GetGoogleNewsProxyLinks(daycount, searchString, themeVsEventModel);
                        }
                        catch
                        {

                        }

                        if (googleNewsProxyResult == null || googleNewsProxyResult.Count == 0)
                        {
                            Console.WriteLine();
                        }
                        else
                        {
                            allLink.AddRange(googleNewsProxyResult);

                        }
                        if (OpengoogleResult == null || OpengoogleResult.Count == 0)
                        {
                            Console.WriteLine();
                        }
                        else
                        {
                            try
                            {
                                allLink.AddRange(OpengoogleResult);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    });
                    searchNewsBing.InsertToDBAsync(allLink, projectId, "GoogleNews");
                    DataBaseMethod.UpdateGroupingId(projectId);
                }
                //   List<NewsLink> finalResult = SearchNewsBing.MergerDuplicateLink(allLink);
                //List<Task> tasks = new List<Task>();
                //Task taskA = Task.Run(() =>
                //{
                //    //searchNewsBing.InsertToDBAsync(allLink);
                //    //DSScriptRun.DedupeLinksAsync(projectId);
                //    //DataBaseMethod.UpdateGroupingId(projectId);
                //    //System.Threading.Thread.Sleep(30000); //30Seconds
                //    //ContentExtractionMethod.ExtractContent(projectId);
                //    //DSScriptRun.SummarizeAsync(projectId);
                //});
            }
        }
        public static string RSSFeedNewsExtract(int projectId)
        {
            string output = "";
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                var NewsSource = dbContext.ProjectNewsSourceMappings.Where(x => x.project_id == projectId).Where(z => z.Active_Status == true).Select(y => y.NewsSource.ScriptType).ToList();
                var daycount = dbContext.DecisionParameters.Where(x => x.project_id == projectId).Select(y => y.NewsExtract_Day_Threshold).FirstOrDefault();

                SearchNewsBing searchNewsBing = new SearchNewsBing();
                if (NewsSource.Contains("rss_feed"))
                {
                    try
                    {
                        output = RSSFeedScript.RSSScript(projectId, daycount);
                        searchNewsBing.UpdateNewsExtractionStatus(projectId);
                        return output;
                    }
                    catch (Exception ex)
                    {
                        return output;

                    }
                    //DataBaseMethod.UpdateGroupingId(projectId);
                }
                return output;
            }
        }
        public static string DataBreachExtract(int projectId)
        {
            string output = "";

            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                var NewsSource = dbContext.ProjectNewsSourceMappings.Where(x => x.project_id == projectId).Where(z => z.Active_Status == true).Select(y => y.NewsSource.ScriptType).ToList();
                var daycount = dbContext.DecisionParameters.Where(x => x.project_id == projectId).Select(y => y.NewsExtract_Day_Threshold).FirstOrDefault();

                SearchNewsBing searchNewsBing = new SearchNewsBing();
                if (NewsSource.Contains("html"))
                {
                    output = RSSFeedScript.DataBreachcript(projectId, daycount);
                    //searchNewsBing.UpdateNewsExtractionStatus(projectId);
                    //DataBaseMethod.UpdateGroupingId(projectId);
                    return output;

                }
                return output;
                //if (NewsSource.Contains("rss_feed"))
                //{
                //    try
                //    {
                //        RSSFeedScript.RSSScript(projectId, daycount);
                //        searchNewsBing.UpdateNewsExtractionStatus(projectId);
                //    }
                //    catch (Exception ex)
                //    {

                //    }
                //    //DataBaseMethod.UpdateGroupingId(projectId);
                //}
            }
        }
        public static void InvestingNewsExtract(int projectId)
        {
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                var NewsSource = dbContext.ProjectNewsSourceMappings.Where(x => x.project_id == projectId).Where(z => z.Active_Status == true).Select(y => y.NewsSource.SourceName).ToList();
                var daycount = dbContext.DecisionParameters.Where(x => x.project_id == projectId).Select(y => y.NewsExtract_Day_Threshold).FirstOrDefault();

                SearchNewsBing searchNewsBing = new SearchNewsBing();
                if (NewsSource.Contains("Investing.com"))
                {
                    InvestingComScript.InvestingScript(projectId, daycount);
                    searchNewsBing.UpdateNewsExtractionStatus(projectId);
                    DataBaseMethod.UpdateGroupingId(projectId);
                }
            }
        }
    }
}