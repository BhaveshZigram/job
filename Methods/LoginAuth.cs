﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Core.Objects;

namespace DragnetAlphaWebAPI.Methods
{
    public class LoginAuth
    {

        public static AuthResponseModel ValidateAuth(string username, string password)
        {
            List<ProjectModel> projects = new List<ProjectModel>();
            List<string> roles = new List<string>();
            string passwordHash = ComputeMd5Hash(password);
            try
            {
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    var userInfo = dbContext.ApplicationsUsers.FirstOrDefault(x => x.UserName == username);
                    int id = userInfo.id;
                    if (userInfo.Password == passwordHash)
                    {
                        var projectList = dbContext.UserProjectRoleMappings.Include("ApplicationsUser").Include("Project").Where(x => x.user_id == id).ToList();
                        var pro = dbContext.UserProjectRoleMappings.Where(x => x.user_id == id).ToList();
                        foreach (var project in pro)
                        {
                            ProjectModel project1 = new ProjectModel { ProjectName = project.Project.ProjectName, ProjectID=project.project_id, ProjectImage = Convert.ToBase64String(project.Project.ProjectImage), ImageType = project.Project.ImageType };


                            projects.Add(project1);
                        }
                        string p = "";

                        var roleList = dbContext.UserProjectRoleMappings.Include("ApplicationsUser").Include("ApplicationRole").Where(x => x.user_id == id).Select(y => y.ApplicationRole.RoleName).Take(1).ToList();
                        if (userInfo.ProjectImage != null)
                        {
                            p = Convert.ToBase64String(userInfo.ProjectImage);
                            return new AuthResponseModel
                            {
                                Status = true,
                                Message = "Success",
                                UserId = id,
                                Role = roleList,
                                ProjectAssigned = projects,
                                UserName = userInfo.UserName,
                                FirstName = userInfo.FirstName,
                                LastName = userInfo.LastName,
                                UserImage = p,
                                ImageType = userInfo.ImageType
                            };
                        }
                        else
                        {
                            return new AuthResponseModel
                            {
                                Status = true,
                                Message = "Success",
                                UserId = id,
                                Role = roleList,
                                ProjectAssigned = projects,
                                UserName = userInfo.UserName,
                                FirstName = userInfo.FirstName,
                                LastName = userInfo.LastName,
                            };
                        }
                    }
                    else
                    {
                        return new AuthResponseModel
                        {
                            Status = false,
                            Message = "Wrong Username Or Password",
                            UserId = -1,
                        };
                    }
                }
            }
            catch
            {
                return new AuthResponseModel
                {
                    Status = false,
                    Message = "Error Occur",
                    UserId = 0,
                };
            }
        }

        public static string ComputeMd5Hash(string password)
        {
            MD5 md = MD5.Create();
            byte[] data = md.ComputeHash(Encoding.UTF8.GetBytes(password));
            StringBuilder res = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                res.Append(data[i].ToString("x2"));
            }

            return res.ToString();
        }
    }
}