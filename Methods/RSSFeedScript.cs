﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;

namespace DragnetAlphaWebAPI.Methods
{
    public class RSSFeedScript
    {
        public static string RSSScript(int project_id, int? daycount)
        {
            string error = "";
            string output = "";
            try
            {
                //string result = "E://AccenturePilot//RSSMaster//";
                string result = "D://home//python364x64//";
                //string filename = result + "googlenewsrssprime.py";
                string filename = result + "rss_master.py";
                BlobMethods.ReadBlobFile("rss_master.py", "dragnet-alpha/RSSFeed", filename);
                Process p = new Process();
                p.StartInfo = new ProcessStartInfo(@"D://home//python364x64//python.exe")
                {
                //    p.StartInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64\python.exe")
                //{
                    Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"", filename, project_id, daycount),
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardError = true
                };
                p.Start();
                 error = p.StandardError.ReadToEnd();
                 output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                return error + output;
            }
            catch
            {
                return error + output;
            }
        }

        public static string  DataBreachcript(int project_id, int? daycount)
        {
            string error = "";
            string output = "";
            try
            {
                //string result = "E://AccenturePilot//RSSMaster//";
                string result = "D://home//python364x64//";
                //string filename = result + "googlenewsrssprime.py";
                string filename = result + "databreaches_script.py";
                BlobMethods.ReadBlobFile("databreaches_script.py", "dragnet-alpha/DataBreached", filename);
                Process p = new Process();
                p.StartInfo = new ProcessStartInfo(@"D://home//python364x64//python.exe")
                {
                //    p.StartInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64\python.exe")
                //{
                    Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"", filename, project_id, daycount),
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardError = true
                };
                p.Start();
                 error = p.StandardError.ReadToEnd();
                 output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                return error + output;
            }
            catch (Exception ex)
            {
                return error + output;
                Console.WriteLine(ex);
            }
        }
    }
}