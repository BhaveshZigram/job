﻿
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using Microsoft.Azure.CognitiveServices.Search.NewsSearch;
using System.Xml.Serialization;
using System.Text;

namespace DragnetAlphaWebAPI.Methods
{
    public class SearchNewsBing
    {
        public static List<NewsLink> BingNewsSearch1(string SearchString)
        {
            string accessKey = "52bb3bf48add47f9ba875f36a5dcbb14";
            string uriBase = "https://api.cognitive.microsoft.com/bing/v7.0/news/search";
            try
            {
                string uriQuery = uriBase + "?q=" + Uri.EscapeDataString(SearchString) + "&count=30";
                WebRequest request = HttpWebRequest.Create(uriQuery);
                request.Headers["Ocp-Apim-Subscription-Key"] = accessKey;
                HttpWebResponse response = (HttpWebResponse)request.GetResponseAsync().Result;
                string json = new StreamReader(response.GetResponseStream()).ReadToEnd();
                dynamic obj = JsonConvert.DeserializeObject(json);
                List<NewsLink> linkNews = new List<NewsLink>();
                NewsLink link = new NewsLink();
                foreach (dynamic data in obj.value)
                {
                    link = new NewsLink
                    {
                        Link = data.url,
                        DatePublished = data.datePublished.Value.Date.ToString("dd mmm yyyy"),
                        Heading = data.name,
                        Description = data.description,
                        LinkCategory = data.category,
                        CreatedAt = DateTime.Now,
                        ReviewStatus_Id = 0,
                        Comment = "",
                        LinkFlag_Id = 0

                    };
                    linkNews.Add(link);
                }
                return linkNews;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public static List<NewsLink> BingNewsSearch(int linkCount, SearchStringModel searchStrings)
        {
            string searchTerm = searchStrings.SearchString;
            string key = "52bb3bf48add47f9ba875f36a5dcbb14";
            NewsSearchClient client = new NewsSearchClient(new ApiKeyServiceClientCredentials(key));

            List<NewsLink> linkNews = new List<NewsLink>();
            //using (DragnetAlpha_DbV2Entities dbContext = new DragnetAlpha_DbV2Entities())
            //{

            //    var searchStringId = dbContext.AllSearchStrings.Where(x => x.SearchString == searchTerm).Select(y => y.SearchStringId).ToList();


            // int id = searchStringId[0];
            // int id = 1;
            try
            {
                Microsoft.Azure.CognitiveServices.Search.NewsSearch.Models.News newsResults = client.News.SearchAsync(query: searchTerm, market: "en-us", count: linkCount, sortBy: "Date", freshness: "Day").Result;

                if (newsResults.Value.Count > 0)
                {
                    NewsLink link = new NewsLink();
                    foreach (Microsoft.Azure.CognitiveServices.Search.NewsSearch.Models.NewsArticle news in newsResults.Value)
                    {
                        if (news.Category != null)
                        {
                            try
                            {
                                string imageUrl1 = "null";
                                try
                                {
                                    if (news.Image != null)
                                    {
                                        imageUrl1 = news.Image.Thumbnail.ContentUrl;
                                        string[] imageUrl2 = imageUrl1.Split('&');
                                        imageUrl1 = imageUrl2[0];
                                    }
                                    else
                                    {
                                        imageUrl1 = "null";
                                    }
                                }
                                catch
                                {
                                    imageUrl1 = "null";
                                }

                                DateTime d1 = DateTime.Now;
                                try
                                {

                                    d1 = DateTime.ParseExact(news.DatePublished, "MM/dd/yyyy HH:mm:ss", null);
                                    d1 = DateTime.Parse(d1.ToString());
                                }
                                catch (Exception ex)
                                {
                                    //d1 = DateTime.ParseExact(news.DatePublished, "MM/dd/yyyy HH:mm:ss", null);
                                    d1 = DateTime.Parse(d1.ToString());
                                }
                                link = new NewsLink
                                {
                                    Heading = news.Name,
                                    Link = news.Url,
                                    Description = news.Description,
                                    DatePublished = d1,
                                    NewsProvider = news.Provider[0].Name,
                                    ImageUrl = imageUrl1,
                                    LinkCategory = news.Category,
                                    SearchTerm_id = searchStrings.SearchTerm_id,
                                    project_id = searchStrings.project_id,
                                    ReviewStatus_Id = 0,
                                    Comment = "",
                                    LinkFlag_Id = 0,
                                    CreatedAt = DateTime.Now,
                                    ContentExtraction = "Yes",
                                    DuplicateFlag = "No",
                                    Sentiment_Script_Status = 0,
                                    DeDupe_Script_Status = 0,
                                    Summary_Script_Status = 0,
                                    Content_Extraction_Script_Status = 0,
                                    Relevance_Score_Status = 0,
                                    Tags_Score_Status = 0,
                                    TargetedEntity = searchStrings.TargetEntity,
                                    EventsTagged = searchStrings.EventTagged,
                                    Theme = searchStrings.Theme,
                                    ThemeVsEventJson = searchStrings.ThemeVsEventModelJson,
                                    CountryCode = searchStrings.CountryCode
                                };
                            }
                            catch (Exception ex)
                            {
                                DateTime d1 = DateTime.Now;
                                try
                                {

                                    d1 = DateTime.ParseExact(news.DatePublished, "MM/dd/yyyy hh:mm:sss", null);
                                }
                                catch (Exception ex1)
                                {
                                    d1 = DateTime.Now;
                                }
                                link = new NewsLink
                                {
                                    Heading = news.Name,
                                    Link = news.Url,
                                    Description = news.Description,
                                    DatePublished = d1,
                                    NewsProvider = news.Provider[0].Name,
                                    ImageUrl = "null",
                                    LinkCategory = news.Category,
                                    SearchTerm_id = searchStrings.SearchTerm_id,
                                    project_id = searchStrings.project_id,
                                    ReviewStatus_Id = 0,
                                    Comment = "",
                                    LinkFlag_Id = 0,
                                    ContentExtraction = "Yes",
                                    DuplicateFlag = "No",
                                    Sentiment_Script_Status = 0,
                                    DeDupe_Script_Status = 0,
                                    Summary_Script_Status = 0,
                                    Content_Extraction_Script_Status = 0,
                                    Relevance_Score_Status = 0,
                                    Tags_Score_Status = 0,
                                    CreatedAt = DateTime.Now,
                                    TargetedEntity = searchStrings.TargetEntity,
                                    EventsTagged = searchStrings.EventTagged,
                                    Theme = searchStrings.Theme,
                                    ThemeVsEventJson = searchStrings.ThemeVsEventModelJson,
                                    CountryCode = searchStrings.CountryCode

                                };
                            }
                            string hashcode = "null";
                            using (MD5 md5Hash = MD5.Create())
                            {
                                hashcode = MD5Hash.GetMd5Hash(md5Hash, link.Link);
                            }
                            link.LinkHashCode = hashcode;
                            linkNews.Add(link);
                            //InsertToDB.AddToCRBNewsLinks(link, hashcode, SubCategory, SearchString);
                        }
                    }
                    return linkNews;
                }

            }



            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return linkNews;
            }

            return linkNews;

        }

        public static List<NewsLink> GoogleNewsSearch()
        {
            List<NewsLink> linkNews = new List<NewsLink>();
            WebClient wc = new WebClient();
            var aMLNews = wc.DownloadString("https://newsapi.org/v2/everything?domains=mjbizdaily.com&sortBy=PublishedAt&apiKey=7397fc01e1c04b4983532dc6691fe627");
            dynamic dyn = JsonConvert.DeserializeObject(aMLNews);
            NewsLink link = new NewsLink();
            foreach (var json1 in dyn.articles)
            {
                var ww = json1.title;
                link = new NewsLink
                {
                    Heading = json1.title,
                    Link = json1.url,
                    Description = json1.description,
                    DatePublished = json1.publishedAt,
                    NewsProvider = json1.source.name,
                    ImageUrl = json1.urlToImage,
                    LinkCategory = json1.source.name,
                    ReviewStatus_Id = 0,
                    Comment = "",
                    LinkFlag_Id = 0,
                    CreatedAt = DateTime.Now

                    //SubCategory = json1.source.name,
                    //SearchSring = json1.source.name
                    ////SubCategorySymbol = SubCategorySymbol
                };
                string hashcode = "null";
                using (MD5 md5Hash = MD5.Create())
                {
                    hashcode = MD5Hash.GetMd5Hash(md5Hash, link.Link);
                }
                link.LinkHashCode = hashcode;

                linkNews.Add(link);
            }
            return linkNews;
            //InsertToDB.AddToCRBNewsLinks(link, hashcode, SubCategory, SearchString);
        }

        public static List<NewsLink> GoogleNewsLinks(int linkcount, SearchStringModel searchString)
        {
            List<NewsLink> linkNews = new List<NewsLink>();
            //string searchStringUpdated = searchString.Replace(" ", string.Empty);
            string baseLink = "https://newsapi.org/v2/everything?q=";
            DateTime daysBefore = DateTime.Today.AddDays(-7);
            string daysBeforeDate = daysBefore.ToString("yyyy-MM-dd");
            //string stringQuery = "q=" + searchString;
            string fullUrl = baseLink + Uri.EscapeDataString(searchString.SearchString) + $"&sortBy=PublishedAt&From={daysBeforeDate}&country=us&apiKey=48c03d32f8224cfbb0d0c90d137d6dba";
            //string searchStringUpdated = new Uri(stringQuery);


            DateTime today = DateTime.Today;
            string todayDate = today.ToString("yyyy-MM-dd");

            //string newsLinkQueryReady = baseLink + stringQuery + "&sortBy=PublishedAt&apiKey=7397fc01e1c04b4983532dc6691fe627";
            WebClient wc = new WebClient();
            //using (DragnetAlpha_DbV2Entities dbContext = new DragnetAlpha_DbV2Entities())
            //{

            //    var searchStringId = dbContext.AllSearchStrings.Where(x => x.SearchString == searchString).Select(y => y.SearchStringId).ToList();
            //    //int id = searchStringId[0];
            //    int id = 1;
            try
            {
                NewsLink bingResult = new NewsLink();
                NewsLink bingResult1 = new NewsLink();


                var GoogleNews = wc.DownloadString(fullUrl);
                dynamic dyn = JsonConvert.DeserializeObject(GoogleNews);

                foreach (var json1 in dyn.articles)
                {
                    var ww = json1.title;
                    bingResult = new NewsLink
                    {
                        Heading = json1.title,
                        Link = json1.url,
                        Description = json1.description,
                        DatePublished = json1.publishedAt,
                        NewsProvider = json1.source.name,
                        ImageUrl = json1.urlToImage,
                        ReviewStatus_Id = 0,
                        Comment = "",
                        LinkFlag_Id = 0,
                        //LinkCategory = json1.source.category,
                        //SubCategory = json1.source.name,
                        SearchTerm_id = searchString.SearchTerm_id,
                        project_id = searchString.project_id,
                        CreatedAt = DateTime.Now,
                        CountryCode = searchString.CountryCode



                        //SubCategorySymbol = SubCategorySymbol
                    };

                    string hashcode = "null";
                    using (MD5 md5Hash = MD5.Create())
                    {
                        hashcode = MD5Hash.GetMd5Hash(md5Hash, bingResult.Link);
                    }
                    bingResult.LinkHashCode = hashcode;
                    //optimize here
                    //if(bingResult.DatePublished==todayDate || bingResult.DatePublished==daysBeforeDate)
                    linkNews.Add(bingResult);
                }
                if (searchString.project_id == 2)
                {
                    var mjbDailyNews = wc.DownloadString("https://newsapi.org/v2/everything?domains=mjbizdaily.com&sortBy=PublishedAt&apiKey=7397fc01e1c04b4983532dc6691fe627");
                    dynamic dyn1 = JsonConvert.DeserializeObject(mjbDailyNews);
                    foreach (var json2 in dyn1.articles)
                    {
                        var ww = json2.title;
                        bingResult1 = new NewsLink
                        {
                            Heading = json2.title,
                            Link = json2.url,
                            Description = json2.description,
                            DatePublished = json2.publishedAt,
                            NewsProvider = json2.source.name,
                            ImageUrl = json2.urlToImage,
                            ReviewStatus_Id = 0,
                            Comment = "",
                            LinkFlag_Id = 0,
                            //Category = json2.source.name,
                            //SubCategory = json2.source.name,
                            SearchTerm_id = searchString.SearchTerm_id,
                            project_id = searchString.project_id,
                            CreatedAt = DateTime.Now,
                            CountryCode = searchString.CountryCode

                            //SubCategorySymbol = SubCategorySymbol
                        };
                        string hashcode = "null";
                        using (MD5 md5Hash = MD5.Create())
                        {
                            hashcode = MD5Hash.GetMd5Hash(md5Hash, bingResult1.Link);
                        }
                        bingResult1.LinkHashCode = hashcode;

                        //if (bingResult1.DatePublished == todayDate || bingResult1.DatePublished == daysBeforeDate)
                        linkNews.Add(bingResult1);
                    }
                }
                return linkNews;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public static List<NewsLink> SocialMediaNews(string searchString, string subCategory, int projectId, int id)
        {
            List<NewsLink> linkNews = new List<NewsLink>();
            //string searchStringUpdated = searchString.Replace(" ", string.Empty);
            string baseLink = "https://api.social-searcher.com/v2/search?q=\"";
            List<string> SocialMediaKey = new List<string>();
            SocialMediaKey.Add("8e3c57b7b030ac39a24c11a992a627d8");
            SocialMediaKey.Add("aeef63b93314b716c143d62daf40f826");
            SocialMediaKey.Add("92923107d9f849e59f5f10a8e04496d4");
            SocialMediaKey.Add("8e3c57b7b030ac39a24c11a992a627d8");
            SocialMediaKey.Add("2328aef9f3d70986361167674cf462f3");
            SocialMediaKey.Add("ad00f5aa4eb3cea287ffc6b2d2b42a5e");
            SocialMediaKey.Add("8f043adf32d9ee497affed8ee4a5fb38");
            SocialMediaKey.Add("1ddbc42fa7392cea88a1f64f8a488b54");
            SocialMediaKey.Add("1ecfbd03223e7e3f06cf304851a41366");
            SocialMediaKey.Add("2618eaa1b0e2c5abcc8ecb19c8dc1cce");
            SocialMediaKey.Add("006f52e9102a8d3be2fe5614f42ba989");
            foreach (var key in SocialMediaKey)
            {
                string endPoint = $"\"&limit=100&lang=en&key={key}";

                string fullUrl = baseLink + Uri.EscapeDataString(searchString) + endPoint;
                DateTime today = DateTime.Today;
                WebClient wc = new WebClient();
                try
                {
                    NewsLink bingResult = new NewsLink();
                    NewsLink bingResult1 = new NewsLink();
                    var GoogleNews = wc.DownloadString(fullUrl);
                    dynamic dyn = JsonConvert.DeserializeObject(GoogleNews);
                    foreach (var json1 in dyn.posts)
                    {
                        DateTime d1 = DateTime.Now;
                        try
                        {
                            string date1 = json1.posted;
                            string[] date = date1.Split(' ');
                            string d1c = date[0] + " " + date[1];
                            d1 = DateTime.Parse(d1c.ToString());
                        }
                        catch (Exception ex)
                        {
                            d1 = DateTime.Parse(d1.ToString());
                        }
                        var ww = json1.title;
                        bingResult = new NewsLink
                        {
                            Link = json1.url,
                            Description = json1.text,
                            DatePublished = d1,
                            NewsProvider = json1.network,
                            ImageUrl = "null",
                            ReviewStatus_Id = 0,
                            Comment = "",
                            LinkFlag_Id = 0,
                            Sentiment = json1.sentiment,
                            type = json1.type,
                            Language = json1.lang,
                            SearchTerm_id = id,
                            project_id = projectId,
                            CreatedAt = DateTime.Now,
                            //CountryCode = searchString.CountryCode

                        };

                        string hashcode = "null";
                        using (MD5 md5Hash = MD5.Create())
                        {
                            hashcode = MD5Hash.GetMd5Hash(md5Hash, bingResult.Link);
                        }
                        bingResult.LinkHashCode = hashcode;
                        linkNews.Add(bingResult);
                    }

                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    continue;
                }
            }
            return linkNews;

        }

        public async void InsertToDBAsync(List<NewsLink> link, int Projectid, string newsSource)
        {
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                string xml = ToXML(link);
                if (xml != null)
                {
                    try
                    {
                        // dbContext.sp_InsertNewsToDB(xml);
                        //  dbContext.Database.CommandTimeout = 0;
                        // await dbContext.SaveChangesAsync();
                        dbContext.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        dbContext.NewsLinks.AddRange(link);
                        dbContext.SaveChanges();
                    }
                    NewsExtractionStatu newsExtractionStatus = new NewsExtractionStatu();
                    newsExtractionStatus.CreatedAt = DateTime.Now;
                    newsExtractionStatus.NewsSource_id = dbContext.NewsSources.Where(x => x.SourceName == newsSource).Select(y => y.Id).FirstOrDefault();
                    newsExtractionStatus.Project_id = Projectid;
                    newsExtractionStatus.status = true;
                    dbContext.NewsExtractionStatus.Add(newsExtractionStatus);
                    //var UpdateNewsSourceStatus = dbContext.ProjectNewsSourceMappings.FirstOrDefault(x => x.project_id == Projectid && x.NewsSource.SourceName==newsSource);
                    //UpdateNewsSourceStatus.ProcessStatus = 1;
                    dbContext.SaveChanges();
                }
                else
                {
                    dbContext.NewsLinks.AddRange(link);
                    dbContext.SaveChanges();
                }
            }
            //return allLink;
        }

        public async void UpdateNewsExtractionStatus(int Projectid)
        {
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {

                NewsExtractionStatu newsExtractionStatus = new NewsExtractionStatu();
                newsExtractionStatus.CreatedAt = DateTime.Now;
                //newsExtractionStatus.NewsSource_id = dbContext.NewsSources.Where(x => x.SourceName == newsSource).Select(y => y.Id).FirstOrDefault();
                newsExtractionStatus.Project_id = Projectid;
                newsExtractionStatus.status = true;
                dbContext.NewsExtractionStatus.Add(newsExtractionStatus);
                //var UpdateNewsSourceStatus = dbContext.ProjectNewsSourceMappings.FirstOrDefault(x => x.project_id == Projectid && x.NewsSource.SourceName==newsSource);
                //UpdateNewsSourceStatus.ProcessStatus = 1;
                dbContext.SaveChanges();

            }
            //return allLink;
        }
        public string ToXML(List<NewsLink> obj)
        {
            try
            {
                using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<NewsLink>));
                    xmlSerializer.Serialize(stringWriter, obj);
                    return stringWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
