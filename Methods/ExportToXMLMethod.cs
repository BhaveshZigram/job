﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Serialization;

namespace DragnetAlphaWebAPI.Methods
{
    public class ExportToXMLMethod
    {
        public static string ToXML(LinkEntities Links)
        {
            using (var stringwriter = new Utf8StringWriter())
            {
                var serializer = new XmlSerializer(Links.GetType());
                serializer.Serialize(stringwriter, Links);
                return stringwriter.ToString();
            }
        }
    }
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding => Encoding.UTF8;
    }
}