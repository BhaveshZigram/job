﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DragnetAlphaWebAPI.Methods
{
   public class BlobMethods
    {
        private static readonly string blobConnectionString = ConfigurationManager.ConnectionStrings["BlobConnectionString"].ConnectionString;

        public static void UploadToBlob(string path, string fileName, string containerName)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(blobConnectionString);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);
                CloudBlockBlob blob = container.GetBlockBlobReference(fileName);
                blob.UploadFromFile(Path.Combine(path));
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static void ReadBlobFile(string blobName, string containerName, string Path)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(blobConnectionString);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);
                CloudBlockBlob blob = container.GetBlockBlobReference(blobName);
                blob.DownloadToFile(Path, FileMode.Create);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
