﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace DragnetAlphaWebAPI.Methods
{
    public class ContentExtractionMethod
    {
        public static void ExtractContent(int projectId)
        {

            try
            {
                using (DragnetAlpha_DbV1Entities dbContext = new DragnetAlpha_DbV1Entities())
                {
                    DateTime TodayDate = DateTime.Today;
                    string DateToday = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
                    
                    int i = 0;
                    while(i==0)
                    {
                        var dedupestatus = dbContext.Links.Where(x => x.ProjectId == projectId).Where(x => x.CreatedAt > TodayDate).Select(x => x.DeDupe_Script_Status).Distinct();
                        if(dedupestatus.Contains(0))
                        {
                            i = 0;
                        }
                        else
                        {
                            i = 1;
                        }
                    }
                    List<Link> LinksToBeDump = dbContext.Links.Where(x => x.ProjectId == projectId).Where(x => x.Content_Extraction_Script_Status == 0).Where(x => x.DuplicateFlag == "No").Where(x => x.DeDupe_Script_Status == 1).Where(x => x.CreatedAt > TodayDate).ToList();
                   
                    
                    //string result = Path.GetTempPath();

                    string result = @"D://home//PythonScripts//";
                    string filename = result + "content_extraction_sep_11_2020.py";
                    string filename1 = result + "utils_contentextraction.py";

                    BlobMethods.ReadBlobFile("content_extraction_sep_11_2020.py", "dragnet-alpha/DataScience", filename);
                    BlobMethods.ReadBlobFile("utils_contentextraction.py", "dragnet-alpha/DataScience", filename1);

                    string error = "";
                    if (LinksToBeDump != null && LinksToBeDump.Count() > 0)
                    {
                        Console.WriteLine($"ContentExtraction Started at {DateTime.Now}");
                        Parallel.ForEach(LinksToBeDump, new ParallelOptions { MaxDegreeOfParallelism =  4}, LinkInput =>
                        {
                            try
                            {
                                Process p = new Process();
                                p.StartInfo = new ProcessStartInfo(@"D://home//python364x64//python.exe")
                                 // p.StartInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64\python.exe")
                                {
                                    Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\"", filename, LinkInput.LinkId, LinkInput.ProjectId),
                                    RedirectStandardOutput = true,
                                    UseShellExecute = false,
                                    CreateNoWindow = true,
                                    RedirectStandardError = true
                                };
                                p.Start();
                                error = p.StandardError.ReadToEnd();
                                string output = p.StandardOutput.ReadToEnd();
                                p.WaitForExit();

                                var link =  dbContext.Links.FirstOrDefault(x => x.LinkId == LinkInput.LinkId);
                                link.Content_Extraction_Script_Status = 1;
                                dbContext.SaveChanges();

                                //DataBaseMethod.UpdateDb(LinkInput.LinkId);
                                //Console.WriteLine(LinkInput.LinkId);
                            }
                            catch(Exception ex)
                            {

                            }
                        });

                        Console.WriteLine($"ContentExtracted Finished at {DateTime.Now}");
                    }
                    else
                    {
                        Console.WriteLine("No Data to Process");
                    }
                    //Thread.Sleep(120000);
                    //Console.WriteLine("Thread on Sleep");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}