﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Methods
{
    public class InvestingComScript
    {
        public static void InvestingScript(int project_id, int? daycount)
        {
            try
            {
                //string result = "E://AccenturePilot//AdditionalPilot//";
                string result = "D://home//python364x64//";
                string filename = result + "investing.py";
                string filename2 = result + "chromedriver.exe";


                BlobMethods.ReadBlobFile("investing.py", "dragnet-alpha/InvestingCom", filename);
                BlobMethods.ReadBlobFile("chromedriver.exe", "dragnet-alpha/InvestingCom", filename2);

                Process p = new Process();
                p.StartInfo = new ProcessStartInfo(@"D://home//python364x64//python.exe")
                {
                    //p.StartInfo = new ProcessStartInfo(@"C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64\python.exe")
                    //{
                    Arguments = string.Format("\"{0}\" \"{1}\"", filename, project_id),
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardError = true
                };
                p.Start();
                string error = p.StandardError.ReadToEnd();
                string output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}