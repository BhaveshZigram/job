﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DragnetAlphaWebAPI.Utilities
{
    public class BasicAuth
    {
        public static bool Login(string username, string password)
        {
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                return dbContext.ApplicationsUsers.Any(user => user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
                && user.Password.Equals(password));
            }
           
        }
    }
}