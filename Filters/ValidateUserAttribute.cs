﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace DragnetAlphaWebAPI.Filters
{
    public class ValidateUserAttribute : ActionFilterAttribute
    {
        public ValidateUserAttribute()
        {

        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            JSONResponse jSONResponse = new JSONResponse();
            string token = actionContext.Request.Headers.Authorization.Parameter;

            //using (DragnetAlpha_DbV2Entities dbContext = new DragnetAlpha_DbV2Entities())
            //{
            //    int isTokeninvalid = dbContext.BlacklistTokens.CountAsync(x => x.Token == token).Result;
            //    if (isTokeninvalid == 1)
            //    {
            //        jSONResponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.BadRequest, Message = "Invalid token." };
            //        string userJson = Newtonsoft.Json.JsonConvert.SerializeObject(jSONResponse);
            //        HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.Forbidden)
            //        {
            //            Content = new StringContent(userJson, System.Text.Encoding.UTF8, "text/json"),
            //        };
            //        throw new HttpResponseException(message);
            //    }
            //};

            //int roleIfFromparams = 0;
            //try { roleIfFromparams = Convert.ToInt32(actionContext.ActionArguments["RoleId"]); }
            //catch { return; }
            //if (roleIfFromparams == null)
            //{
            //    return;
            //}
            //int roleIdFromtoken = ResolveToken(token);
            //if (roleIdFromtoken != roleIfFromparams)
            //{
            //    jSONResponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.Forbidden, Message = "Invalid Access" };
            //    string userJson = Newtonsoft.Json.JsonConvert.SerializeObject(jSONResponse);
            //    HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.Forbidden)
            //    {
            //        Content = new StringContent(userJson, System.Text.Encoding.UTF8, "text/json"),
            //    };
            //    throw new HttpResponseException(message);
            //}
        }

        private async Task<HttpResponseMessage> CheckToken(HttpResponseMessage httpResponseMessage, string token)
        {

            return  httpResponseMessage;
        }

        public bool AllowMultiple => true;
        //public int ResolveToken(string Token)
        //{
        //    try
        //    {
        //        //First Token decryption
        //        AuthenticationTicket ticket = Startup.OAuthOptions.AccessTokenFormat.Unprotect(Token);
        //        if (ticket != null && (ticket.Properties != null && ticket.Properties.ExpiresUtc.HasValue))
        //        {
        //            List<UserRoleMapping> projectRoleMapping = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserRoleMapping>>(ticket.Identity.Claims.ToList()[4].Value);

        //            //List<Claim> claims = ClaimsPrincipal.Current.Claims.ToList();
        //            //List<ProjectRoleMapping> projectRoleMappingClaims = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectRoleMapping>>(claims[4].Value);
        //            //ProjectRoleMapping role = projectRoleMappingClaims.FirstOrDefault(x => x.ProjectId == 1);
        //            if (projectRoleMapping == null || projectRoleMapping.Count == 0)
        //            {
        //            }
        //            return projectRoleMapping[0].RoleId;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return 0;
        //    }
        //    return 0;
        //}
    }
}