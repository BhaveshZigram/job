﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DragnetAlphaWebAPI.Models;
using DragnetAlphaWebAPI.Utilities;
using DragnetAlphaWebAPI.Methods;
using System.Web.Script.Serialization;

namespace DragnetAlphaWebAPI.Providers
{

    public class OAuthProvider : OAuthAuthorizationServerProvider
    {


        public AuthResponseModel user = new AuthResponseModel();

        #region[GrantResourceOwnerCredentials]
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return Task.Factory.StartNew(() =>
            {
                string userName = context.UserName;
                string password = context.Password;
                user = LoginAuth.ValidateAuth(userName, password);
                if (user != null)
                {
                    //string userJson = Newtonsoft.Json.JsonConvert.SerializeObject(user.ProjectRoleMappings);
                    //if (user.MainRole == null)
                    //{
                    //    context.SetError("invalid_grant", "The user name or password is incorrect");
                    //}
                    //else
                    //{
                        List<Claim> claims = new List<Claim>()
                    {
                            new Claim(ClaimTypes.Sid, Convert.ToString(user.UserId)),
                            new Claim(ClaimTypes.Name, user.FirstName+"_"+user.LastName),
                            new Claim(ClaimTypes.Email, user.UserName)
                           // new Claim(ClaimTypes.Role, user.MainRole.RoleName ?? "-"),
                            //new Claim("Permissions",userJson)
                    };
                        ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claims,
                                    Startup.OAuthOptions.AuthenticationType);

                        context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
                        context.Request.Context.Authentication.SignIn(oAuthIdentity);

                        AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties());
                        context.Validated(ticket);
                   // }
                }
                else
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect");
                }
            });
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            ClaimsIdentity newIdentity = new ClaimsIdentity(context.Ticket.Identity);

            AuthenticationTicket newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

        #endregion

        #region[ValidateClientAuthentication]
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }
        #endregion

        #region[TokenEndpoint]
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            AuthenticationProperties properties = CreateProperties(user);
            foreach (KeyValuePair<string, string> property in properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, new JavaScriptSerializer().Serialize(user));
            }

            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }
        #endregion

        #region[CreateProperties]
        public static AuthenticationProperties CreateProperties(AuthResponseModel user)
        {
            //UserDetailsModel userLoginDetailsModel = new UserDetailsModel()
            //{
            //    UserId = user.UserId,
            //    ProjectaAssigned = user.ProjectAssigned
            //};

            string userJson = new JavaScriptSerializer().Serialize(user);

            IDictionary<string, string> data = new Dictionary<string, string>
            {
                {"userInfo", userJson }
            };
            return new AuthenticationProperties(data);
        }
        #endregion
    }

}