﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]


    public class TagsDropDownListController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public List<string> GET()
        {
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {

                    var TagList = dbContext.t_tag_link_clusters.OrderBy(p => p.Tags).ToList();
                    List<string> t_tag_link_cluster = new List<string>();
                    foreach (var tag in TagList)
                    {
                        t_tag_link_cluster.Add(tag.Tags);
                    }
                    return t_tag_link_cluster;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
    }
}
