﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Models;
using DragnetAlphaWebAPI.Methods;
using Newtonsoft.Json;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class DeleteWhiteLinkController : ApiController
    {

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };

        }

        public HttpResponseMessage POST(HttpRequestMessage requestMessage)
        {
            JSONResponse jsonresponse = new JSONResponse();
            try
            {
                WhiteList whiteListLinks = new WhiteList();
                string paramsSend = Request.Content.ReadAsStringAsync().Result;
                whiteListLinks = JsonConvert.DeserializeObject<WhiteListLink>(paramsSend.ToString());
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    var toDelete = dbContext.WhiteListLinks.SingleOrDefault(x => x.Id == whiteListLinks.Id);
                    dbContext.WhiteListLinks.Remove(toDelete);
                    dbContext.SaveChanges();
                }
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "White Link Deleted" };
                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }
}

