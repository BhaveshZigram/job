﻿using DragnetAlphaWebAPI.Filters;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class CommentController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public HttpResponseMessage POST(HttpRequestMessage request)
        {
            JSONResponse jsonresponse = new JSONResponse();

            string params_send = request.Content.ReadAsStringAsync().Result;
            ReviewStatus input = JsonConvert.DeserializeObject<ReviewStatus>(params_send);
            string comment = input.Comment;
            int linkId = input.LinkId;
            DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1();
            
                try
                {
                    var link = dbContext.UpdateCommentByLinkId( linkId, comment);
                    //link.Comment = comment;
                    //dbContext.SaveChanges();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Review Updated" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
         
        }
    }
}
