﻿//using DragnetAlphaWebAPI.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//namespace DragnetAlphaWebAPI.Controllers
//{
//    [Authorize]

//    public class GetSearchStringController : ApiController
//    {
//        public HttpResponseMessage Options()
//        {
//            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
//        }

//        public HttpResponseMessage GET(string project)
//        {
//            JSONResponse jsonresponse = new JSONResponse();

//            List<AllSearchString> searchStrings = new List<AllSearchString>();

//            using (DragnetAlpha_DbV2Entities dbContext = new DragnetAlpha_DbV2Entities())
//            {
//                try
//                {
//                    searchStrings = dbContext.AllSearchStrings.Where(x => x.ProjectName == project).ToList();
//                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = searchStrings };
//                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
//                }
//                catch (Exception ex)
//                {
//                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
//                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
//                }
//            }
//        }
//    }
//}
