﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    public class EventListController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET()
        {
            JSONResponse jsonresponse = new JSONResponse();

            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    List<string> eventsList = dbContext.EventLists.Select(x=>x.EventName).ToList();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = eventsList };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                    //  return downloadUrl;
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }

    }
}
