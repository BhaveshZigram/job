﻿using DragnetAlphaWebAPI.Filters;
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    //[Authorize]
    //[ValidateUser]

    public class CompanyProfileOnIdController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET(int id, int project_id)
        {
            JSONResponse jsonresponse = new JSONResponse();
            CompanyProfile profile = new CompanyProfile();
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {

                    profile = dbContext.CompanyProfiles.Where(x => x.Id == id).FirstOrDefault();
                    List<string> Bod = new List<string>();
                    if (profile.BoardofDirectors != null)
                    {
                        Bod = profile.BoardofDirectors.Replace("[", "").Replace("]", "").Split(',').ToList();
                    }
                    List<string> founder = new List<string>();
                    if (profile.Founders != null)
                    {
                        founder = profile.Founders.Replace("[", "").Replace("]", "").Split(',').ToList();
                    }
                    List<string> subsi = new List<string>();
                    if (profile.Subsidiaries != null)
                    {
                        subsi = profile.Subsidiaries.Replace("[", "").Replace("]", "").Split(',').ToList();
                    }
                    List<string> otherNamesforSearch = new List<string>();
                    if (profile.OtherNamesforSearch != null)
                    {
                        otherNamesforSearch = profile.OtherNamesforSearch.Replace("[", "").Replace("]", "").Split(',').ToList();
                    }
                    CompanyProfileModel companyProfile = new CompanyProfileModel();
                    var hits = String.Format("{0:n0}", profile.Hits);
                    companyProfile = new CompanyProfileModel
                    {
                        EntityName = profile.EntityName,
                        BoardofDirectors = Bod,
                        Founders = founder,
                        Subsidiaries = subsi,
                        Sector = profile.Sector,
                        CEO = profile.CEO,
                        CFO = profile.CFO,
                        CompanyType_Global_Local = profile.CompanyType_Global_Local,
                        Id = profile.Id,
                        Industry = profile.Industry,
                        MarketIndex = profile.MarketIndex,
                        FinancialType_PublicorPrivate = profile.FinancialType_PublicorPrivate,
                        Headquarters = profile.Headquarters,
                        OtherNamesforSearch = otherNamesforSearch,
                        Website = profile.Website,
                        TradeName = profile.TradeName,
                        Hits = hits,
                        CompanyLogoUrl = profile.CompanyLogoUrl
                    };
                    int CompanyProfileId = id;
                    int ProjectId = project_id;
                    var AdditionCounts = dbContext.BioViewStoryData(ProjectId, CompanyProfileId).FirstOrDefault();
                    companyProfile.ThemesCount = AdditionCounts.ThemeCount;
                    companyProfile.EventsCount = AdditionCounts.EventsCount;
                    companyProfile.Stories = AdditionCounts.Stories;
                    //int companyId = id;
                    companyProfile.AssociationMapLink = dbContext.EntityWiseAssociationMapURLs.Where(o => o.Project_Id == project_id && o.TargetEntityId == CompanyProfileId).Select(o=>o.AssociationMapUrl).ToList()[0];

                    //companyProfile.AssociationMapLink = "https://darelationshipmap.azurewebsites.net/home/index/da_databreaches/DA61316F4F0AC7?token=e1d1588a3d026a521271ddc37d5b4479&clientId=302BF78C-418D-48C0-90C2-49769D2458D2#";
                    //return companyProfile;

                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = companyProfile };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                    // return Profile;
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    //return null;
                }
            }
        }
    }
}
