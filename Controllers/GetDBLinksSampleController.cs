﻿//using System;
//using System.Collections.Generic;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using System.Linq;
//using System.Data.Entity.Core.Objects;

//namespace DragnetAlphaWebAPI.Controllers
//{
//    [Authorize]

//    //[EnableCors(origins: "https://dragnetalphaprodapi.azurewebsites.net", headers: "*", methods: "*")]
//    public class GetDBLinksSampleController : ApiController
//    {
//        public HttpResponseMessage Options()
//        {
//            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
//        }

//        public dynamic GET(int Id, string Role, string project)
//        {
//            DateTime TodayDate = DateTime.Today;
//            string YesterdayDate = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
//            //var da = DataAsset;
//            //var params_send = request.Content.ReadAsStringAsync().Result;
//            //var input = JsonConvert.DeserializeObject<AllocatedLink>(params_send);
//            int id = Id;
//            string role = Role;
//            string Project = project;
//            List<NewsLink> links = new List<NewsLink>();

//            if (role == "Maker")
//            {
//                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
//                {
//                    try
//                    {

//                        ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(Project);
//                        int projectId = projectInfo.FirstOrDefault().ProjectID;
//                        string MakerName = dbContext.ApplicationsUsers.Where(z => z.UserId == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
//                        //ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
//                        //int projectId = projectInfo.ToList().FirstOrDefault().ProjectID;

//                        var linksFromDB = (from p in dbContext.NewsLinks
//                                           join e in dbContext.ApplicationsUsers
//                                           on p.user_id equals e.UserId into gf
//                                           from subpet in gf.DefaultIfEmpty()
//                                           join s in dbContext.AllSearchStrings
//                                           on p.SearchTerm_id equals s.SearchStringId
//                                           join r in dbContext.ReviewStatus
//                                           on p.ReviewStatus_Id equals r.id
//                                           where p.ProjectId == projectId && p.CreatedAt > TodayDate && p.user_id == id && p.DuplicateFlag == "No"
//                                           select new
//                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.StatusName, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTerm_id, p.AdminId, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, s.SubCategory, p.LinkId, p.Theme, p.TargetedEntity }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

//                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTerm_id, p.AdminId, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.SubCategory, p.LinkId, p.Theme, p.TargetedEntity }).ToList();
//                        return linksDisplay;
//                        // var linksFromDB = dbContext.NewsLinks.Where(z => z.ProjectId == projectId && z.MakerId == id).Distinct().OrderByDescending(y => y.DatePublished).Select(i => new { i.Link, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlag_Id, reviewstatus = ((ReviewStatusEnum)i.ReviewStatus_Id).ToString(), i.MakerId, MakerName, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTerm_id, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory, i.Sentiment, i.SentimentScore, i.SentimentSentence, i.RelevanceScore, i.Tags, i.LinkId }).ToList();
//                        //return linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTerm_id, p.AdminId, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.SubCategory, p.LinkId }).ToList();

//                        //return linksFromDB;
//                    }
//                    catch
//                    {
//                        return null;
//                    }

//                }
//                // string readUrl = string.Concat($"select distinct Link, Heading, Description, HashCode, DatePublished, NewsProvider, Category, Comment, ReviewStatus, SubCategory, ImageUrl, LinkFlag, MakerId from crbNewsLinks where CreatedAt > '{TodaysDate}' and MakerId = {id} and DataAsset = '{dataAsset}' order by DatePublished desc");

//                //result = ReadFromDB.GetTodaysLinks(TodayDate, id, dataAsset, YesterdayDate);
//            }
//            else if (role == "Admin" || role == "SuperAdmin")
//            {
//                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
//                    try
//                    {
//                        ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
//                        int projectId = projectInfo.ToList().FirstOrDefault().ProjectID;

//                        var linksFromDB = (from p in dbContext.NewsLinks
//                                           join e in dbContext.ApplicationsUsers
//                                           on p.user_id equals e.UserId into gf
//                                           from subpet in gf.DefaultIfEmpty()
//                                           join s in dbContext.AllSearchStrings
//                                           on p.SearchTerm_id equals s.SearchStringId
//                                           join r in dbContext.ReviewStatus
//                                           on p.ReviewStatus_Id equals r.id
//                                           where p.ProjectId == projectId && p.CreatedAt > TodayDate && p.DuplicateFlag == "No" && p.RelevanceScore > 0
//                                           select new
//                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.StatusName, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTerm_id, p.AdminId, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, s.SubCategory, p.LinkId, p.GroupingId, p.SourceReputation, p.Theme, p.TargetedEntity }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();


//                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTerm_id, p.AdminId, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.SubCategory, p.LinkId, p.GroupingId, p.SourceReputation, p.Theme, p.TargetedEntity }).ToList();

//                        //List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.ProjectId, SearchTerm_id = p.SearchTerm_id, AdminId = p.AdminId, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.ToString(), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, SubCategory = p.SubCategory, LinkId = p.LinkId, GroupingId = p.GroupingId }).ToList();

//                        //var linksDisplayCopy = LinkModelList;

//                        //var MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

//                        //linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

//                        //foreach(var maintaglink in MainTagLinks)
//                        //{
//                        //    maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
//                        //    if (maintaglink.GroupingId == 380029)
//                        //    {

//                        //    }
//                        //}

//                        return linksDisplay;
//                    }
//                    catch (Exception ex)
//                    {
//                        Console.WriteLine(ex.InnerException);
//                        return null;
//                    }

//                //result = ReadFromDB.GetAdminTodaysLinks(TodayDate, DataAsset, YesterdayDate);
//            }
//            else
//                return null;
//        }
//        //public dynamic GET(int Id, string Role, string project)
//        //{
//        //    DateTime TodayDate = DateTime.Today;
//        //    string YesterdayDate = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
//        //    //var da = DataAsset;
//        //    //var params_send = request.Content.ReadAsStringA]sync().Result;
//        //    //var input = JsonConvert.DeserializeObject<AllocatedLink>(params_send);
//        //    int id = Id;
//        //    string role = Role;
//        //    string Project = project;
//        //    List<NewsLink> links = new List<NewsLink>();
//        //    if (role == "Maker")
//        //    {
//        //        using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
//        //        {
//        //            try
//        //            {

//        //                ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(Project);
//        //                int projectId = projectInfo.FirstOrDefault().ProjectID;
//        //                var linksFromDB = dbContext.NewsLinks.Where(z => z.ProjectId == projectId && z.MakerId == id).Distinct().OrderByDescending(y => y.DatePublished).Select(i => new { i.Link, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlag_Id, i.ReviewStatus_Id, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTerm_id, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
//        //                //links.AddRange(linksFromDB);
//        //                return linksFromDB;
//        //            }
//        //            catch
//        //            {
//        //                return null;
//        //            }

//        //        }
//        //        // string readUrl = string.Concat($"select distinct Link, Heading, Description, HashCode, DatePublished, NewsProvider, Category, Comment, ReviewStatus, SubCategory, ImageUrl, LinkFlag, MakerId from crbNewsLinks where CreatedAt > '{TodaysDate}' and MakerId = {id} and DataAsset = '{dataAsset}' order by DatePublished desc");

//        //        //result = ReadFromDB.GetTodaysLinks(TodayDate, id, dataAsset, YesterdayDate);
//        //    }
//        //    else if (role == "Admin" ||  role == "SuperAdmin")
//        //    {
//        //        using (DragnetAlpha_DbV2Entities1 dbContext=new DragnetAlpha_DbV2Entities1())
//        //            try

//        //            {

//        //                ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(Project);
//        //              //  var abc = projectInfo.ToList();
//        //                int projectId = projectInfo.FirstOrDefault().ProjectID;
//        //                var linksFromDB = dbContext.NewsLinks.Where(x =>x.CreatedAt > TodayDate && x.ProjectId == projectId).Distinct().OrderByDescending(y => y.DatePublished).Select(i => new { i.Link, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlag_Id, i.ReviewStatus_Id, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTerm_id, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
//        //               // var linksFromDB = dbContext.NewsLinks.Where(x => x.ProjectId == projectId).Distinct().OrderByDescending(y => y.DatePublished).ToList();
//        //                //var linksFromDB = dbContext.NewsLinks.ToList();
//        //                // links.AddRange(linksFromDB);
//        //                //string jsonData = JsonConvert.SerializeObject(links);
//        //                //return jsonData;
//        //                return linksFromDB; 
//        //            }
//        //            catch(Exception ex)
//        //            {
//        //                return null;
//        //            }

//        //        //result = ReadFromDB.GetAdminTodaysLinks(TodayDate, DataAsset, YesterdayDate);
//        //    }
//        //    else
//        //    return null;
//        //}
//    }
//}
