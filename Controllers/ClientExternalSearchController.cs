﻿using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    //[Authorize]
    public class ClientExternalSearchController : ApiController
    {
        DragnetAlpha_DbV2Entities1 entities1 = new DragnetAlpha_DbV2Entities1();
        JSONResponse json;
        //If Data send in Query Parameter Use this POST method
        public HttpResponseMessage POST(int projectId,int day,int frequency,string Entity,string language,bool review,bool dsmodel)
        {
            if(projectId!=0 && day!=0 && frequency!=0 && Entity!=null && language!=null)
            {
                var input = new ClientExternalSearchInput()
                {
                    ProjectId = projectId,
                    DaysCount=day,
                    TimeFrequency=frequency,
                    EntityName=Entity,
                    Language=language,
                    Review=review,
                    DSModel=dsmodel,
                    Guid=Guid.NewGuid()
                };
                try
                {
                    entities1.ClientExternalSearchInputs.Add(input);
                    entities1.SaveChanges();
                    json = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Data Added Successfully" };
                    return Request.CreateResponse(json);
                }
                catch(Exception ex)
                {
                    json = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = ex.Message };
                    return Request.CreateResponse(json);
                }
            }
            else
            {
                json = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Send Valid Data" };
                return Request.CreateResponse(json);
            }
        }

        //If data send in Body use this Post method.
        /*public HttpResponseMessage POST()
        {
            var data = Request.Content.ReadAsStringAsync().Result;
            var input= JsonConvert.DeserializeObject<ClientExternalSearchModel>(data.ToString());
            if(input.ProjectId!=0 && input.DaysCount!=0 && input.TimeFrequency!=0 && input.EntityName!=null && input.Language!=null)
            {
                var client = new ClientExternalSearchInput()
                {
                    ProjectId = input.ProjectId,
                    DaysCount = input.DaysCount,
                    TimeFrequency = input.TimeFrequency,
                    EntityName = input.EntityName,
                    Language = input.Language,
                    Review = input.Review,
                    DSModel = input.DsModel,
                    Guid = Guid.NewGuid()
                };
                try
                {
                    entities1.ClientExternalSearchInputs.Add(client);
                    entities1.SaveChanges();
                    json = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Data Added Successfully" };
                    return Request.CreateResponse(json);
                }
                catch (Exception ex)
                {
                    json = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = ex.Message };
                    return Request.CreateResponse(json);
                }
            }
            else
            {
                json = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Send Valid Data" };
                return Request.CreateResponse(json);
            }
        }*/
    }
}
