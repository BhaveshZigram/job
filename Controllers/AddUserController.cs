﻿using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DragnetAlphaWebAPI;
using System.Data.Entity.Core.Objects;
using DragnetAlphaWebAPI.Methods;
using System.Web.Http.Cors;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AddUserController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage Put()
        {
            JSONResponse jsonresponse = new JSONResponse();

            HttpRequest httpRequest = HttpContext.Current.Request;
            string params_send = Request.Content.ReadAsStringAsync().Result;
            SignUp user;
            try
            {
                user = JsonConvert.DeserializeObject<SignUp>(params_send.ToString());
            }
            catch (Exception)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.BadRequest, Message = "Invalid Format" };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                string username = user.UserName;
                string password = user.Password;
                string role = user.Role;
                string project = user.Project;
                string fName = user.FirstName;
                string lName = user.LastName;
                int res = 0;
                try
                {
                    if (username != null && password != null && username != "" && password != "")
                    {
                        int CheckIdAlreadyExist = dbContext.ApplicationsUsers.Where(o => o.UserName == user.UserName).ToList().Count();
                        if (CheckIdAlreadyExist == 0)
                        {


                            ApplicationsUser toAdd = new ApplicationsUser
                            {
                                FirstName = fName,
                                LastName = lName,
                                UserName = username,
                                Password = LoginAuth.ComputeMd5Hash(password),
                                UserGuid = Guid.NewGuid(),
                            };
                            dbContext.ApplicationsUsers.Add(toAdd);
                            dbContext.SaveChanges();
                            var roleInfo = dbContext.ApplicationRoles.Where(x => x.RoleName == role).Select(y => y.id).ToList();
                            int roleId = roleInfo[0];
                            var userInfo = dbContext.ApplicationsUsers.Where(x => x.UserName == username).Select(y => y.id).ToList();
                            int userId = userInfo[0];
                            ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
                            int projectId = projectInfo.FirstOrDefault().id;
                            var CheckIfSameUserExistForSameProject = dbContext.UserProjectRoleMappings.Where(o => o.project_id == projectId && o.user_id == userId).ToList().Count();
                            if (CheckIfSameUserExistForSameProject == 0)
                            {
                                UserProjectRoleMapping userRoleMapping = new UserProjectRoleMapping
                                {
                                    ApplicationRole_id = roleId,
                                    project_id = projectId,
                                    user_id = userId
                                };
                                dbContext.UserProjectRoleMappings.Add(userRoleMapping);
                                dbContext.SaveChanges();
                                res = 1;
                            }
                            else
                            {
                                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.Ambiguous, Message = "User Name Already Exist and can not assign same project two times to same user" };
                                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                            }
                        }
                        else
                        {
                            var roleInfo = dbContext.ApplicationRoles.Where(x => x.RoleName == role).Select(y => y.id).ToList();
                            int roleId = roleInfo[0];
                            var userInfo = dbContext.ApplicationsUsers.Where(x => x.UserName == username).Select(y => y.id).ToList();
                            int userId = userInfo[0];
                            ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
                            int projectId = projectInfo.FirstOrDefault().id;
                            var CheckIfSameUserExistForSameProject = dbContext.UserProjectRoleMappings.Where(o => o.project_id == projectId && o.user_id == userId).ToList().Count();
                            if (CheckIfSameUserExistForSameProject == 0)
                            {
                                UserProjectRoleMapping userRoleMapping = new UserProjectRoleMapping
                                {
                                    ApplicationRole_id = roleId,
                                    project_id = projectId,
                                    user_id = userId
                                };
                                dbContext.UserProjectRoleMappings.Add(userRoleMapping);
                                dbContext.SaveChanges();
                                res = 1;
                            }
                            else
                            {
                                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.Ambiguous, Message = "User Name Already Exist and can not assign same project two times to same user" };
                                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                            }
                        }
                    }
                    else
                    {
                        jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.BadRequest, Message = "Please Enter Valid Details" };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                    if (res == 1)
                    {
                        jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "User Added" };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                    else
                    {
                        jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.BadRequest, Message = "Something Went Wrong" };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Body = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }

            }
        }
    }
}
