﻿using DragnetAlphaWebAPI.Filters;
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class DateFilterForReleventLinksGroupingController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET(string dateFrom, string dateTo, int id, string role, int projectId)
        {
            JSONResponse jsonresponse = new JSONResponse();

            string fromDate = dateFrom;
            string toDate = dateTo;
            //string[] todate = dateTo.Split('-');
            //int date = int.Parse(todate[2]) + 1;
            string NextDate = " ";
            var todayDate = DateTime.Now;
            if(dateTo==null)
            {
                dateTo = todayDate.ToString("yyyy-MM-dd");
            }
            string[] todate = dateTo.Split('-');
            int date = int.Parse(todate[2]) + 1;

            var todayDate1 = todayDate.ToString("yyyy-MM-dd");
            if (toDate == todayDate1)
            {
                var DateTomorrow = todayDate.AddDays(1);
                NextDate = DateTomorrow.ToString("yyyy-MM-dd");
            }
            else
            {
                string toDateUpdated = todate[0] + "-" + todate[1] + "-" + date.ToString();
                NextDate = toDateUpdated;
            }
            DateTime From = DateTime.Parse(fromDate).AddDays(-1);
            DateTime to = DateTime.Parse(NextDate);
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {

                if (role == "Maker")

                {
                    try
                    {
                        //var links = dbContext.NewsLinks.Where(x => x.MakerId == id && x.CreatedAt > from && x.CreatedAt < to && x.ProjectId == projectId && (x.LinkFlag_Id == 1 || x.LinkFlag_Id == 2)).Distinct().Select(i => new { i.Link, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlag_Id, i.ReviewStatus_Id, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTerm_id, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
                        //return links;
                        string MakerName = dbContext.ApplicationsUsers.Where(z => z.id == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                        var linksFromDB = (from p in dbContext.NewsLinks
                                           join e in dbContext.ApplicationsUsers
                                           on p.user_id equals e.id into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join s in dbContext.SearchTerms
                                           on p.SearchTerm_id equals s.id
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatus_Id equals r.id
                                           where p.project_id == projectId && p.DatePublished < to && p.DatePublished > From && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2) && (p.user_id == id) && p.DuplicateFlag == "No" && p.RelevanceScore > 0
                                           select new
                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                        List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId=p.TargetedEntityId }).ToList();

                        var linksDisplayCopy = LinkModelList;

                        var MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                        linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                        foreach (var maintaglink in MainTagLinks)
                        {
                            maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                        }

                        jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = MainTagLinks };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                    catch (Exception ex)
                    {
                        jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                }
                else if (role == "SuperAdmin" || role == "Admin")
                {
                    try
                    {
                        string MakerName = dbContext.ApplicationsUsers.Where(z => z.id == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                        var linksFromDB = (from p in dbContext.NewsLinks
                                           join e in dbContext.ApplicationsUsers
                                            on p.user_id equals e.id into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join s in dbContext.SearchTerms
                                           on p.SearchTerm_id equals s.id
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatus_Id equals r.id
                                           where p.project_id == projectId && p.DatePublished < to && p.DatePublished > From && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2) && p.DuplicateFlag == "No" && p.RelevanceScore > 50
                                           select new
                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(a => a.RelevanceScore).ToList();

                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                        List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId=p.TargetedEntityId }).ToList();

                        var linksDisplayCopy = LinkModelList;

                        var MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                        linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                        foreach (var maintaglink in MainTagLinks)
                        {
                            maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                        }
                        jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = MainTagLinks };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                    catch (Exception ex)
                    {
                        jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                }
                else
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.NotFound, Message = "Invalid User Role" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }

        public HttpResponseMessage PUT(string dateFrom, string dateTo, int id, string role, int projectId, int TargetEntityId)
        {
            JSONResponse jsonresponse = new JSONResponse();

            string fromDate = dateFrom;
            string toDate = dateTo;
            //string[] todate = dateTo.Split('-');
            //int date = int.Parse(todate[2]) + 1;
            string NextDate = " ";
            var todayDate = DateTime.Now;
            if (dateTo == null)
            {
                dateTo = todayDate.ToString("yyyy-MM-dd");
            }
            string[] todate = dateTo.Split('-');
            int date = int.Parse(todate[2]) + 1;

            var todayDate1 = todayDate.ToString("yyyy-MM-dd");
            if (toDate == todayDate1)
            {
                var DateTomorrow = todayDate.AddDays(1);
                NextDate = DateTomorrow.ToString("yyyy-MM-dd");
            }
            else
            {
                string toDateUpdated = todate[0] + "-" + todate[1] + "-" + date.ToString();
                NextDate = toDateUpdated;
            }
            DateTime From = DateTime.Parse(fromDate).AddDays(-1);
            DateTime to = DateTime.Parse(NextDate);
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {

                if (role == "Maker")

                {
                    try
                    {
                        //var links = dbContext.NewsLinks.Where(x => x.MakerId == id && x.CreatedAt > from && x.CreatedAt < to && x.ProjectId == projectId && (x.LinkFlag_Id == 1 || x.LinkFlag_Id == 2)).Distinct().Select(i => new { i.Link, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlag_Id, i.ReviewStatus_Id, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTerm_id, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
                        //return links;
                        string MakerName = dbContext.ApplicationsUsers.Where(z => z.id == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                        var linksFromDB = (from p in dbContext.NewsLinks
                                           join e in dbContext.ApplicationsUsers
                                           on p.user_id equals e.id into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join s in dbContext.SearchTerms
                                           on p.SearchTerm_id equals s.id
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatus_Id equals r.id
                                           where p.project_id == projectId && p.DatePublished < to && p.DatePublished > From && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2) && (p.user_id == id) && p.DuplicateFlag == "No" && p.TargetedEntityId== TargetEntityId && p.RelevanceScore > 0
                                           select new
                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                        List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                        var linksDisplayCopy = LinkModelList;

                        var MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                        linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                        foreach (var maintaglink in MainTagLinks)
                        {
                            maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                        }

                        jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = MainTagLinks };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                    catch (Exception ex)
                    {
                        jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                }
                else if (role == "SuperAdmin" || role == "Admin")
                {
                    try
                    {
                        string MakerName = dbContext.ApplicationsUsers.Where(z => z.id == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                        var linksFromDB = (from p in dbContext.NewsLinks
                                           join e in dbContext.ApplicationsUsers
                                            on p.user_id equals e.id into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join s in dbContext.SearchTerms
                                           on p.SearchTerm_id equals s.id
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatus_Id equals r.id
                                           where p.project_id == projectId && p.DatePublished < to && p.DatePublished > From && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2) && p.DuplicateFlag == "No" && p.TargetedEntityId == TargetEntityId && p.RelevanceScore > 50
                                           select new
                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(a => a.RelevanceScore).ToList();

                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                        List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                        var linksDisplayCopy = LinkModelList;

                        var MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                        linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                        foreach (var maintaglink in MainTagLinks)
                        {
                            maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                        }
                        jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = MainTagLinks };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                    catch (Exception ex)
                    {
                        jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                        return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    }
                }
                else
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.NotFound, Message = "Invalid User Role" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }
    }
}
