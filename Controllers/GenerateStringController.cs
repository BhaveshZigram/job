﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace DragnetAlphaWebAPI.Controllers
{
    public class GenerateStringController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public string GET(int projectId)
        {
            try
            {
                List<SearchTermMapping> searchStringList = new List<SearchTermMapping>();
                List<NewsLink> bingAndGoogleLink = new List<NewsLink>();
                List<NewsLink> allResult = new List<NewsLink>();
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    ThemeVsEventModel themeVsEventModel = new ThemeVsEventModel();
                    var NewsSource = dbContext.ProjectNewsSourceMappings.Where(x => x.project_id == projectId && x.Active_Status == true && x.NewsSource.Active_Status == true).Select(y => y.NewsSource.SourceName).ToList();
                    var NewsSource1 = dbContext.ProjectNewsSourceMappings.Where(x => x.project_id == projectId && x.Active_Status == true && x.NewsSource.Active_Status == true).ToList();

                    dbContext.Configuration.AutoDetectChangesEnabled = false;
                    var themeLists = dbContext.ThemeLists.Where(x => x.Project_id == projectId).ToList();
                    themeVsEventModel.ThemeVsEvent = new List<ThemeModel>();
                    var EventLists = dbContext.EventLists.Where(x => x.Project_id == projectId).ToList();

                    themeVsEventModel.ThemeVsEvent = new List<ThemeModel>();
                    foreach (var themeList in themeLists)
                    {
                        List<EventModel> eventModels = themeList.EventLists.Select(o => new EventModel() { EventId = o.Id, EventName = o.EventName }).ToList();
                        ThemeModel themeModel = new ThemeModel() { ColorCode = themeList.ColorCode, ColorName = themeList.ColouName, ThemeId = themeList.Id, ThemeName = themeList.ThemeName, EventList = eventModels };
                        themeVsEventModel.ThemeVsEvent.Add(themeModel);
                    }
                    var json = new JavaScriptSerializer().Serialize(themeVsEventModel);
                    var entityToHit = dbContext.TargetEntities.Where(x => x.project_id == projectId).ToList();
                    SearchStringFormation searchStringFormation = new SearchStringFormation();
                    var StgOutput = searchStringFormation.GenerateSearchString(projectId, NewsSource, NewsSource1);
                    return "Search String Generated and Count is : " + StgOutput.Count();
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.InnerException);
                return "Search String Generation Failed";
            }
        }
    }
}
