﻿using DragnetAlphaWebAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    public class FAQController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET(string project)
        {
            JSONResponse jsonresponse = new JSONResponse();

            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                if (project != null)
                {
                    var quesAns = dbContext.FAQs.Where(x => x.Project == project).ToList();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = quesAns };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }
                else
                {
                    var quesAns = dbContext.FAQs.ToList();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = quesAns };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }
            }
        }
    }
}
