﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

using System.Linq;
namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    public class LoginController : ApiController
    {
        public HttpResponseMessage Options()
        {

            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public HttpResponseMessage Post()
        {
            string params_send = Request.Content.ReadAsStringAsync().Result;
            ApplicationsUser user;
            try
            {
                user = JsonConvert.DeserializeObject<ApplicationsUser>(params_send.ToString());
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Enter Valid Details");
            }
            string username = user.UserName;
            string password = user.Password;
            if (username != null && password != null && username != "" && password != "")
            {
                AuthResponseModel auth = LoginAuth.ValidateAuth(username, password);
                return Request.CreateResponse(HttpStatusCode.OK, auth);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Enter Valid Details");
            }
        }
    }
}
