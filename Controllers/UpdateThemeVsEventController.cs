﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class UpdateThemeVsEventController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage POST(HttpRequestMessage request)
        {
            JSONResponse jsonresponse = new JSONResponse();

            string params_send = Request.Content.ReadAsStringAsync().Result;
            UpdateThemeVsEventModel input = new UpdateThemeVsEventModel();
            try
            {
                input = JsonConvert.DeserializeObject<UpdateThemeVsEventModel>(params_send.ToString());
            }
            catch(Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.BadRequest, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
            int linkId = input.linkId;
            var themeVsEventJson = input.ThemeVsEventJson.ToString(); ;
            try
            {
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    dbContext.UpdateThemeVsEventJsonByLinkId(linkId, themeVsEventJson);
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Review Updated" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }
}
