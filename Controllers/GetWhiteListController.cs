﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class GetWhiteListController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET()
        {
            JSONResponse jsonresponse = new JSONResponse();

            List<WhiteListLink> whiteListLinks = new List<WhiteListLink>();
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    whiteListLinks = dbContext.WhiteListLinks.ToList();
                    //whiteListLinks = ReadFromDB.GetWhiteListLinks();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = whiteListLinks };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }
                catch (Exception ex)
                {
                    var quesAns = dbContext.FAQs.ToList();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = quesAns };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }
            }

        }
    }
}
