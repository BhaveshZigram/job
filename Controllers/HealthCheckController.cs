﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DragnetAlphaWebAPI.Controllers
{
    public class HealthCheckController : ApiController
    {
        // GET: api/HealthCheck   
        public string Get()
        {
            return "Dragnet Aplha Api service is up and running";
        }
    }
}
