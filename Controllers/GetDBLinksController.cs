﻿using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;

namespace DragnetAlphaWebAPI.Controllers
{
    //  [Authorize]
    public class GetDBLinksController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public void GET(HttpRequestMessage request)
        {



        }

        public HttpResponseMessage POST(HttpRequestMessage request)
        {
            JSONResponse jsonresponse = new JSONResponse();

            string params_send = request.Content.ReadAsStringAsync().Result;
            ReviewStatus input = JsonConvert.DeserializeObject<ReviewStatus>(params_send);
            int reviewStatus = input.ReviewStatusCode;
            int linkId = input.LinkId;
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    NewsLink link = dbContext.NewsLinks.First(x => x.id == linkId);
                    link.ReviewStatus_Id = reviewStatus;
                    dbContext.SaveChanges();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Review Updated" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }

        public HttpResponseMessage PUT(HttpRequestMessage request)
        {
            JSONResponse jsonresponse = new JSONResponse();

            string params_send = request.Content.ReadAsStringAsync().Result;
            ReviewStatus input = JsonConvert.DeserializeObject<ReviewStatus>(params_send);
            string comment = input.Comment;
            int linkId = input.LinkId;
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    var links = dbContext.NewsLinks.Where(x => x.id == linkId).ToList();
                    foreach (var link in links)
                    {
                        link.Comment = comment;
                        dbContext.SaveChanges();
                    }
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Comment Updated" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }
    }
}
