﻿using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    //[ValidateUser]
    public class UpdateSelectedEntityController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage POST(HttpRequestMessage request)
        {
            JSONResponse jsonresponse = new JSONResponse();
            try
            {
                string params_send = Request.Content.ReadAsStringAsync().Result;
                SelectedEntityModel input = new SelectedEntityModel();
                try
                {
                    input = JsonConvert.DeserializeObject<SelectedEntityModel>(params_send.ToString());
                }
                catch
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.BadRequest, Message = "Invalid Format" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
                var EntitiesSelected = input.EntitySelected;
                int projectId = input.ProjectId;
                var Language = input.Language;
                var SourceSelected = input.SourceSelected;
                bool ManualSupervision = input.ManualSupervision;
                bool OneTimeMonitoring = input.OneTimeMonitoring;
                string TimeFrameSelected = input.TimeFrameSelected;
              
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Allocated Successfully" };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }
}
