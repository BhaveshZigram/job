﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Models;

using System.Data.Entity.Core.Objects;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class AnalyticsDashboardController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public HttpResponseMessage POST(string project, int userId)
        {
            JSONResponse jsonresponse = new JSONResponse();

            // CountForAnalytics countForAnalytics = new CountForAnalytics();
            //  List<AnalyticsModel> trials = new List<AnalyticsModel>();
            List<AnalyticsModel> analytics = new List<AnalyticsModel>();
            DateTime dateTime = DateTime.Today.AddDays(-30);
            //string fromDate = dateTime.ToString("yyyy-MM-dd");

            try
            {
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);

                    int projectId = projectInfo.FirstOrDefault().ProjectID;

                    var result = dbContext.GetLinkFlagAnalyticsMaker(projectId, userId, dateTime);
                    foreach (var res in result)
                    {
                        analytics.Add(new AnalyticsModel
                        {
                            FlagName = res.FlagName,
                            Id = res.LinkFlag_Id,
                            Count = res.Counts
                        });
                    }
                    var result1 = dbContext.GetReviewStatusAnalyticsMaker(projectId, userId, dateTime);
                    foreach (var res in result1)
                    {
                        analytics.Add(new AnalyticsModel
                        {
                            StatusName = res.StatusName,
                            Id = res.ReviewStatus_Id,
                            Count = res.Counts
                        });
                    }
                    ObjectResult<int?> result2 = dbContext.GetTotalLinksAnalyticsMaker(projectId, userId, dateTime);
                    analytics.Add(new AnalyticsModel
                    {
                        TotalLinks = result2.FirstOrDefault().Value
                    });


                    // trials = ReadFromDB.GetLinksCount(dataAsset,userId,role);
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = analytics };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }
            }

            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }
}