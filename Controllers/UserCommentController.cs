﻿using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    public class UserCommentController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage PUT(HttpRequestMessage request)
        {
            string params_send = request.Content.ReadAsStringAsync().Result;
            ReviewStatus input = JsonConvert.DeserializeObject<ReviewStatus>(params_send);
            string comment = input.Comment;
            int linkId = input.LinkId;
            using (DragnetAlpha_DbV1Entities dbContext = new DragnetAlpha_DbV1Entities())
            {
                try
                {
                    var links = dbContext.Links.Where(x => x.LinkId == linkId).ToList();
                    foreach (var link in links)
                    {
                        link.Comment = comment;
                        dbContext.SaveChanges();
                    }
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "True");
                }
                catch
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "False");
                }
            }
        }
    }
}
