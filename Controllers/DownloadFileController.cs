﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    public class DownloadFileController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public dynamic GET(int projectId, string fileFormat)
        {
            //DateTime today = DateTime.Today.AddDays(-3);
            DateTime weekBefore = DateTime.Today.AddDays(-30);
            List<LinksModel> MainTagLinks = new List<LinksModel>();

            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    //var linksFromDB = (from p in dbContext.NewsLinks
                    //                   join e in dbContext.ApplicationsUsers
                    //                   on p.user_id equals e.id into gf
                    //                   from subpet in gf.DefaultIfEmpty()
                    //                   join r in dbContext.ReviewStatus
                    //                   on p.ReviewStatus_Id equals r.id
                    //                   where p.project_id == projectId && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.CreatedAt > today && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                    //                   select new
                    //                   { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson }).OrderByDescending(a => a.RelevanceScore).ToList();


                    //var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson }).ToList();

                    //List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson }).ToList();

                    //var linksDisplayCopy = LinkModelList;
                    var linksFromDB = (from p in dbContext.NewsLinks
                                       join e in dbContext.ApplicationsUsers
                                       on p.user_id equals e.id into gf
                                       from subpet in gf.DefaultIfEmpty()
                                       join r in dbContext.ReviewStatus
                                       on p.ReviewStatus_Id equals r.id
                                       where p.project_id == projectId && p.DuplicateFlag == "No" && p.RelevanceScore > 50 && p.CreatedAt > weekBefore && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                       select new
                                       { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(a => a.RelevanceScore).ToList();


                    var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                    List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                    var linksDisplayCopy = LinkModelList;

                    MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                    MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                    linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                    foreach (var maintaglink in MainTagLinks)
                    {
                        var Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).Select(y => y.Link).ToList();
                        if (Grouping.Count > 0)
                        {
                            string similarLinks = "";
                            foreach (var Groupelink in Grouping)
                            {
                                var a = Groupelink;
                                similarLinks = similarLinks + ", " + a;

                            }
                            similarLinks = similarLinks.Remove(0, 2);
                            maintaglink.SimilarLinks = similarLinks;
                        }
                    }

                    List<LinkEntity> LinkModelList1 = MainTagLinks.Select(p => new LinkEntity() { NewsLink = p.Link, Heading = p.Heading, NewsProvider = p.NewsProvider, DatePublish = p.DatePublished, Description = p.Description, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, Relevance = p.RelevanceScore, SourceReputation = p.SourceReputation, Summary = p.Summary, Keywords = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, Event = p.EventsTagged, SimilarLinks = p.SimilarLinks }).ToList();

                    string name = dbContext.Projects.Where(x => x.id == projectId).Select(y => y.ProjectName).FirstOrDefault().Replace(" ", "");
                    string result = Path.GetTempPath();
                    string fileName = name + "_NewsLinks";
                       // "DragnetAlpha_AccentureLinks";
                    string fileNameCSV = "";
                    string fileNameXML = "";

                    LinkEntities linkEntities = new LinkEntities();
                    linkEntities.LinkEntity = LinkModelList1;
                    string downloadUrl1 = "";
                    string downloadUrl2 = "";
                    if (fileFormat.Contains("csv"))
                    {
                        fileNameCSV = $"{fileName}.csv";
                        string CSVfilepath = $"{result}{fileNameCSV}";
                        ExportToCSVMethod exportToCSVMethod = new ExportToCSVMethod();
                        exportToCSVMethod.ExportAsCSV(LinkModelList1, CSVfilepath);
                        BlobMethods.UploadToBlob(CSVfilepath, fileNameCSV, "dragnet-alpha");
                        downloadUrl1 = $"https://altx.blob.core.windows.net/dragnet-alpha/{fileNameCSV}?sv=2019-02-02&si=downloadDAfile&sr=c&sig=XtenN5%2FYrQjM2NdVffyz2B9H66%2BvmZV65swORoIQvfQ%3D";
                    }
                    if (fileFormat.Contains("xml"))
                    {
                        fileNameXML = $"{fileName}.xml";
                        string XMLfilepath = $"{result}{fileNameXML}";
                        var XmlString = ExportToXMLMethod.ToXML(linkEntities);
                        System.IO.File.WriteAllText(XMLfilepath, XmlString);
                        BlobMethods.UploadToBlob(XMLfilepath, fileNameXML, "dragnet-alpha");
                        downloadUrl2 = $"https://altx.blob.core.windows.net/dragnet-alpha/{fileNameXML}?sv=2019-02-02&si=downloadDAfile&sr=c&sig=XtenN5%2FYrQjM2NdVffyz2B9H66%2BvmZV65swORoIQvfQ%3D";

                    }
                    DownloadFileUrlResponseModel downloadFileUrlResponseModel = new DownloadFileUrlResponseModel();
                    downloadFileUrlResponseModel.CSVDownloadUrl = downloadUrl1;
                    downloadFileUrlResponseModel.XMLDownloadUrl = downloadUrl2;

                    List<DownloadFileUrlResponseModel> downloadUrl = new List<DownloadFileUrlResponseModel>();
                    downloadUrl.Add(downloadFileUrlResponseModel);
                    return downloadUrl;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException);
                    return null;
                }

            }
        }

        public dynamic PUT(int projectId, string fileFormat, int TargetEntityId)
        {
            //DateTime today = DateTime.Today.AddDays(-3);
            DateTime weekBefore = DateTime.Today.AddDays(-30);
            List<LinksModel> MainTagLinks = new List<LinksModel>();

            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    var linksFromDB = (from p in dbContext.NewsLinks
                                       join e in dbContext.ApplicationsUsers
                                       on p.user_id equals e.id into gf
                                       from subpet in gf.DefaultIfEmpty()
                                       join r in dbContext.ReviewStatus
                                       on p.ReviewStatus_Id equals r.id
                                       where p.project_id == projectId && p.DuplicateFlag == "No" && p.RelevanceScore > 50 && p.CreatedAt > weekBefore && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                       select new
                                       { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(a => a.RelevanceScore).ToList();


                    var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                    List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                    var linksDisplayCopy = LinkModelList;


                    MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                    linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                    foreach (var maintaglink in MainTagLinks)
                    {
                        var Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).Select(y => y.Link).ToList();
                        if (Grouping.Count > 0)
                        {
                            string similarLinks = "";
                            foreach (var Groupelink in Grouping)
                            {
                                var a = Groupelink;
                                similarLinks = similarLinks + ", " + a;

                            }
                            similarLinks = similarLinks.Remove(0, 2);
                            maintaglink.SimilarLinks = similarLinks;
                        }
                    }

                    List<LinkEntity> LinkModelList1 = MainTagLinks.Select(p => new LinkEntity() { NewsLink = p.Link, Heading = p.Heading, NewsProvider = p.NewsProvider, DatePublish = p.DatePublished, Description = p.Description, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, Relevance = p.RelevanceScore, SourceReputation = p.SourceReputation, Summary = p.Summary, Keywords = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, Event = p.EventsTagged, SimilarLinks = p.SimilarLinks }).ToList();
                    string name = dbContext.Projects.Where(x => x.id == projectId).Select(y => y.ProjectName).FirstOrDefault().Replace(" ", "");
                    string result = Path.GetTempPath();
                    string fileName = name + "_NewsLinks";
                    //string fileName = "DragnetAlpha_AccentureLinks";
                    string fileNameCSV = "";
                    string fileNameXML = "";

                    LinkEntities linkEntities = new LinkEntities();
                    linkEntities.LinkEntity = LinkModelList1;
                    string downloadUrl1 = "";
                    string downloadUrl2 = "";
                    if (fileFormat.Contains("csv"))
                    {
                        fileNameCSV = $"{fileName}.csv";
                        string CSVfilepath = $"{result}{fileNameCSV}";
                        ExportToCSVMethod exportToCSVMethod = new ExportToCSVMethod();
                        exportToCSVMethod.ExportAsCSV(LinkModelList1, CSVfilepath);
                        BlobMethods.UploadToBlob(CSVfilepath, fileNameCSV, "dragnet-alpha");
                        downloadUrl1 = $"https://altx.blob.core.windows.net/dragnet-alpha/{fileNameCSV}?sv=2019-02-02&si=downloadDAfile&sr=c&sig=XtenN5%2FYrQjM2NdVffyz2B9H66%2BvmZV65swORoIQvfQ%3D";
                    }
                    if (fileFormat.Contains("xml"))
                    {
                        fileNameXML = $"{fileName}.xml";
                        string XMLfilepath = $"{result}{fileNameXML}";
                        var XmlString = ExportToXMLMethod.ToXML(linkEntities);
                        System.IO.File.WriteAllText(XMLfilepath, XmlString);
                        BlobMethods.UploadToBlob(XMLfilepath, fileNameXML, "dragnet-alpha");
                        downloadUrl2 = $"https://altx.blob.core.windows.net/dragnet-alpha/{fileNameXML}?sv=2019-02-02&si=downloadDAfile&sr=c&sig=XtenN5%2FYrQjM2NdVffyz2B9H66%2BvmZV65swORoIQvfQ%3D";

                    }
                    DownloadFileUrlResponseModel downloadFileUrlResponseModel = new DownloadFileUrlResponseModel();
                    downloadFileUrlResponseModel.CSVDownloadUrl = downloadUrl1;
                    downloadFileUrlResponseModel.XMLDownloadUrl = downloadUrl2;

                    List<DownloadFileUrlResponseModel> downloadUrl = new List<DownloadFileUrlResponseModel>();
                    downloadUrl.Add(downloadFileUrlResponseModel);
                    return downloadUrl;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException);
                    return null;
                }

            }
        }
    }
}
