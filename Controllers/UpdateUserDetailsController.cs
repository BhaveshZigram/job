﻿
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class UpdateUserDetailsController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        // POST api/<controller>
        public HttpResponseMessage Post(UserDetailsModel input)
        {
            try
            {
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    int userId = input.UserId;
                    ApplicationsUser toUpdate1 = dbContext.ApplicationsUsers.FirstOrDefault(x => x.id == userId);

                    ImageWriterHelper imageWriterHelper = new ImageWriterHelper();
                    byte[] ProjectImage = null;
                    string imageType = "";
                    if (input.ProjectImage != null)
                    {
                        ProjectImage = input.ProjectImage;

                        imageType = ImageWriterHelper.GetImageFormat(ProjectImage).ToString();
                    }
                    else
                    {
                        ProjectImage = toUpdate1.ProjectImage;
                        imageType = toUpdate1.ImageType;

                    }
                    ApplicationsUser toUpdate = dbContext.ApplicationsUsers.FirstOrDefault(x => x.id == userId);

                    if (input.FirstName != null && input.LastName != null)
                    {
                        toUpdate.FirstName = input.FirstName;
                        toUpdate.LastName = input.LastName;
                        toUpdate.ContactNumber = input.ContactNumber;
                        toUpdate.SecondaryEmail = input.SecondaryEmail;
                        toUpdate.Country = input.Country;
                        toUpdate.Company = input.Company;
                        toUpdate.OfficialEmail = input.OfficialEmail;
                        toUpdate.Department = input.Department;
                        toUpdate.Designation = input.Designation;
                        toUpdate.Industry = input.Industry;
                        toUpdate.ProjectImage = input.ProjectImage;
                        toUpdate.ImageType = imageType;
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "First Name and Last Name are Mandatory");
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, "Profile Updated Successfully");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}