﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class DeleteBlackLinkController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode=HttpStatusCode.OK};


        }

        public HttpResponseMessage POST(HttpRequestMessage requestMessage)
        {
            JSONResponse jsonresponse = new JSONResponse();
            try
            {
                BlackListLink blackListLink = new BlackListLink();
            string paramsSend = Request.Content.ReadAsStringAsync().Result;
            blackListLink = JsonConvert.DeserializeObject<BlackListLink>(paramsSend.ToString());
            
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    var toDelete=dbContext.BlackListLinks.SingleOrDefault(x => x.Id == blackListLink.Id);
                    dbContext.BlackListLinks.Remove(toDelete);
                    dbContext.SaveChanges();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Black Link Deleted" };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }
}
