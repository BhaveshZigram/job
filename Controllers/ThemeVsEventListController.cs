﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using DragnetAlphaWebAPI.Models;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class ThemeVsEventListController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public dynamic GET(int projectId)
        {
            ThemeVsEventModel themeVsEventModel = new ThemeVsEventModel();
           
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    //ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
                    //int projectId = projectInfo.FirstOrDefault().ProjectID;
                    var themeLists = dbContext.ThemeLists.Where(x => x.Project_id == projectId && x.Active_Status==true).ToList();
                    themeVsEventModel.ThemeVsEvent = new List<ThemeModel>();
                    foreach (var themeList in themeLists)
                    {
                        List<EventModel> eventModels = themeList.EventLists.Where(x=>x.Project_id== projectId && x.Active_Status==true).OrderBy(o=>o.Criticality_id).Select(o => new EventModel() { EventId = o.Id, EventName = o.EventName, ColorCode=o.ColorCode, Criticality = o.Criticality.Status }).ToList();
                        ThemeModel themeModel = new ThemeModel() { ColorCode = themeList.ColorCode, ColorName = themeList.ColouName, ThemeId = themeList.Id, ThemeName = themeList.ThemeName, EventList = eventModels };
                        themeVsEventModel.ThemeVsEvent.Add(themeModel);
                    }
                    //EventModel abc = new EventModel() { EventId = 1, EventName = "Loss" };
                    //ThemeModel abcd = new ThemeModel() { ColorCode = "#1234", ColorName = "Orange", ThemeName = "Finance", ThemeId = 1, EventList = new List<EventModel>() { abc } };
                    //var json = new JavaScriptSerializer().Serialize(themeVsEventModel);
                        return themeVsEventModel;
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}
