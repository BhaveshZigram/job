﻿
using DragnetAlphaWebAPI.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
      [Authorize]
    public class GlobalSearchController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET(int projectId, string stringsearched, int roleid)
        {
            JSONResponse jsonresponse = new JSONResponse();

            DateTime TodayDate = DateTime.Today;
            DateTime weekBefore = DateTime.Today.AddDays(-14);

            List<NewsLink> links = new List<NewsLink>();
            if (stringsearched != null)
            {
                string[] SearchedStringArray = stringsearched.Split(' ');
                int wordinString = SearchedStringArray.Count();
                var percentage = (double)wordinString / (double)2;
                int wordcountmatched = (int)Math.Ceiling(percentage);
                List<LinksModel> LinkModelList1 = new List<LinksModel>();
                List<LinksModel> LinkModelList2 = new List<LinksModel>();
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                    try
                    {

                        if (roleid == 4)
                        {


                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join s in dbContext.SearchTerms
                                                                                           on p.SearchTerm_id equals s.id
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.DuplicateFlag == "No" && p.RelevanceScore > 50 && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2) && p.GroupingId != null
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            LinkModelList1 = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, ConcatenatedData = p.EventsTagged + " " + p.Theme + " " + p.NewsProvider + " " + p.TargetedEntity + " " + p.Tags + " " + p.KeywordsFound + " " + p.id }).ToList();
                            LinkModelList2 = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, ConcatenatedData2 = p.Description + " " + p.Heading + " " + p.ImageUrl + " " + p.SentimentSentence + " " + p.Summary + " " + p.id }).ToList();
                        }
                        else
                        {
                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join s in dbContext.SearchTerms
                                               on p.SearchTerm_id equals s.id
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.DuplicateFlag == "No" && p.GroupingId != null
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(a => a.RelevanceScore).ToList();


                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            LinkModelList1 = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, ConcatenatedData = p.EventsTagged + " " + p.Theme + " " + p.NewsProvider + " " + p.TargetedEntity + " " + p.Tags + " " + p.KeywordsFound + " " + p.id }).ToList();
                            LinkModelList2 = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, ConcatenatedData2 = p.Description + " " + p.Heading + " " + p.ImageUrl + " " + p.SentimentSentence + " " + p.Summary + " " + p.id }).ToList();


                        }
                        List<LinksModel> ResultLinkModelList1 = new List<LinksModel>();
                        List<GlobalSearchModel> ResultCountbasedList = new List<GlobalSearchModel>();
                        List<LinksModel> ResultWordCountLinkModelList = new List<LinksModel>();

                        foreach (var str in SearchedStringArray)
                        {
                            var resultedrows = LinkModelList1.Where(o => o.ConcatenatedData.ToLower().Contains(str.ToLower())).ToList();
                            ResultLinkModelList1.AddRange(resultedrows);
                        }



                        var result = ResultLinkModelList1.GroupBy(x => x.LinkId);
                        foreach (var grp in result)
                        {
                            GlobalSearchModel globalSearchModel = new GlobalSearchModel();
                            globalSearchModel.Foundcount = grp.Count();
                            globalSearchModel.linkid = grp.Key;
                            ResultCountbasedList.Add(globalSearchModel);
                        }
                        var sortedResultCountbasedList = ResultCountbasedList.OrderByDescending(o => o.Foundcount).ToList();
                        foreach (var i in ResultCountbasedList)
                        {
                            if (i.Foundcount >= wordcountmatched)
                            {
                                var linkmodel = LinkModelList1.Where(o => o.LinkId == i.linkid).ToList();
                                linkmodel[0].ConcatenatedData = null;
                                ResultWordCountLinkModelList.Add(linkmodel[0]);
                            }
                        }
                        var resultedrowsExactMatch = LinkModelList2.Where(o => o.ConcatenatedData2.Contains(stringsearched)).ToList();
                        ResultWordCountLinkModelList.AddRange(resultedrowsExactMatch);


                        jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = ResultWordCountLinkModelList };
                        return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);

                    }

                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.InnerException);
                        return null;
                    }
            }
            else
            {
                if (roleid == 4)
                {
                    using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                    {
                        List<LinksModel> MainTagLinks = new List<LinksModel>();
                        try
                        {
                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.DuplicateFlag == "No" && p.RelevanceScore > 50 && p.CreatedAt > weekBefore && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(a => a.RelevanceScore).ToList();


                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                            var linksDisplayCopy = LinkModelList;

                            MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                            linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                            foreach (var maintaglink in MainTagLinks)
                            {
                                maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                            }
                            jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.OK, Body = MainTagLinks };
                            return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                        }
                        catch (Exception ex)
                        {
                            jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                            return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                        }
                    }
                }
                else
                {
                    using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                        try
                        {
                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join s in dbContext.SearchTerms
                                                                                          on p.SearchTerm_id equals s.id
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.CreatedAt > TodayDate && p.DuplicateFlag == "No" && p.RelevanceScore > 50 && p.GroupingId != null
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                            var linksDisplayCopy = LinkModelList;

                            var MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                            linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                            foreach (var maintaglink in MainTagLinks)
                            {
                                maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                            }
                            jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = MainTagLinks };
                            return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                        }
                        catch (Exception ex)
                        {
                            jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                            return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                        }
                }
            }

        }

        public HttpResponseMessage PUT(int projectId, string stringsearched, int roleid, int TargetEntityId)
        {
            JSONResponse jsonresponse = new JSONResponse();

            DateTime TodayDate = DateTime.Today;
            DateTime weekBefore = DateTime.Today.AddDays(-14);

            List<NewsLink> links = new List<NewsLink>();
            if (stringsearched != null)
            {
                string[] SearchedStringArray = stringsearched.Split(' ');
                int wordinString = SearchedStringArray.Count();
                var percentage = (double)wordinString / (double)2;
                int wordcountmatched = (int)Math.Ceiling(percentage);
                List<LinksModel> LinkModelList1 = new List<LinksModel>();
                List<LinksModel> LinkModelList2 = new List<LinksModel>();
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                    try
                    {

                        if (roleid == 4)
                        {


                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join s in dbContext.SearchTerms
                                                                                           on p.SearchTerm_id equals s.id
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.DuplicateFlag == "No" && p.RelevanceScore > 50 && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2) && p.TargetedEntityId== TargetEntityId && p.GroupingId != null
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            LinkModelList1 = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, ConcatenatedData = p.EventsTagged + " " + p.Theme + " " + p.NewsProvider + " " + p.TargetedEntity + " " + p.Tags + " " + p.KeywordsFound + " " + p.id }).ToList();
                            LinkModelList2 = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, ConcatenatedData2 = p.Description + " " + p.Heading + " " + p.ImageUrl + " " + p.SentimentSentence + " " + p.Summary + " " + p.id }).ToList();
                        }
                        else
                        {
                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join s in dbContext.SearchTerms
                                               on p.SearchTerm_id equals s.id
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.DuplicateFlag == "No" && p.TargetedEntityId == TargetEntityId && p.GroupingId != null
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(a => a.RelevanceScore).ToList();


                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            LinkModelList1 = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, ConcatenatedData = p.EventsTagged + " " + p.Theme + " " + p.NewsProvider + " " + p.TargetedEntity + " " + p.Tags + " " + p.KeywordsFound + " " + p.id }).ToList();
                            LinkModelList2 = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, ConcatenatedData2 = p.Description + " " + p.Heading + " " + p.ImageUrl + " " + p.SentimentSentence + " " + p.Summary + " " + p.id }).ToList();


                        }
                        List<LinksModel> ResultLinkModelList1 = new List<LinksModel>();
                        List<GlobalSearchModel> ResultCountbasedList = new List<GlobalSearchModel>();
                        List<LinksModel> ResultWordCountLinkModelList = new List<LinksModel>();

                        foreach (var str in SearchedStringArray)
                        {
                            var resultedrows = LinkModelList1.Where(o => o.ConcatenatedData.ToLower().Contains(str.ToLower())).ToList();
                            ResultLinkModelList1.AddRange(resultedrows);
                        }



                        var result = ResultLinkModelList1.GroupBy(x => x.LinkId);
                        foreach (var grp in result)
                        {
                            GlobalSearchModel globalSearchModel = new GlobalSearchModel();
                            globalSearchModel.Foundcount = grp.Count();
                            globalSearchModel.linkid = grp.Key;
                            ResultCountbasedList.Add(globalSearchModel);
                        }
                        var sortedResultCountbasedList = ResultCountbasedList.OrderByDescending(o => o.Foundcount).ToList();
                        foreach (var i in ResultCountbasedList)
                        {
                            if (i.Foundcount >= wordcountmatched)
                            {
                                var linkmodel = LinkModelList1.Where(o => o.LinkId == i.linkid).ToList();
                                linkmodel[0].ConcatenatedData = null;
                                ResultWordCountLinkModelList.Add(linkmodel[0]);
                            }
                        }
                        var resultedrowsExactMatch = LinkModelList2.Where(o => o.ConcatenatedData2.Contains(stringsearched)).ToList();
                        ResultWordCountLinkModelList.AddRange(resultedrowsExactMatch);


                        jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = ResultWordCountLinkModelList };
                        return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);

                    }

                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.InnerException);
                        return null;
                    }
            }
            else
            {
                if (roleid == 4)
                {
                    using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                    {
                        List<LinksModel> MainTagLinks = new List<LinksModel>();
                        try
                        {
                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.DuplicateFlag == "No" && p.RelevanceScore > 50 && p.CreatedAt > weekBefore && p.TargetedEntityId == TargetEntityId && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(a => a.RelevanceScore).ToList();


                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                            var linksDisplayCopy = LinkModelList;

                            MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                            linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                            foreach (var maintaglink in MainTagLinks)
                            {
                                maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                            }
                            jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.OK, Body = MainTagLinks };
                            return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                        }
                        catch (Exception ex)
                        {
                            jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                            return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                        }
                    }
                }
                else
                {
                    using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                        try
                        {
                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join s in dbContext.SearchTerms
                                                                                          on p.SearchTerm_id equals s.id
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.CreatedAt > TodayDate && p.DuplicateFlag == "No" && p.RelevanceScore > 50 && p.TargetedEntityId == TargetEntityId && p.GroupingId != null
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                            var linksDisplayCopy = LinkModelList;

                            var MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                            linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                            foreach (var maintaglink in MainTagLinks)
                            {
                                maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                            }
                            jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = MainTagLinks };
                            return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                        }
                        catch (Exception ex)
                        {
                            jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                            return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                        }
                }
            }

        }
    }
}
