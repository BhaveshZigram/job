﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    public class CompanyProfilesController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET(int projectId)
        {
            JSONResponse jsonresponse = new JSONResponse();

            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    List<CompanyProfileModel> Profile = new List<CompanyProfileModel>();
                    //ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);

                    //int projectId = projectInfo.FirstOrDefault().ProjectID;
                    List<CompanyProfile> CompanyProfiles = dbContext.CompanyProfiles.Where(x=>x.Project_id== projectId).OrderBy(a => a.EntityName).ToList();
                    foreach (var profile in CompanyProfiles)
                    {
                        List<string> Bod = new List<string>();
                        if (profile.BoardofDirectors != null)
                        {
                            Bod = profile.BoardofDirectors.Replace("[", "").Replace("]", "").Split(',').ToList();
                        }
                        List<string> founder = new List<string>();
                        if (profile.Founders != null)
                        {
                            founder = profile.Founders.Replace("[", "").Replace("]", "").Split(',').ToList();
                        }
                        List<string> subsi = new List<string>();
                        if (profile.Subsidiaries != null)
                        {
                            subsi = profile.Subsidiaries.Replace("[", "").Replace("]", "").Split(',').ToList();
                        }
                        List<string> otherNamesforSearch = new List<string>();
                        if (profile.OtherNamesforSearch != null)
                        {
                            otherNamesforSearch = profile.OtherNamesforSearch.Replace("[", "").Replace("]", "").Split(',').ToList();
                        }
                        CompanyProfileModel companyProfile = new CompanyProfileModel();
                        var hits = String.Format("{0:n0}", profile.Hits);
                        companyProfile = new CompanyProfileModel
                        {
                            EntityName = profile.EntityName,
                            BoardofDirectors = Bod,
                            Founders = founder,
                            Subsidiaries = subsi,
                            Sector = profile.Sector,
                            CEO = profile.CEO,
                            CFO = profile.CFO,
                            CompanyType_Global_Local = profile.CompanyType_Global_Local,
                            Id = profile.Id,
                            Industry = profile.Industry,
                            MarketIndex = profile.MarketIndex,
                            FinancialType_PublicorPrivate = profile.FinancialType_PublicorPrivate,
                            Headquarters = profile.Headquarters,
                            OtherNamesforSearch = otherNamesforSearch,
                            Website = profile.Website,
                            TradeName = profile.TradeName,
                            Hits = hits,
                            CompanyLogoUrl = profile.CompanyLogoUrl
                        };
                        Profile.Add(companyProfile);
                    }
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = Profile };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                    // return Profile;
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    //return null;
                }
            }
        }
    }
}
