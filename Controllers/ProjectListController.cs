﻿using DragnetAlphaWebAPI.Filters;
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    //[Authorize]
    //[ValidateUser]

    public class ProjectListController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public HttpResponseMessage GET(int UserId)
        {
            JSONResponse jsonresponse = new JSONResponse();
            try
            {
                //   List<Project> projectInfo = new List<Project>();
                //List<ProjectInformation> projectInfo = new List<ProjectInformation>();
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    //foreach (var proj in projectName)
                    List<Project> projectInfo = dbContext.UserProjectRoleMappings.Where(y => y.ApplicationsUser.id == UserId).Select(o => o.Project).ToList();

                    List<ProjectInfo> projectInfos = new List<ProjectInfo>();
                    foreach (var project in projectInfo)
                    {
                        ProjectInfo projectInfo1 = new ProjectInfo();
                        projectInfo1.ProjectID = project.id;
                        projectInfo1.ProjectName = project.ProjectName;
                        projectInfo1.ProjectImage = project.ProjectImage;
                        ImageWriterHelper imageWriterHelper = new ImageWriterHelper();
                        projectInfo1.ImageType = ImageWriterHelper.GetImageFormat(projectInfo1.ProjectImage).ToString();
                        projectInfos.Add(projectInfo1);
                    }
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = projectInfos };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                    //return projectInfos;
                }
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }
}
