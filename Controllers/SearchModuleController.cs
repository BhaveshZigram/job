﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    public class SearchModuleController : ApiController
    {
        DragnetAlpha_DbV2Entities1 entities1 = new DragnetAlpha_DbV2Entities1();
        JSONResponse json;

        public HttpResponseMessage GET()
        {
            try
            {
                var news = entities1.NewsSources.Select(n => new { n.SourceName,n.Id, n.Active_Status, n.ScriptType, n.ScriptURL, n.KeyMapping }).ToList();
                List<Search_NewsSourceModel> nmodel = new List<Search_NewsSourceModel>();
                foreach(var data in news)
                {
                    Search_NewsSourceModel newsSourceModel = new Search_NewsSourceModel()
                    {
                        SourceName=data.SourceName,
                        Id=data.Id,
                        Active_Status=data.Active_Status,
                        ScriptType=data.ScriptType,
                        ScriptURL=data.ScriptURL,
                        KeyMapping=data.KeyMapping
                    };
                    nmodel.Add(newsSourceModel);
                }
                TimeFrame_Language_Monntoring_SourceListModel model = new TimeFrame_Language_Monntoring_SourceListModel()
                {
                    timeframe = entities1.Search_TimeFrame.ToList(),
                    monitoring = entities1.Search_Monitoring.ToList(),
                    language = entities1.Search_Language.ToList(),
                    news = nmodel
                };
                json = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message ="Data Send in Body",Body=model};
                return Request.CreateResponse(json);
            }
            catch(Exception ex)
            {
                json = new JSONResponse() {Success=false,ResponseCode=HttpStatusCode.BadRequest,Message=ex.Message };
                return Request.CreateResponse(json);
            }

        }
    }
}
