﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using System.Data.Entity.Core.Objects;
using DragnetAlphaWebAPI.Models.Enums;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class DateFilterForReleventLinksController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public dynamic GET(string dateFrom, string dateTo, int id, string role, string project)
        {
            string fromDate = dateFrom;
            string toDate = dateTo;
            string[] todate = dateTo.Split('-');
            int date = int.Parse(todate[2]) + 1;
            string NextDate = " ";
            var todayDate = DateTime.Now;
            var todayDate1 = todayDate.ToString("yyyy-MM-dd");
            if (toDate == todayDate1)
            {
                var DateTomorrow = todayDate.AddDays(1);
                NextDate = DateTomorrow.ToString("yyyy-MM-dd");
            }
            else
            {
                string toDateUpdated = todate[0] + "-" + todate[1] + "-" + date.ToString();
                NextDate = toDateUpdated;
            }
            //List<BingResult> result = new List<BingResult>();
            //result = ReadFromDB.GetDateWiseReleventLinks(fromDate, NextDate, id, project);
            //string readUrl = string.Concat($"select distinct Link, Heading, Description, HashCode, DatePublished, NewsProvider, Category, Comment, ReviewStatus, SubCategory,ImageUrl, LinkFlag  from crbNewsLinks where MakerId={id} and DataAsset='{DataAsset}' and CreatedAt < '{toDate}' and CreatedAt >= '{fromDate}' and (LinkFlag={1} or LinkFlag={2})");
            DateTime From = DateTime.Parse(fromDate);
            DateTime to = DateTime.Parse(NextDate);
            using (DragnetAlpha_DbV1Entities1 dbContext = new DragnetAlpha_DbV1Entities1())
            {
                ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
                int projectId = projectInfo.FirstOrDefault().ProjectID;

                if (role == "Maker")

                {
                    try
                    {
                        //var links = dbContext.Links.Where(x => x.MakerId == id && x.CreatedAt > from && x.CreatedAt < to && x.ProjectId == projectId && (x.LinkFlagId == 1 || x.LinkFlagId == 2)).Distinct().Select(i => new { i.Link1, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlagId, i.ReviewStatusId, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTermId, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
                        //return links;
                        string MakerName = dbContext.Users.Where(z => z.UserId == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                        var linksFromDB = (from p in dbContext.Links
                                           join e in dbContext.Users
                                           on p.MakerId equals e.UserId into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join s in dbContext.AllSearchStrings
                                           on p.SearchTermId equals s.SearchStringId
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatusId equals r.ReviewStatusId
                                           where p.ProjectId == projectId && p.CreatedAt < to && p.CreatedAt > From && (p.LinkFlagId == 1 || p.LinkFlagId == 2) && (p.MakerId == id)
                                           select new
                                           { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlagId, reviewstatus = r.StatusName.ToString(), p.MakerId, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, s.SubCategory, p.LinkId }).ToList();

                        //     var linksFromDB = dbContext.Links.Where(z => z.ProjectId == projectId && z.MakerId == id && z.CreatedAt < to && z.CreatedAt > from && (z.LinkFlagId == 1 || z.LinkFlagId == 2)).Distinct().OrderByDescending(y => y.DatePublished).Select(i => new { i.Link1, i.Heading, i.ImageUrl, i.LinkCategorization,i.LinkFlagId, reviewstatus = ((ReviewStatusEnum)i.ReviewStatusId).ToString(), i.MakerId, MakerName, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTermId, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory, i.Sentiment, i.SentimentScore, i.SentimentSentence, i.RelevanceScore, i.Tags }).ToList();
                        return linksFromDB.Select(p => new { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlagId, p.reviewstatus, p.MakerId, p.MakerName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.LinkId }).ToList();


                    }
                    catch
                    {
                        return null;
                    }
                }
                else if (role == "superadmin" || role == "Admin" || role == "SuperAdmin" || role == "MAAdmin")
                {
                    try
                    {
                        //var links = dbContext.Links.Where(x => x.MakerId == id && x.CreatedAt > from && x.CreatedAt < to && x.ProjectId == projectId && (x.LinkFlagId == 1 || x.LinkFlagId == 2)).Distinct().Select(i => new { i.Link1, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlagId, i.ReviewStatusId, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTermId, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
                        //return links;
                        string MakerName = dbContext.Users.Where(z => z.UserId == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                        var linksFromDB = (from p in dbContext.Links
                                           join e in dbContext.Users
                                           on p.MakerId equals e.UserId into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join s in dbContext.AllSearchStrings
                                           on p.SearchTermId equals s.SearchStringId
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatusId equals r.ReviewStatusId
                                           where p.ProjectId == projectId && p.CreatedAt < to && p.CreatedAt > From && (p.LinkFlagId == 1 || p.LinkFlagId == 2)
                                           select new
                                           { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlagId, reviewstatus = r.StatusName.ToString(), p.MakerId, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, s.SubCategory, p.LinkId }).ToList();

                        //     var linksFromDB = dbContext.Links.Where(z => z.ProjectId == projectId && z.MakerId == id && z.CreatedAt < to && z.CreatedAt > from && (z.LinkFlagId == 1 || z.LinkFlagId == 2)).Distinct().OrderByDescending(y => y.DatePublished).Select(i => new { i.Link1, i.Heading, i.ImageUrl, i.LinkCategorization,i.LinkFlagId, reviewstatus = ((ReviewStatusEnum)i.ReviewStatusId).ToString(), i.MakerId, MakerName, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTermId, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory, i.Sentiment, i.SentimentScore, i.SentimentSentence, i.RelevanceScore, i.Tags }).ToList();
                        return linksFromDB.Select(p => new { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlagId, p.reviewstatus, p.MakerId, p.MakerName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.LinkId }).ToList();


                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                    return null;
            }
        }
    }
}

