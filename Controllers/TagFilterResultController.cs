﻿
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class TagFilterResultController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public dynamic GET(int Id, string Role, int projectId, string tagSelected)
        {
            DateTime TodayDate = DateTime.Today;
            string YesterdayDate = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
            //var da = DataAsset;
            //var params_send = request.Content.ReadAsStringAsync().Result;
            //var input = JsonConvert.DeserializeObject<AllocatedLink>(params_send);
            int id = Id;
            string role = Role;
            List<NewsLink> links = new List<NewsLink>();

            if (role == "Maker")
            {
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    try
                    {

                        string MakerName = dbContext.ApplicationsUsers.Where(z => z.id == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                        //ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
                        //int projectId = projectInfo.ToList().FirstOrDefault().ProjectID;

                        var linksFromDB = (from p in dbContext.NewsLinks
                                           join e in dbContext.ApplicationsUsers
                                           on p.user_id equals e.id into gf
                                           from subpet in gf.DefaultIfEmpty()
join s in dbContext.SearchTerms
                                           on p.SearchTerm_id equals s.id
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatus_Id equals r.id
                                           where p.project_id == projectId && p.CreatedAt > TodayDate && p.user_id == id && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null
                                           select new
                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson }).OrderByDescending(a => a.RelevanceScore).ToList();


                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson }).ToList();

                        List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson }).ToList();

                        var linksDisplayCopy = LinkModelList;

                        var MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                        linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                        foreach (var maintaglink in MainTagLinks)
                        {
                            maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                        }

                        return MainTagLinks;
                    }
                    catch
                    {
                        return null;
                    }

                }
                // string readUrl = string.Concat($"select distinct Link, Heading, Description, HashCode, DatePublished, NewsProvider, Category, Comment, ReviewStatus, SubCategory, ImageUrl, LinkFlag, MakerId from crbNewsLinks where CreatedAt > '{TodaysDate}' and MakerId = {id} and DataAsset = '{dataAsset}' order by DatePublished desc");

                //result = ReadFromDB.GetTodaysLinks(TodayDate, id, dataAsset, YesterdayDate);
            }
            else if (role == "Admin" || role == "SuperAdmin")
            {
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                    try
                    {

                        var linksFromDB = (from p in dbContext.NewsLinks
                                           join e in dbContext.ApplicationsUsers
                                           on p.user_id equals e.id into gf
                                           from subpet in gf.DefaultIfEmpty()
join s in dbContext.SearchTerms
                                           on p.SearchTerm_id equals s.id
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatus_Id equals r.id
                                           where p.project_id == projectId && p.CreatedAt > TodayDate && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null && p.Tags.Contains(tagSelected)
                                           select new
                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson }).OrderByDescending(a => a.RelevanceScore).ToList();


                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson }).ToList();

                        List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson }).ToList();

                        var linksDisplayCopy = LinkModelList;

                        var MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                        linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                        foreach (var maintaglink in MainTagLinks)
                        {
                            maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                        }

                        return MainTagLinks;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.InnerException);
                        return null;
                    }

                //result = ReadFromDB.GetAdminTodaysLinks(TodayDate, DataAsset, YesterdayDate);
            }
            else
                return null;
        }
    }
}
