﻿using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    public class ExistingThemeEventUpdateController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public dynamic POST(HttpRequestMessage request)
        {
            string params_send = Request.Content.ReadAsStringAsync().Result;
            ThemeVsEventModel input = new ThemeVsEventModel();
            try
            {
                input = JsonConvert.DeserializeObject<ThemeVsEventModel>(params_send.ToString());

            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "ThemevsEvent Updation Request Failed");
            }
            var dataDeserialized = input.ThemeVsEvent;
            DateTime dateTime = DateTime.Parse("2021-01-11");

            string NextDate = dateTime.ToString("yyyy-MM-dd");
            DateTime dateTimeTo = DateTime.Parse(NextDate);

            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                ThemeVsEventModel themeVsEventModel = new ThemeVsEventModel();
                var linksFromDB = (from p in dbContext.NewsLinks
                                   where p.project_id == 59 && p.CreatedAt > dateTimeTo && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null && p.ThemeVsEventJson == null
                                   select new
                                   { p.id, p.project_id, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.KeywordsFound }).ToList();

                var linksDisplay = linksFromDB.Select(p => new { p.project_id, p.id, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.KeywordsFound }).ToList();

                List<LinksModel> LinkModelList = linksFromDB.Select(p => new LinksModel() { ProjectId = p.project_id, LinkId = p.id, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, KeywordsFound = p.KeywordsFound }).ToList();
                Parallel.ForEach(LinkModelList, new ParallelOptions { MaxDegreeOfParallelism = 1 }, link =>
                {
                    //    foreach (var link in LinkModelList)
                    //{
                    int linkid = link.LinkId;
                    string[] themes = link.Theme.Split(',');
                    string[] eventtagged = link.KeywordsFound.Split(',');

                    foreach (var data in dataDeserialized)
                    {
                        foreach (var t in themes)
                        {
                            if (data.ThemeName == t.Trim())
                            {
                                foreach (var e in eventtagged)
                                {
                                    data.ThemeAvailable = true;
                                    foreach (var eventavailable in data.EventList)
                                    {
                                        if (eventavailable.EventName.ToLower() == e.Trim())
                                        {
                                            eventavailable.EventAvailable = true;
                                        }
                                    }
                                }
                            }
                        }
                    }


                    var ThemeVsEventjson = new JavaScriptSerializer().Serialize(dataDeserialized);
                    NewsLink toSet = dbContext.NewsLinks.FirstOrDefault(x => x.id == link.LinkId);
                    toSet.ThemeVsEventJson = ThemeVsEventjson;
                    dbContext.SaveChanges();
                });
                //dbContext.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, "Updated Successfully");
            }
        }
    }
}