﻿using DragnetAlphaWebAPI.Filters;
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]
    public class LogOutController : ApiController
    {
        public HttpResponseMessage Options()
        {

            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public HttpResponseMessage Post()
        {
            JSONResponse jsonresponse = new JSONResponse();
            try
            {
                string token = Request.Headers.GetValues("Authorization").FirstOrDefault().Split(' ')[1];
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    dbContext.BlacklistTokens.Add(new BlacklistToken() { Token = token, CreatedAt = DateTime.UtcNow });
                    dbContext.SaveChanges();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Logout successfully" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
            //finally
            //{
            //    UserActivityLogging.Log(jsonresponse);
            //}
        }
    }
}
