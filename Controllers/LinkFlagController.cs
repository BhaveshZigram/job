﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Linq;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class LinkFlagController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage POST(HttpRequestMessage request)
        {
            JSONResponse jsonresponse = new JSONResponse();

            try
            {
                string params_send = request.Content.ReadAsStringAsync().Result;
                Models.LinkFlag input = JsonConvert.DeserializeObject<Models.LinkFlag>(params_send);
                int linkFlag = input.LinkFlags;
                //int projectId = input.projectId;
                int LinkId = input.LinkId;
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    var link1 = dbContext.UpdateLinkFlagByLinkId(LinkId, linkFlag);

                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Link Flag Updated" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }
}

