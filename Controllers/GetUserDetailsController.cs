﻿
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class GetUserDetailsController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public HttpResponseMessage GET(int Userid)
        {
            JSONResponse jsonresponse = new JSONResponse();

            List<ApplicationsUser> UserDetails = new List<ApplicationsUser>();
            List<string> projects = new List<string>();
            UserDetailsModel userDetailsModel = new UserDetailsModel();
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    UserDetails = dbContext.ApplicationsUsers.Where(x => x.id == Userid).ToList();
                    var projectList = dbContext.UserProjectRoleMappings.Where(x => x.user_id == Userid).Select(y => y.project_id).ToList();
                    foreach (var project in projectList)
                    {
                        var ProjectName = dbContext.Projects.Where(x => x.id == project).Select(y => y.ProjectName).ToList();
                        projects.Add(ProjectName[0]);
                    }
                    var roleName = dbContext.UserProjectRoleMappings.Where(x => x.user_id == Userid).Select(y => y.ApplicationRole.RoleName).ToList()[0];
                    //var name = dbContext.GetRoleName(roleId);
                    //string rolename = dbContext.ApplicationRoles.Where(x=>x.id== roleId)
                    //string roleName = name.FirstOrDefault();
                    userDetailsModel = new UserDetailsModel()
                    {
                        FirstName = UserDetails[0].FirstName,
                        LastName = UserDetails[0].LastName,
                        OfficialEmail = UserDetails[0].OfficialEmail,
                        Designation = UserDetails[0].Designation,
                        Department = UserDetails[0].Department,
                        ContactNumber = UserDetails[0].ContactNumber,
                        Company = UserDetails[0].Company,
                        Industry = UserDetails[0].Industry,
                        LinkedinProfileURL = UserDetails[0].LinkedinProfileURL,
                        SecondaryEmail = UserDetails[0].SecondaryEmail,
                        Country = UserDetails[0].Country,
                        Role = roleName,
                        ProjectaAssigned = projects,
                        UserName = UserDetails[0].UserName,
                        ProjectImage = UserDetails[0].ProjectImage,
                        ImageType = UserDetails[0].ImageType
                    };
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = userDetailsModel };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }
    }
}