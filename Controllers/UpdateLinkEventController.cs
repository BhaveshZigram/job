﻿using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    //[Authorize]
    //[ValidateUser]

    public class UpdateLinkEventController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage POST(HttpRequestMessage request)
        {
            JSONResponse jsonresponse = new JSONResponse();

            string params_send = Request.Content.ReadAsStringAsync().Result;
            EventUpdate input = new EventUpdate();
            try
            {
                input = JsonConvert.DeserializeObject<EventUpdate>(params_send.ToString());
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Event Updation Request Failed");
            }
            string project = input.Project;
            int linkId = input.LinkId;
            string events = input.EventSelected;
            try
            {
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    //var toSet = dbContext.NewsLinks.Where(x => x.GroupingId == linkId && x.Project.ProjectName == project).ToList();
                    //foreach (var link in toSet)
                    //{
                    //    link.Theme = themes;
                    //    dbContext.SaveChanges();
                    //}
                    dbContext.UpdateEventByLinkId(linkId, events);
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Review Updated" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }

    }
}
