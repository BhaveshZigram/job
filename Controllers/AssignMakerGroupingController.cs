﻿
using DragnetAlphaWebAPI.Filters;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    //[ValidateUser]

    public class AssignMakerGroupingController : ApiController
    {

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage POST(HttpRequestMessage request)
        {
            JSONResponse jsonresponse = new JSONResponse();
            try
            {
                string params_send = Request.Content.ReadAsStringAsync().Result;
                UserAssign input = new UserAssign();
                try
                {
                    input = JsonConvert.DeserializeObject<UserAssign>(params_send.ToString());
                }
                catch
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.BadRequest, Message = "Invalid Format" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
                int makerId = input.MakerId;
                int projectId = input.projectId;
                string linkId = input.LinkId;
                string[] LinkIds = linkId.Split(',');
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    foreach (string linkid in LinkIds)
                    {
                        int linkIds = Int32.Parse(linkid);
                        var toSet = dbContext.NewsLinks.Where(x => x.GroupingId == linkIds && x.project_id == projectId).ToList();
                        foreach(var toset in toSet)
                        {
                            toset.user_id = makerId;
                            dbContext.SaveChanges();
                        }
                        //toSet.user_id = makerId;
                        //dbContext.SaveChanges();
                    }
                }
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Allocated Successfully" };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }
}
