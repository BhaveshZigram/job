﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity.Core.Objects;
using DragnetAlphaWebAPI.Utilities;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class LinkTypeController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public HttpResponseMessage GET(HttpRequestMessage requestMessage)
        {
            JSONResponse jsonresponse = new JSONResponse();

            string paramsSend = requestMessage.Content.ReadAsStringAsync().Result;
            LinkType input = JsonConvert.DeserializeObject<LinkType>(paramsSend);
            string project = input.Project;
            string hashCode = input.HashCode;
            int userId = input.UserId;
            string linkType = input.LinkCategory;
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    NewsLink toUpdate = dbContext.NewsLinks.FirstOrDefault(x => x.LinkHashCode == hashCode && x.user_id == userId && x.Project.ProjectName == project);
                    toUpdate.LinkCategorization = linkType;
                    dbContext.SaveChanges();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Link Type Updated Successfully" };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }
    }
}