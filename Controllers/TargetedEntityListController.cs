﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    public class TargetedEntityListController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET(int projectId)
        {
            JSONResponse jsonresponse = new JSONResponse();
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    List<TargetEntity> targetEntityList = new List<TargetEntity>();
                    List<TargetEntity> TargetEntityList = dbContext.TargetEntities.Where(x => x.Project.id == projectId).ToList();
                    foreach (var Entity in TargetEntityList)
                    {
                        targetEntityList.Add(new TargetEntity
                        {
                            id = Entity.id,
                            TargetedEntity = Entity.TargetedEntity,
                            CountryCode = Entity.CountryCode,
                            project_id = Entity.project_id,
                            ActiveStatus = Entity.ActiveStatus
                        });
                    }
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = targetEntityList };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }
    }
}
