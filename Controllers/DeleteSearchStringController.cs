﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class DeleteSearchStringController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage POST(HttpRequestMessage requestMessage)
        {
            JSONResponse jsonresponse = new JSONResponse();

            string paramsSend = Request.Content.ReadAsStringAsync().Result;
            AllSearchString searchStrings = new AllSearchString();
            searchStrings = JsonConvert.DeserializeObject<AllSearchString>(paramsSend.ToString());
            //string response = UpdateToDB.DeleteSearchTerm(searchStringInput.Id,searchStringInput.Category);
            //string query = $"Delete from CRB_SearchString where Id = {id} and Category='{dataAsset}'";
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    var toDelete = dbContext.AllSearchStrings.FirstOrDefault(x => x.ProjectName == searchStrings.ProjectName && x.SearchStringId == searchStrings.SearchStringId);
                    dbContext.AllSearchStrings.Remove(toDelete);
                    dbContext.SaveChanges();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Search String Deleted" };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }

                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }
    }
}
    