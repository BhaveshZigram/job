﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using System.Data.Entity.Core.Objects;
using DragnetAlphaWebAPI.Models.Enums;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class DateFilterController : ApiController
    {

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public dynamic GET(string dateFrom, string dateTo, int id, string role, string project)
        {
            string fromDate = dateFrom;
            string toDate = dateTo;
            string[] todate = dateTo.Split('-');
            int date = int.Parse(todate[2]) + 1;
            string NextDate = " ";
            var TodayDate = DateTime.Now;
            var TodayDate1 = TodayDate.ToString("yyyy-MM-dd");
            if (toDate == TodayDate1)
            {
                var DateTomorrow = TodayDate.AddDays(1);
                NextDate = DateTomorrow.ToString("yyyy-MM-dd");
            }
            else
            {
                string toDateUpdated = todate[0] + "-" + todate[1] + "-" + date.ToString();
                NextDate = toDateUpdated;
            }
            List<BingResult> result = new List<BingResult>();
            DateTime dateTimeFrom = DateTime.Parse(fromDate);
            DateTime dateTimeTo = DateTime.Parse(NextDate);
            using (DragnetAlpha_DbV1Entities1 dbContext = new DragnetAlpha_DbV1Entities1())
            {
              
                    ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
                int projectId = projectInfo.FirstOrDefault().ProjectID;

                if (role == "Maker")

                {
                    try
                    {
                        //var links = dbContext.Links.Where(x => x.DatePublished > dateTimeFrom && x.DatePublished < dateTimeTo && x.MakerId == id && x.ProjectId == projectId).Distinct().OrderByDescending(y => y.DatePublished).Select(i => new { i.Link1, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlagId, i.ReviewStatusId, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTermId, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
                        //return links;
                        string MakerName = dbContext.Users.Where(z => z.UserId == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                        var linksFromDB = (from p in dbContext.Links
                                           join e in dbContext.Users
                                           on p.MakerId equals e.UserId into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join r in dbContext.ReviewStatus
                                            on p.ReviewStatusId equals r.ReviewStatusId
                                           where p.ProjectId == projectId && p.DatePublished > dateTimeFrom && p.DatePublished < dateTimeTo && p.MakerId == id
                                           select new
                                           { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlagId, reviewstatus = r.StatusName.ToString(), p.MakerId, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.LinkId }).Distinct().OrderByDescending(p => p.DatePublished).ToList();
                        return linksFromDB.Select(p => new { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlagId, p.reviewstatus, p.MakerId, p.MakerName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.LinkId }).ToList();

                     //var linksFromDB = dbContext.Links.Where(z => z.ProjectId == projectId && z.MakerId == id &&  z.DatePublished > dateTimeFrom && z.DatePublished < dateTimeTo).Distinct().OrderByDescending(y => y.DatePublished).Select(i => new { i.Link1, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlagId, reviewstatus = ((ReviewStatusEnum)i.ReviewStatusId).ToString(), i.MakerId, MakerName, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTermId, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory, i.Sentiment, i.SentimentScore, i.SentimentSentence, i.RelevanceScore, i.Tags }).ToList();
                      //return linksFromDB.Select(p => new { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlagId, p.reviewstatus, p.MakerId, p.MakerName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags }).ToList();

                    }
                    catch
                    {
                        return null;
                    }
                }
                //if (role == "Maker")
                //{
                //    result = ReadFromDB.GetdateWiseLinks(fromDate, NextDate, id, project);
                //}
                else if (role == "superadmin" || role == "Admin" || role == "SuperAdmin" || role == "MAAdmin")
                {
                    try
                    {
                        //var links = dbContext.Links.Where(x => x.DatePublished > dateTimeFrom && x.DatePublished < dateTimeTo && x.ProjectId == projectId).Distinct().OrderByDescending(y => y.DatePublished).Select(i => new { i.Link1, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlagId, i.ReviewStatusId, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTermId, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
                        //return links;

                        var linksFromDB = (from p in dbContext.Links
                                           join e in dbContext.Users
                                           on p.MakerId equals e.UserId into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatusId equals r.ReviewStatusId
                                           where p.ProjectId == projectId &&  p.DatePublished > dateTimeFrom && p.DatePublished < dateTimeTo
                                           select new
                                           { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization,p.LinkFlagId, reviewstatus = r.StatusName.ToString(), p.MakerId, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.LinkId }).Distinct().OrderByDescending(p => p.DatePublished).ToList();
                         return linksFromDB.Select(p => new { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlagId, p.reviewstatus, p.MakerId, p.MakerName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.LinkId }).ToList();

                    }
                    //result = ReadFromDB.GetdateWiseLinks(fromDate, NextDate, project);
                    catch
                    {
                        return null;            //        }
                    }
                }
                else
                    return null;
            }
        }
}
}