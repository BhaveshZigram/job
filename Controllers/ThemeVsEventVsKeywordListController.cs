﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class ThemeVsEventVsKeywordListController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public HttpResponseMessage GET(int projectId)
        {
            JSONResponse jsonresponse = new JSONResponse();

            try
            {
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    ThemeVsEventModel themeVsEventModel = new ThemeVsEventModel();

                    //ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
                    //int projectId = projectInfo.FirstOrDefault().ProjectID;

                    dbContext.Configuration.AutoDetectChangesEnabled = false;
                    var themeLists = dbContext.ThemeLists.Where(x=>x.Project_id== projectId && x.Active_Status==true).ToList();
                    themeVsEventModel.ThemeVsEvent = new List<ThemeModel>();
                    //var EventLists = dbContext.EventLists.ToList();
                    //var KeyWordLists = dbContext.KeywordsEvents.Include("EventName").Include("ThemeName").ToList();
                    themeVsEventModel.ThemeVsEvent = new List<ThemeModel>();
                    foreach (var themeList in themeLists)
                    {
                        List<EventModel> eventModels = themeList.EventLists.Where(x=>x.Project_id== projectId && x.Active_Status == true).OrderBy(x=>x.Criticality_id).Select(o => new EventModel() { EventId = o.Id, EventName = o.EventName, ColorCode=o.ColorCode, Criticality = o.Criticality.Status }).ToList();
                        foreach (var eachevent in eventModels)
                        {
                            eachevent.KeyWordList = dbContext.KeywordsEvents.Where(x => x.EventId == eachevent.EventId).ToList().Select(y => new KeywordModel() { Keyword = y.keyword, KeywordId = y.Id, Criticality=y.Criticality.Status }).ToList();
                        }
                        ThemeModel themeModel = new ThemeModel() { ColorCode = themeList.ColorCode, ColorName = themeList.ColouName, ThemeId = themeList.Id, ThemeName = themeList.ThemeName, EventList = eventModels };
                        themeVsEventModel.ThemeVsEvent.Add(themeModel);
                    }
                    //EventModel abc = new EventModel() { EventId = 1, EventName = "Loss" };
                    //ThemeModel abcd = new ThemeModel() { ColorCode = "#1234", ColorName = "Orange", ThemeName = "Finance", ThemeId = 1, EventList = new List<EventModel>() { abc } };
                    //var json = new JavaScriptSerializer().Serialize(themeVsEventModel);

                    //foreach (var themeList in themeLists)
                    //{
                    //    ThemeModel themeModel = new ThemeModel() { ColorCode = themeList.ColorCode, ColorName = themeList.ColouName, ThemeId = themeList.Id, ThemeName = themeList.ThemeName };
                    //    themeVsEventModel.ThemeVsEvent.Add(themeModel);
                    //}
                    var events = themeVsEventModel.ThemeVsEvent;


                    //events = events.Select(x =>
                    //{
                    //    if (x.ThemeName == searchStrings1.Category && x.EventList.Where(d => d.EventName == searchStrings1.SubCategory).Count() == 0)
                    //    {
                    //        try
                    //        {
                    //            var z = x.EventList.Select(y => y.EventName == searchStrings1.SubCategory).FirstOrDefault();

                    //        }
                    //        catch (Exception ex)
                    //        {

                    //        }
                    //    }
                    //    x.EventList = x.EventList.DistinctBy(p => new { p.EventId, p.EventName }).ToList();
                    //    return x;
                    //}).ToList();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = themeVsEventModel };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);


                }
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }
}
