﻿using DragnetAlphaWebAPI.Methods;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;
using System.Data.Entity.Core.Objects;
using DragnetAlphaWebAPI.Models;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;

namespace DragnetAlphaWebAPI.Controllers
{
    // [Authorize]
    public class ExtractLinksController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public string GET(int projectId)
        {
            try
            {
                //int linkCount = 15;
                //List<SearchTermMapping> searchStringList = new List<SearchTermMapping>();
                //List<NewsLink> bingAndGoogleLink = new List<NewsLink>();
                //List<NewsLink> allResult = new List<NewsLink>();
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    //    ThemeVsEventModel themeVsEventModel = new ThemeVsEventModel();
                    //    var NewsSource = dbContext.ProjectNewsSourceMappings.Where(x => x.project_id == projectId && x.Active_Status == true && x.NewsSource.Active_Status == true).Select(y => y.NewsSource.SourceName).ToList();
                    //    var NewsSource1 = dbContext.ProjectNewsSourceMappings.Where(x => x.project_id == projectId && x.Active_Status==true && x.NewsSource.Active_Status==true).ToList();

                    //    dbContext.Configuration.AutoDetectChangesEnabled = false;
                    //    var themeLists = dbContext.ThemeLists.Where(x => x.Project_id == projectId).ToList();
                    //    themeVsEventModel.ThemeVsEvent = new List<ThemeModel>();
                    //    var EventLists = dbContext.EventLists.Where(x => x.Project_id == projectId).ToList();

                    //    themeVsEventModel.ThemeVsEvent = new List<ThemeModel>();
                    //    foreach (var themeList in themeLists)
                    //    {
                    //        List<EventModel> eventModels = themeList.EventLists.Select(o => new EventModel() { EventId = o.Id, EventName = o.EventName }).ToList();
                    //        ThemeModel themeModel = new ThemeModel() { ColorCode = themeList.ColorCode, ColorName = themeList.ColouName, ThemeId = themeList.Id, ThemeName = themeList.ThemeName, EventList = eventModels };
                    //        themeVsEventModel.ThemeVsEvent.Add(themeModel);
                    //    }
                    //    var json = new JavaScriptSerializer().Serialize(themeVsEventModel);
                    //    //var searchStrings = dbContext.SearchTerms.Where(x => x.Project_id == projectId).ToList();
                    //    var entityToHit = dbContext.TargetEntities.Where(x => x.project_id == projectId).ToList();
                    //    //ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
                    //    //int projectId = projectInfo.FirstOrDefault().ProjectID;
                    //    SearchStringFormation searchStringFormation = new SearchStringFormation();
                    //var StgOutput = searchStringFormation.GenerateSearchString_New(projectId, NewsSource, NewsSource1);


                    //SearchNewsBing searchNewsBing = new SearchNewsBing();

                    //var daycount = dbContext.DecisionParameters.Where(x => x.project_id == projectId).Select(y=>y.NewsExtract_Day_Threshold).FirstOrDefault();
                    Task taskA = Task.Run(() =>
                    {
                        //NewsServices.BingNewsExtract(NewsSource, StgOutput, linkCount, daycount, themeVsEventModel, projectId);
                        //NewsServices.GoogleNewsExtract(NewsSource, StgOutput, linkCount, daycount, themeVsEventModel, projectId);
                        NewsServices.DataBreachExtract(projectId);
                       NewsServices.RSSFeedNewsExtract(projectId);
                    //NewsServices.InvestingNewsExtract(projectId);

                    }); 
                    //return output;
                    return "News Link Extraction Started";
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.InnerException);
                return "News Link Failed";
            }
        }


    }
}
