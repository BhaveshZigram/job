﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Models;
using System.Text;
using System.Data.Entity.Core.Objects;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class AddProjectController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public HttpResponseMessage POST(ProjectInformation projectInformation)
        {
            JSONResponse jsonresponse = new JSONResponse();

            try
            {
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    var ImageType = ImageWriterHelper.GetImageFormat(projectInformation.ImageInfo).ToString();
                    var CheckIfAlreadyExist = dbContext.Projects.Where(o => o.ProjectName == projectInformation.Name).ToList();
                    if (CheckIfAlreadyExist.Count() == 0)
                    {
                        var newProject = new Project
                        {
                            ProjectName = projectInformation.Name,
                            ProjectImage = projectInformation.ImageInfo,
                            ImageType = ImageType
                        };
                        dbContext.Projects.Add(newProject);
                        try
                        {
                            dbContext.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.BadRequest, Message = ex.Message };
                            return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                        }
                    }
                    else
                    {
                        jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.Ambiguous, Message = "Project Name Already Exist" };
                        return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                    }
                    ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(projectInformation.Name);
                    int projectId = projectInfo.FirstOrDefault().id;
                    UserProjectRoleMapping userProjectMapping = new UserProjectRoleMapping
                    {
                        ApplicationRole_id = 1,
                        project_id = projectId,
                        user_id=3,                       
                    };
                    dbContext.UserProjectRoleMappings.Add(userProjectMapping);
                    dbContext.SaveChanges();
                }
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Project Added Successfully" };
                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }

    public class ImageWriterHelper
    {
        public enum ImageFormat
        {
            Bmp,
            Jpeg,
            Gif,
            Tiff,
            Png,
            Unknown
        }

        public static ImageFormat GetImageFormat(byte[] bytes)
        {
            var bmp = Encoding.ASCII.GetBytes("BM");
            var gif = Encoding.ASCII.GetBytes("GIF");
            var png = new byte[] { 137, 80, 78, 71 };
            var tiff = new byte[] { 73, 73, 42 };
            var tiff2 = new byte[] { 77, 77, 42 };
            var jpeg = new byte[] { 255, 216, 255, 224 };
            var jpeg2 = new byte[] { 255, 216, 255, 225 };

            if (bmp.SequenceEqual(bytes.Take(bmp.Length)))
                return ImageFormat.Bmp;

            if (gif.SequenceEqual(bytes.Take(gif.Length)))
                return ImageFormat.Gif;

            if (png.SequenceEqual(bytes.Take(png.Length)))
                return ImageFormat.Png;

            if (tiff.SequenceEqual(bytes.Take(tiff.Length)))
                return ImageFormat.Tiff;

            if (tiff2.SequenceEqual(bytes.Take(tiff2.Length)))
                return ImageFormat.Tiff;

            if (jpeg.SequenceEqual(bytes.Take(jpeg.Length)))
                return ImageFormat.Jpeg;

            if (jpeg2.SequenceEqual(bytes.Take(jpeg2.Length)))
                return ImageFormat.Jpeg;

            return ImageFormat.Unknown;
        }
    }
}

