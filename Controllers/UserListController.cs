﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Linq;
using System.Data.Entity.Core.Objects;
using System;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class UserListController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET(int projectId)
        {
            JSONResponse jsonresponse = new JSONResponse();
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    List<UserList> users = new List<UserList>();
                    List<UserProjectRoleMapping> userProjectMapping = dbContext.UserProjectRoleMappings.Include("ApplicationsUser").Include("Project").Where(x => x.Project.id == projectId).Where(x=>x.ApplicationRole_id!=4).ToList();
                    foreach (var user1 in userProjectMapping)
                    {
                        users.Add(new UserList
                        {
                            UserId = user1.user_id,
                            UserName = user1.ApplicationsUser.UserName,
                            FullName = user1.ApplicationsUser.FirstName + " " + user1.ApplicationsUser.LastName
                        });
                    }
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = users };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }
    }
}
