﻿using DragnetAlphaWebAPI.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class ThemeFilterRelevanceResultController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        public HttpResponseMessage GET(int id, string role, int projectId, string themeSelected, Boolean relevanceResult, string dateFrom, string dateTo, string eventSelected)
        {
            JSONResponse jsonresponse = new JSONResponse();

            DateTime today = DateTime.Today.AddDays(1);
            DateTime TodayDate = DateTime.Today;
            DateTime weekBefore = DateTime.Today.AddDays(-7);
            List<LinksModel> MainTagLinks = new List<LinksModel>();
            List<LinksModel> LinkModelList = new List<LinksModel>();
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    // var projectInfo = dbContext.GetProjectId(project);
                    // var linkResults = dbContext.NewsLinks.Where(x => x.CreatedAt > weekBefore && x.CreatedAt < today && x.ProjectId== projectId).Where(z => z.LinkFlag_Id == 1 || z.LinkFlag_Id == 2).Distinct().Select(i => new { i.Link, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlag_Id, i.ReviewStatus_Id, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTerm_id, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
                    if (relevanceResult == true)
                    {
                        var linksFromDB = (from p in dbContext.NewsLinks
                                           join e in dbContext.ApplicationsUsers
                                           on p.user_id equals e.id into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join s in dbContext.SearchTerms
                                                                                      on p.SearchTerm_id equals s.id
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatus_Id equals r.id
                                           where p.project_id == projectId && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null && p.CreatedAt < today && p.CreatedAt > weekBefore && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                           select new
                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                        LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId=p.TargetedEntityId }).ToList();
                    }
                    else if (dateFrom != null)
                    {

                        DateTime dateTime = DateTime.Parse(dateTo);
                        dateTime = dateTime.AddDays(1);
                        string NextDate = dateTime.ToString("yyyy-MM-dd");
                        DateTime From = DateTime.Parse(dateFrom);
                        DateTime to = DateTime.Parse(NextDate);
                        string MakerName = dbContext.ApplicationsUsers.Where(z => z.id == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                        if (relevanceResult == true)
                        {
                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join s in dbContext.SearchTerms
                                                                                          on p.SearchTerm_id equals s.id
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.CreatedAt < to && p.CreatedAt > From && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                            MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();
                        }
                        else
                        {
                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join s in dbContext.SearchTerms
                                                                                          on p.SearchTerm_id equals s.id
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.CreatedAt < to && p.CreatedAt > From && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                        }
                    }
                    else if (relevanceResult == false && dateFrom == null)
                    {
                        var linksFromDB = (from p in dbContext.NewsLinks
                                           join e in dbContext.ApplicationsUsers
                                           on p.user_id equals e.id into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join s in dbContext.SearchTerms
                                                                                      on p.SearchTerm_id equals s.id
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatus_Id equals r.id
                                           where p.project_id == projectId && p.CreatedAt > TodayDate && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                           select new
                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                        LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                    }
                    var linksDisplayCopy = LinkModelList;

                    MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                    linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                    foreach (var maintaglink in MainTagLinks)
                    {
                        maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException);
                    return Request.CreateResponse(HttpStatusCode.OK, ex);
                }
            }
            if (themeSelected != null && eventSelected != null)
            {
                string[] themesSelected = themeSelected.Split(',');
                List<LinksModel> LinkModelList1 = MainTagLinks.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.ReviewStatu, MakerId = p.MakerId, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.ProjectId, SearchTerm_id = p.SearchTerm_id, AdminId = p.AdminId, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished, Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, SubCategory = p.SubCategory, LinkId = p.LinkId, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, ConcatenatedData = p.Theme + " " + p.EventsTagged + " " + p.LinkId }).ToList();

                List<LinksModel> ResultLinkModelList = new List<LinksModel>();

                foreach (var str in themesSelected)
                {
                    var resultedrows = LinkModelList1.Where(o => o.ConcatenatedData.Contains(str)).ToList();
                    ResultLinkModelList.AddRange(resultedrows);
                }
                string[] eventsSelected = eventSelected.Split(',');

                List<LinksModel> ResultLinkModelListFinal = new List<LinksModel>();
                foreach (var str in eventsSelected)
                {
                    var resultedrows = ResultLinkModelList.Where(o => o.ConcatenatedData.Contains(str)).ToList();
                    ResultLinkModelListFinal.AddRange(resultedrows);
                }

                var result = ResultLinkModelListFinal.DistinctBy(x => x.LinkId).ToList();
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = result };
                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);

                //var result = ResultLinkModelList.DistinctBy(x => x.LinkId).ToList();
                //return result;
            }
            if (themeSelected != null && eventSelected == null)
            {
                string[] themesSelected = themeSelected.Split(',');
                List<LinksModel> LinkModelList1 = MainTagLinks.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.ReviewStatu, MakerId = p.MakerId, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.ProjectId, SearchTerm_id = p.SearchTerm_id, AdminId = p.AdminId, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished, Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, SubCategory = p.SubCategory, LinkId = p.LinkId, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, EventsTagged = p.EventsTagged , ConcatenatedData = p.Theme + " " + p.EventsTagged + " " + p.LinkId }).ToList();

                List<LinksModel> ResultLinkModelList = new List<LinksModel>();

                foreach (var str in themesSelected)
                {
                    var resultedrows = LinkModelList1.Where(o => o.ConcatenatedData.Contains(str)).ToList();
                    ResultLinkModelList.AddRange(resultedrows);
                }


                var result = ResultLinkModelList.DistinctBy(x => x.LinkId).ToList();
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = result };
                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);

                //var result = ResultLinkModelList.DistinctBy(x => x.LinkId).ToList();
                //return result;
            }
            else
            {
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = MainTagLinks };
                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
            }
        }
        public HttpResponseMessage PUT(int id, string role, int projectId, string themeSelected, Boolean relevanceResult, string dateFrom, string dateTo, string eventSelected, int TargetEntityId)
        {
            JSONResponse jsonresponse = new JSONResponse();

            DateTime today = DateTime.Today.AddDays(1);
            DateTime TodayDate = DateTime.Today;
            DateTime weekBefore = DateTime.Today.AddDays(-7);
            List<LinksModel> MainTagLinks = new List<LinksModel>();
            List<LinksModel> LinkModelList = new List<LinksModel>();
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    // var projectInfo = dbContext.GetProjectId(project);
                    // var linkResults = dbContext.NewsLinks.Where(x => x.CreatedAt > weekBefore && x.CreatedAt < today && x.ProjectId== projectId).Where(z => z.LinkFlag_Id == 1 || z.LinkFlag_Id == 2).Distinct().Select(i => new { i.Link, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlag_Id, i.ReviewStatus_Id, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTerm_id, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
                    if (relevanceResult == true)
                    {
                        var linksFromDB = (from p in dbContext.NewsLinks
                                           join e in dbContext.ApplicationsUsers
                                           on p.user_id equals e.id into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join s in dbContext.SearchTerms
                                                                                      on p.SearchTerm_id equals s.id
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatus_Id equals r.id
                                           where p.project_id == projectId && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null && p.CreatedAt < today && p.CreatedAt > weekBefore && p.TargetedEntityId == TargetEntityId && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                           select new
                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                        LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();
                    }
                    else if (dateFrom != null)
                    {

                        DateTime dateTime = DateTime.Parse(dateTo);
                        dateTime = dateTime.AddDays(1);
                        string NextDate = dateTime.ToString("yyyy-MM-dd");
                        DateTime From = DateTime.Parse(dateFrom);
                        DateTime to = DateTime.Parse(NextDate);
                        string MakerName = dbContext.ApplicationsUsers.Where(z => z.id == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
                        if (relevanceResult == true)
                        {
                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join s in dbContext.SearchTerms
                                                                                          on p.SearchTerm_id equals s.id
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.CreatedAt < to && p.CreatedAt > From && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null && p.TargetedEntityId == TargetEntityId && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                            MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();
                        }
                        else
                        {
                            var linksFromDB = (from p in dbContext.NewsLinks
                                               join e in dbContext.ApplicationsUsers
                                               on p.user_id equals e.id into gf
                                               from subpet in gf.DefaultIfEmpty()
                                               join s in dbContext.SearchTerms
                                                                                          on p.SearchTerm_id equals s.id
                                               join r in dbContext.ReviewStatus
                                               on p.ReviewStatus_Id equals r.id
                                               where p.project_id == projectId && p.CreatedAt < to && p.CreatedAt > From && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null && p.TargetedEntityId == TargetEntityId && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                               select new
                                               { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                            var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                            LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                        }
                    }
                    else if (relevanceResult == false && dateFrom == null)
                    {
                        var linksFromDB = (from p in dbContext.NewsLinks
                                           join e in dbContext.ApplicationsUsers
                                           on p.user_id equals e.id into gf
                                           from subpet in gf.DefaultIfEmpty()
                                           join s in dbContext.SearchTerms
                                                                                      on p.SearchTerm_id equals s.id
                                           join r in dbContext.ReviewStatus
                                           on p.ReviewStatus_Id equals r.id
                                           where p.project_id == projectId && p.CreatedAt > TodayDate && p.DuplicateFlag == "No" && p.RelevanceScore > 0 && p.GroupingId != null && p.TargetedEntityId == TargetEntityId && (p.LinkFlag_Id == 1 || p.LinkFlag_Id == 2)
                                           select new
                                           { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, reviewstatus = r.Status, p.user_id, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).OrderByDescending(b => b.DatePublished).OrderByDescending(a => a.RelevanceScore).ToList();

                        var linksDisplay = linksFromDB.Select(p => new { p.Link, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlag_Id, p.reviewstatus, p.user_id, p.MakerName, p.NewsProvider, p.LinkHashCode, p.project_id, p.SearchTerm_id, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.id, p.GroupingId, p.SourceReputation, p.Summary, p.KeywordsFound, p.Theme, p.TargetedEntity, p.EventsTagged, p.ThemeVsEventJson, p.TargetedEntityId }).ToList();

                        LinkModelList = linksFromDB.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.reviewstatus, MakerId = p.user_id, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.project_id, SearchTerm_id = p.SearchTerm_id, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished.Value.ToString("dd MMM yyyy"), Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, LinkId = p.id, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId }).ToList();

                    }
                    var linksDisplayCopy = LinkModelList;

                    MainTagLinks = LinkModelList.Where(o => o.LinkId == o.GroupingId).ToList();

                    linksDisplayCopy.RemoveAll(o => o.LinkId == o.GroupingId);

                    foreach (var maintaglink in MainTagLinks)
                    {
                        maintaglink.Grouping = linksDisplayCopy.Where(x => x.GroupingId == maintaglink.GroupingId).ToList();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException);
                    return Request.CreateResponse(HttpStatusCode.OK, ex);
                }
            }
            if (themeSelected != null && eventSelected != null)
            {
                string[] themesSelected = themeSelected.Split(',');
                List<LinksModel> LinkModelList1 = MainTagLinks.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.ReviewStatu, MakerId = p.MakerId, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.ProjectId, SearchTerm_id = p.SearchTerm_id, AdminId = p.AdminId, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished, Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, SubCategory = p.SubCategory, LinkId = p.LinkId, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, EventsTagged = p.EventsTagged, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, ConcatenatedData = p.Theme + " " + p.EventsTagged + " " + p.LinkId }).ToList();

                List<LinksModel> ResultLinkModelList = new List<LinksModel>();

                foreach (var str in themesSelected)
                {
                    var resultedrows = LinkModelList1.Where(o => o.ConcatenatedData.Contains(str)).ToList();
                    ResultLinkModelList.AddRange(resultedrows);
                }
                string[] eventsSelected = eventSelected.Split(',');

                List<LinksModel> ResultLinkModelListFinal = new List<LinksModel>();
                foreach (var str in eventsSelected)
                {
                    var resultedrows = ResultLinkModelList.Where(o => o.ConcatenatedData.Contains(str)).ToList();
                    ResultLinkModelListFinal.AddRange(resultedrows);
                }

                var result = ResultLinkModelListFinal.DistinctBy(x => x.LinkId).ToList();
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = result };
                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);

                //var result = ResultLinkModelList.DistinctBy(x => x.LinkId).ToList();
                //return result;
            }
            if (themeSelected != null && eventSelected == null)
            {
                string[] themesSelected = themeSelected.Split(',');
                List<LinksModel> LinkModelList1 = MainTagLinks.Select(p => new LinksModel() { Link = p.Link, Heading = p.Heading, ImageUrl = p.ImageUrl, LinkCategorization = p.LinkCategorization, LinkFlag_Id = p.LinkFlag_Id, ReviewStatu = p.ReviewStatu, MakerId = p.MakerId, MakerName = p.MakerName, NewsProvider = p.NewsProvider, LinkHashCode = p.LinkHashCode, ProjectId = p.ProjectId, SearchTerm_id = p.SearchTerm_id, AdminId = p.AdminId, CreatedAt = p.CreatedAt, Comment = p.Comment, DatePublished = p.DatePublished, Description = p.Description, LinkCategory = p.LinkCategory, Sentiment = p.Sentiment, SentimentScore = p.SentimentScore, SentimentSentence = p.SentimentSentence, RelevanceScore = p.RelevanceScore, Tags = p.Tags, SubCategory = p.SubCategory, LinkId = p.LinkId, GroupingId = p.GroupingId, SourceReputation = p.SourceReputation, Summary = p.Summary, KeywordsFound = p.KeywordsFound, Theme = p.Theme, TargetedEntity = p.TargetedEntity, ThemeVsEventJson = p.ThemeVsEventJson, TargetedEntityId = p.TargetedEntityId, EventsTagged = p.EventsTagged, ConcatenatedData = p.Theme + " " + p.EventsTagged + " " + p.LinkId }).ToList();

                List<LinksModel> ResultLinkModelList = new List<LinksModel>();

                foreach (var str in themesSelected)
                {
                    var resultedrows = LinkModelList1.Where(o => o.ConcatenatedData.Contains(str)).ToList();
                    ResultLinkModelList.AddRange(resultedrows);
                }


                var result = ResultLinkModelList.DistinctBy(x => x.LinkId).ToList();
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = result };
                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);

                //var result = ResultLinkModelList.DistinctBy(x => x.LinkId).ToList();
                //return result;
            }
            else
            {
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = MainTagLinks };
                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
            }
        }

    }
}
