﻿
using DragnetAlphaWebAPI.Methods;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class StartContentExtractionController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        //public string GET(string project)
        //{
        //    try
        //    {
        //        using (DragnetAlpha_DbV1Entities1 dbContext = new DragnetAlpha_DbV1Entities1())
        //        {
        //            ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
        //            int projectId = projectInfo.FirstOrDefault().ProjectID;
        //            List<Task> tasks = new List<Task>();
        //            Task taskA = Task.Run(() =>
        //            {
        //                ContentExtractionMethod.ExtractContent(projectId);
        //                DSScriptRun.SummarizeAsync(projectId);
        //            });
        //            return "News Link Extracted";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return "News Link Failed";
        //    }
        //}
    }
}
