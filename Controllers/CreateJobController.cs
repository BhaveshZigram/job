﻿using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    public class CreateJobController : ApiController
    {
        DragnetAlpha_DbV2Entities1 entities = new DragnetAlpha_DbV2Entities1();
        JSONResponse json;

        public async Task<HttpResponseMessage> POST()
        {
            var data = Request.Content.ReadAsStringAsync().Result;
            JobModel input = new JobModel();
            try
            {
                input = JsonConvert.DeserializeObject<JobModel>(data.ToString());
            }
            catch
            {
                json = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.BadRequest, Message = "Invalid Format" };
                return Request.CreateResponse(json);
            }
            //String to store the Data coming in the form of Array or List
            string lang = string.Join(",", input.Language);
            string channel = string.Join(",", input.SearchChannel);
            string subs = string.Join(",", input.Subsidaries);
            string alias = string.Join(",", input.Alias);

            var obj = entities.Jobs.Where(x => x.ProjectId == input.ProjectId && x.EntityName == input.EntityName).ToList().Count();

            if(obj==0)
            {
                var dateAndTime = DateTime.Now;
                var jobs = new Job()
                {
                    ProjectId = input.ProjectId,
                    EntityName = input.EntityName,
                    CreatedAt = dateAndTime.Date,
                    Filename=input.Filename,
                    Alias = alias,
                    Country = input.Country,
                    State = input.State,
                    Subsidaries = subs,
                    Language = lang,
                    SearchChannel = channel,
                    TimeFrame = input.TimeFrame,
                    OngoingMonitoringFrequency = input.OngoingMonitoringFrequency,
                    Status=false
                };
                try
                {
                    entities.Jobs.Add(jobs);
                    await entities.SaveChangesAsync();
                    json = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Data Stored Successfully" };
                    return Request.CreateResponse(json);
                }
                catch(Exception ex)
                {
                    json = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.BadRequest, Message = ex.Message };
                    return Request.CreateResponse(json);
                }
            }
            else
            {
                json = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.BadRequest, Message = "Data Already Exists" };
                return Request.CreateResponse(json);
            }
        }
    }
}
