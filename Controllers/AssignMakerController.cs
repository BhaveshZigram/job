﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Linq;
using System.Data.Entity.Core.Objects;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class AssignMakerController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage POST(HttpRequestMessage request)
        {
            JSONResponse jsonresponse = new JSONResponse();
            try
            {
                string params_send = Request.Content.ReadAsStringAsync().Result;
                UserAssign input = new UserAssign();
                try
                {
                    input = JsonConvert.DeserializeObject<UserAssign>(params_send.ToString());
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.BadRequest, Message = "Invalid Format" };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
                int makerId = input.MakerId;
                string hashcodes = input.HashCode;
                string project = input.Project;
                string linkId = input.LinkId;
                string[] hashCodeList = hashcodes.Split(',');
                string[] LinkIds = linkId.Split(',');

                using (DragnetAlpha_DbV1Entities1 dbContext = new DragnetAlpha_DbV1Entities1())
                {
                    ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);
                    int projectId = projectInfo.FirstOrDefault().ProjectID;

                    foreach (string linkid in LinkIds)
                    {
                        int linkIds = Int32.Parse(linkid);
                        Link toSet = dbContext.Links.FirstOrDefault(x => x.LinkId == linkIds && x.ProjectId == projectId);

                        toSet.MakerId = makerId;
                        dbContext.SaveChanges();
                    }
                }
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Allocated Successfully" };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
        }
    }
}

