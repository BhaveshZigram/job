﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using DragnetAlphaWebAPI.Filters;


namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class SupportController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };

        }

        public HttpResponseMessage POST(HttpRequestMessage request)
        {
            JSONResponse jsonresponse = new JSONResponse();

            string paramsSend = Request.Content.ReadAsStringAsync().Result;
            Support info = new Support();

            info = JsonConvert.DeserializeObject<Support>(paramsSend.ToString());
            string question = info.Question;
            string emailId = info.EmailId;
            string questionCategory = info.QuestionCategory;
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    var userinfo = dbContext.ApplicationsUsers.Where(x => x.UserName == emailId).ToList();
                    var newSupport = new Support
                    {
                        Question = question,
                        UserId = userinfo[0].id,
                        QuestionCategory = questionCategory,
                        CreatedAt = DateTime.Now,
                        EmailId = userinfo[0].UserName
                    };
                    dbContext.Supports.Add(newSupport);
                    dbContext.SaveChanges();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "Query Submitted" };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }
                catch (Exception ex)
                {
                    jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                    return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
                }
            }
        }
    }
}
