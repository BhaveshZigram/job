﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DragnetAlphaWebAPI.Models;
using System.Data.Entity.Core.Objects;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class AnalyticsDashboardAdminController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET(string project)
        {
            JSONResponse jsonresponse = new JSONResponse();

            DateTime dateTime = DateTime.Today.AddDays(-30);
            // List<AnalyticsModel> trials = new List<AnalyticsModel>();
            //using (DragnetAlphaEntities dbContext = new DragnetAlphaEntities())
            //{
            //    var result = dbContext.GetAnalyticsAdmin(2, dateTime);

            //     CountForAnalytics countForAnalytics = new CountForAnalytics();
            //        List<AnalyticsModel> trials = new List<AnalyticsModel>();
            //    trials = ReadFromDB.GetLinksCountAdmin(dataAsset);
            //   trials.AddRange(result);
            //   return result;
            List<AnalyticsModel> analytics = new List<AnalyticsModel>();
            // DateTime dateTime = DateTime.Today.AddDays(-30);
            //string fromDate = dateTime.ToString("yyyy-MM-dd");

            try
            {
                using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
                {
                    ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);

                    int projectId = projectInfo.FirstOrDefault().ProjectID;

                    var result = dbContext.GetLinkFlagAnalyticsSuperAdmin(projectId, 1, dateTime);
                    foreach (var res in result)
                    {
                        analytics.Add(new AnalyticsModel
                        {
                            FlagName = res.FlagName,
                            Id = res.LinkFlag_Id,
                            Count = res.Count
                        });
                    }
                    var result1 = dbContext.GetReviewStatusAnalyticsSuperAdmin(projectId, 1, dateTime);
                    foreach (var res in result1)
                    {
                        analytics.Add(new AnalyticsModel
                        {
                            StatusName = res.StatusName,
                            Id = res.ReviewStatus_Id,
                            Count = res.Count
                        });
                    }
                    ObjectResult<int?> result2 = dbContext.GetTotalLinksAnalyticsSuperAdmin(projectId, 1, dateTime);
                    analytics.Add(new AnalyticsModel
                    {
                        TotalLinks = result2.FirstOrDefault().Value
                    });
                }
                jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = analytics };
                return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
            }

            catch (Exception ex)
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = ex.Message };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }

        }

    }
}

