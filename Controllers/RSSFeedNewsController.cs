﻿using DragnetAlphaWebAPI.Methods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    public class RSSFeedNewsController : ApiController
    {
        public string GET(int projectId)
        {
            try
            {              
                    Task taskA = Task.Run(() =>
                    {
                        NewsServices.RSSFeedNewsExtract(projectId);
                    });
                    return "News Link Extracted";
                
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.InnerException);
                return "News Link Failed";
            }
        }
    }
}
