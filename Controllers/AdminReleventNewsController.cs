﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Data.Entity.Core.Objects;
using DragnetAlphaWebAPI.Models.Enums;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class AdminReleventNewsController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public dynamic GET(string project)
        {
            DateTime today = DateTime.Today.AddDays(1);
            DateTime weekBefore = DateTime.Today.AddDays(-7);
            using (DragnetAlpha_DbV1Entities1 dbContext = new DragnetAlpha_DbV1Entities1())
            {
               // var projectInfo = dbContext.GetProjectId(project);
                ObjectResult<GetProjectId_Result> projectInfo = dbContext.GetProjectId(project);

                int projectId= projectInfo.FirstOrDefault().ProjectID;
                // var linkResults = dbContext.Links.Where(x => x.CreatedAt > weekBefore && x.CreatedAt < today && x.ProjectId== projectId).Where(z => z.LinkFlagId == 1 || z.LinkFlagId == 2).Distinct().Select(i => new { i.Link1, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlagId, i.ReviewStatusId, i.MakerId, i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTermId, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
                var linksFromDB = (from p in dbContext.Links
                                   join e in dbContext.Users
                                   on p.MakerId equals e.UserId into gf
                                   from subpet in gf.DefaultIfEmpty()
                                   join s in dbContext.AllSearchStrings
                                   on p.SearchTermId equals s.SearchStringId
                                   join r in dbContext.ReviewStatus
                                   on p.ReviewStatusId equals r.ReviewStatusId
                                   where p.ProjectId == projectId && p.CreatedAt < today && p.CreatedAt > weekBefore && (p.LinkFlagId==1 || p.LinkFlagId==2)
                                   select new
                                   { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlagId, reviewstatus = ((ReviewStatusEnum)p.ReviewStatusId).ToString(), p.MakerId, MakerName = subpet.FirstName + " " + subpet.LastName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, p.DatePublished, p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, s.SubCategory, p.LinkId }).ToList();

                //var linksFromDB = dbContext.Links.Where(x => x.CreatedAt > TodayDate && x.ProjectId == projectId).Distinct().OrderByDescending(y => y.DatePublished).Select(i => new { i.Link1, i.Heading, i.ImageUrl, i.LinkCategorization, i.LinkFlagId, i.ReviewStatusId, i.MakerId,  i.NewsProvider, i.LinkHashCode, i.ProjectId, i.SearchTermId, i.AdminId, i.CreatedAt, i.Comment, i.DatePublished, i.Description, i.LinkCategory }).ToList();
                //var ind =   linksFromDB.FindIndex(x => x.datepublished != null) ;
                //   linksFromDB[ind] = linksFromDB[ind].datepublished.Value.ToString("dd-mm-yyyy");

                return linksFromDB.Select(p => new { p.Link1, p.Heading, p.ImageUrl, p.LinkCategorization, p.LinkFlagId, p.reviewstatus, p.MakerId, p.MakerName, p.NewsProvider, p.LinkHashCode, p.ProjectId, p.SearchTermId, p.AdminId, p.CreatedAt, p.Comment, datepublished = p.DatePublished.Value.ToString("dd MMM yyyy"), p.Description, p.LinkCategory, p.Sentiment, p.SentimentScore, p.SentimentSentence, p.RelevanceScore, p.Tags, p.SubCategory, p.LinkId }).ToList();


                //List<Link> linkResults = new List<Link>();
                //linkResults = ;
                //ReadFromDB.GetLinksFromLastWeekAdmin(dataAsset);
               // return linkResults;
            }
        }
    }
}
