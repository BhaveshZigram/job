﻿using DragnetAlphaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]

    public class GetBlackListController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage GET()
        {
            JSONResponse jsonresponse = new JSONResponse();

            List<BlackListLink> blackListLinks = new List<BlackListLink>();
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                try
                {
                    BlackListLink blackListLink = new BlackListLink();
                    blackListLinks = dbContext.BlackListLinks.ToList();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = blackListLinks };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }
                catch (Exception ex)
                {
                    var quesAns = dbContext.FAQs.ToList();
                    jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Body = quesAns };
                    return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
                }
            }
        }


    }
}
