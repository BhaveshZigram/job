﻿using DragnetAlphaWebAPI.Methods;
using DragnetAlphaWebAPI.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using DragnetAlphaWebAPI.Filters;

namespace DragnetAlphaWebAPI.Controllers
{
    [Authorize]
    [ValidateUser]

    public class WhiteListController : ApiController
    {
        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public HttpResponseMessage Post()
        {
            JSONResponse jsonresponse = new JSONResponse();

            string params_send = Request.Content.ReadAsStringAsync().Result;
            WhiteListInput input = new WhiteListInput();

            try
            {
                input = JsonConvert.DeserializeObject<WhiteListInput>(params_send.ToString());
            }
            catch
            {
                jsonresponse = new JSONResponse() { Success = false, ResponseCode = HttpStatusCode.InternalServerError, Message = "Invalid Format" };
                return Request.CreateResponse(jsonresponse.ResponseCode, jsonresponse);
            }
            string whiteList = input.WhiteList;
            using (DragnetAlpha_DbV2Entities1 dbContext = new DragnetAlpha_DbV2Entities1())
            {
                var whiteLink = new WhiteListLink
                { 
                Link=whiteList
                };
                dbContext.WhiteListLinks.Add(whiteLink);
                dbContext.SaveChanges();

            }
            jsonresponse = new JSONResponse() { Success = true, ResponseCode = HttpStatusCode.OK, Message = "White Link Added" };
            return Request.CreateResponse(HttpStatusCode.OK, jsonresponse);
        }

    }
}
