
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace DragnetAlphaWebAPI
{

using System;
    using System.Collections.Generic;
    
public partial class NewsSource
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public NewsSource()
    {

        this.NewsExtractionStatus = new HashSet<NewsExtractionStatu>();

        this.NewsLinks = new HashSet<NewsLink>();

        this.ProjectNewsSourceMappings = new HashSet<ProjectNewsSourceMapping>();

    }


    public string SourceName { get; set; }

    public Nullable<bool> Active_Status { get; set; }

    public int Id { get; set; }

    public string ScriptType { get; set; }

    public string ScriptURL { get; set; }

    public string KeyMapping { get; set; }



    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<NewsExtractionStatu> NewsExtractionStatus { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<NewsLink> NewsLinks { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<ProjectNewsSourceMapping> ProjectNewsSourceMappings { get; set; }

}

}
